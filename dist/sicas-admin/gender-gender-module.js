(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["gender-gender-module"],{

/***/ "./src/app/admin/gender/gender-form/gender-form.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/admin/gender/gender-form/gender-form.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"page-title\">\n      <h3 class=\"breadcrumb-header\"> Géneros </h3>\n  </div>\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n  <div class=\"col-md-12\">\n      <div class=\"panel panel-white\">\n          <div class=\"panel-heading clearfix\">\n              <h4 class=\"panel-title\">              <span *ngIf=\"gender._id == null\">Nuevo género</span>\n               <span *ngIf=\"gender._id != null\">Editar género</span>     </h4>\n          </div>\n          <div class=\"panel-body\">\n              <form role=\"form\"  autocomplete=\"off\" (ngSubmit)=\"f.form.valid && onSubmit(f)\" #f=\"ngForm\" novalidate>\n                 <div class=\"row\">\n                        <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !code.valid}\">\n                                <label for=\"code\" class=\"control-label\">Código</label>\n                                <input type=\"text\" placeholder=\"\" name=\"code\" #code=\"ngModel\" [(ngModel)]=\"gender.code\" class=\"form-control\" required>\n                                <span class=\"help-block\" *ngIf=\"f.submitted && !code.valid\">El código es requerido.</span>\n                            </div>\n                           <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !description.valid}\">\n                              <label for=\"description\" class=\"control-label\">Descripción</label>\n                              <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"description\" #description=\"ngModel\" [(ngModel)]=\"gender.description\" required>\n                              <span class=\"help-block\" *ngIf=\"f.submitted && !description.valid\">La descripción es requerido.</span>\n                           </div>\n                           <div class=\"form-group col-md-12\">\n                              <button type=\"submit\" class=\"btn btn-primary float-right\">Guardar</button>\n                           </div>\n                 </div>\n                \n                    </form>\n          </div>\n      </div>\n   \n \n  </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/gender/gender-form/gender-form.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/admin/gender/gender-form/gender-form.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2dlbmRlci9nZW5kZXItZm9ybS9nZW5kZXItZm9ybS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/gender/gender-form/gender-form.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/admin/gender/gender-form/gender-form.component.ts ***!
  \*******************************************************************/
/*! exports provided: GenderFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenderFormComponent", function() { return GenderFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _gender__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../gender */ "./src/app/admin/gender/gender.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _gender_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../gender.service */ "./src/app/admin/gender/gender.service.ts");





var GenderFormComponent = /** @class */ (function () {
    function GenderFormComponent(_router, _activatedRoute, _genderService) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._genderService = _genderService;
        this.gender = new _gender__WEBPACK_IMPORTED_MODULE_2__["Gender"](null, null, null);
    }
    GenderFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this._activatedRoute.params.subscribe(function (params) {
            if (params['id']) {
                _this.id = params['id'];
                _this.getGender();
            }
        });
    };
    GenderFormComponent.prototype.getGender = function () {
        var _this = this;
        this._genderService.getGender(this.id).then(function (data) {
            _this.gender = new _gender__WEBPACK_IMPORTED_MODULE_2__["Gender"](data._id, data.code, data.description);
        }).catch(function (error) { return console.log(error); });
    };
    GenderFormComponent.prototype.onSubmit = function (f) {
        var _this = this;
        this._genderService
            .saveGender(this.gender)
            .then(function (data) {
            _this._router.navigate(['/admin/genders']);
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    GenderFormComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    GenderFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gender-form',
            template: __webpack_require__(/*! ./gender-form.component.html */ "./src/app/admin/gender/gender-form/gender-form.component.html"),
            providers: [_gender_service__WEBPACK_IMPORTED_MODULE_4__["GenderService"]],
            styles: [__webpack_require__(/*! ./gender-form.component.scss */ "./src/app/admin/gender/gender-form/gender-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _gender_service__WEBPACK_IMPORTED_MODULE_4__["GenderService"]])
    ], GenderFormComponent);
    return GenderFormComponent;
}());



/***/ }),

/***/ "./src/app/admin/gender/gender-list/gender-list.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/admin/gender/gender-list/gender-list.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row\">\n    <div class=\"col-sm-7 col-6\">\n        <div class=\"page-title\">\n            <h3 class=\"breadcrumb-header\">Géneros</h3>\n          </div>\n    </div>\n\n    <div class=\"col-sm-5 col-6 text-right\">\n        <a class=\"btn btn-primary btn-rounded btn-title\" routerLink=\"new\"><i class=\"fa fa-plus\"></i> Nueva género</a>\n    </div>\n</div>\n\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n<div class=\"col-md-12\">\n    <div class=\"panel panel-white\">\n        <div class=\"panel-heading clearfix\">\n            <h4 class=\"panel-title\">Lista</h4>\n        </div>\n        <div class=\"panel-body\">\n           <div class=\"table-responsive\">\n              <table datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" class=\"display table\">\n                  <thead>\n                    <tr>\n                      <th>Código</th>\n                      <th>Descripción</th>\n                      <th>Opciones</th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <tr *ngFor=\"let item of genderList\">\n                      <td>{{ item.code }}</td>\n                      <td>{{ item.description }}</td>\n                      <td>\n                        <a class=\"btn btn-default btn-xs\" routerLink=\"edit/{{item._id}}\">\n                          <i class=\"fa fa-edit\"></i> Editar</a>\n                      </td>\n                    </tr>\n                  </tbody>                \n                </table>     \n            </div>\n        </div>\n    </div>\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/gender/gender-list/gender-list.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/admin/gender/gender-list/gender-list.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2dlbmRlci9nZW5kZXItbGlzdC9nZW5kZXItbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/gender/gender-list/gender-list.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/admin/gender/gender-list/gender-list.component.ts ***!
  \*******************************************************************/
/*! exports provided: GenderListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenderListComponent", function() { return GenderListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _gender_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../gender.service */ "./src/app/admin/gender/gender.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var GenderListComponent = /** @class */ (function () {
    function GenderListComponent(_genderService) {
        this._genderService = _genderService;
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.genderList = [];
    }
    GenderListComponent.prototype.ngOnInit = function () {
        this.dtOptions = {
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        };
        this.getAllGenders();
    };
    GenderListComponent.prototype.getAllGenders = function () {
        var _this = this;
        this._genderService.getGenders()
            .then(function (data) {
            _this.genderList = data;
            _this.dtTrigger.next();
        })
            .catch(function (error) { return console.log(error); });
    };
    GenderListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gender-list',
            template: __webpack_require__(/*! ./gender-list.component.html */ "./src/app/admin/gender/gender-list/gender-list.component.html"),
            providers: [_gender_service__WEBPACK_IMPORTED_MODULE_2__["GenderService"]],
            styles: [__webpack_require__(/*! ./gender-list.component.scss */ "./src/app/admin/gender/gender-list/gender-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_gender_service__WEBPACK_IMPORTED_MODULE_2__["GenderService"]])
    ], GenderListComponent);
    return GenderListComponent;
}());



/***/ }),

/***/ "./src/app/admin/gender/gender.component.html":
/*!****************************************************!*\
  !*** ./src/app/admin/gender/gender.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin/gender/gender.component.scss":
/*!****************************************************!*\
  !*** ./src/app/admin/gender/gender.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2dlbmRlci9nZW5kZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/gender/gender.component.ts":
/*!**************************************************!*\
  !*** ./src/app/admin/gender/gender.component.ts ***!
  \**************************************************/
/*! exports provided: GenderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenderComponent", function() { return GenderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var GenderComponent = /** @class */ (function () {
    function GenderComponent() {
    }
    GenderComponent.prototype.ngOnInit = function () {
    };
    GenderComponent.prototype.ngAfterViewInit = function () {
    };
    GenderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gender',
            template: __webpack_require__(/*! ./gender.component.html */ "./src/app/admin/gender/gender.component.html"),
            styles: [__webpack_require__(/*! ./gender.component.scss */ "./src/app/admin/gender/gender.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], GenderComponent);
    return GenderComponent;
}());



/***/ }),

/***/ "./src/app/admin/gender/gender.module.ts":
/*!***********************************************!*\
  !*** ./src/app/admin/gender/gender.module.ts ***!
  \***********************************************/
/*! exports provided: GenderModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenderModule", function() { return GenderModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _gender_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./gender.component */ "./src/app/admin/gender/gender.component.ts");
/* harmony import */ var _gender_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gender.routing */ "./src/app/admin/gender/gender.routing.ts");
/* harmony import */ var _gender_list_gender_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./gender-list/gender-list.component */ "./src/app/admin/gender/gender-list/gender-list.component.ts");
/* harmony import */ var _gender_form_gender_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./gender-form/gender-form.component */ "./src/app/admin/gender/gender-form/gender-form.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");









var GenderModule = /** @class */ (function () {
    function GenderModule() {
    }
    GenderModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _gender_routing__WEBPACK_IMPORTED_MODULE_5__["GenderRoutingModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"]
            ],
            declarations: [_gender_component__WEBPACK_IMPORTED_MODULE_4__["GenderComponent"], _gender_list_gender_list_component__WEBPACK_IMPORTED_MODULE_6__["GenderListComponent"], _gender_form_gender_form_component__WEBPACK_IMPORTED_MODULE_7__["GenderFormComponent"]]
        })
    ], GenderModule);
    return GenderModule;
}());



/***/ }),

/***/ "./src/app/admin/gender/gender.routing.ts":
/*!************************************************!*\
  !*** ./src/app/admin/gender/gender.routing.ts ***!
  \************************************************/
/*! exports provided: GenderRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenderRoutingModule", function() { return GenderRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _gender_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./gender.component */ "./src/app/admin/gender/gender.component.ts");
/* harmony import */ var _gender_list_gender_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./gender-list/gender-list.component */ "./src/app/admin/gender/gender-list/gender-list.component.ts");
/* harmony import */ var _gender_form_gender_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gender-form/gender-form.component */ "./src/app/admin/gender/gender-form/gender-form.component.ts");






var routes = [
    {
        path: '',
        component: _gender_component__WEBPACK_IMPORTED_MODULE_3__["GenderComponent"],
        children: [{
                path: '',
                component: _gender_list_gender_list_component__WEBPACK_IMPORTED_MODULE_4__["GenderListComponent"],
            }, {
                path: 'new',
                component: _gender_form_gender_form_component__WEBPACK_IMPORTED_MODULE_5__["GenderFormComponent"],
            }, {
                path: 'edit/:id',
                component: _gender_form_gender_form_component__WEBPACK_IMPORTED_MODULE_5__["GenderFormComponent"],
            }
        ]
    }
];
var GenderRoutingModule = /** @class */ (function () {
    function GenderRoutingModule() {
    }
    GenderRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], GenderRoutingModule);
    return GenderRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/gender/gender.service.ts":
/*!************************************************!*\
  !*** ./src/app/admin/gender/gender.service.ts ***!
  \************************************************/
/*! exports provided: GenderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenderService", function() { return GenderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var GenderService = /** @class */ (function () {
    function GenderService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    GenderService.prototype.getGenders = function () {
        var url = this.apiEndpoint + "/genders";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    GenderService.prototype.getGender = function (id) {
        var url = this.apiEndpoint + "/genders/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    GenderService.prototype.saveGender = function (gender) {
        if (gender._id) {
            return this.updateGender(gender);
        }
        return this.addGender(gender);
    };
    GenderService.prototype.addGender = function (gender) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/genders";
        return this.http.post(url, gender, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    GenderService.prototype.updateGender = function (gender) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/genders/" + gender._id;
        return this.http
            .patch(url, gender, {
            headers: headers
        })
            .toPromise()
            .then(function () { return gender; })
            .catch(this.handleError);
    };
    GenderService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    GenderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], GenderService);
    return GenderService;
}());



/***/ }),

/***/ "./src/app/admin/gender/gender.ts":
/*!****************************************!*\
  !*** ./src/app/admin/gender/gender.ts ***!
  \****************************************/
/*! exports provided: Gender */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Gender", function() { return Gender; });
var Gender = /** @class */ (function () {
    function Gender(_id, code, description) {
        this._id = _id;
        this.code = code;
        this.description = description;
    }
    return Gender;
}());



/***/ })

}]);
//# sourceMappingURL=gender-gender-module.js.map