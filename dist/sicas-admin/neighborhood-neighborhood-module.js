(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["neighborhood-neighborhood-module"],{

/***/ "./src/app/admin/neighborhood/neighborhood-form/neighborhood-form.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood-form/neighborhood-form.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "      <div class=\"page-title\">\n          <h3 class=\"breadcrumb-header\"> Barrios </h3>\n      </div>\n  <div id=\"main-wrapper\">\n  \n  <div class=\"row\">\n      <div class=\"col-md-12\">\n          <div class=\"panel panel-white\">\n              <div class=\"panel-heading clearfix\">\n                  <h4 class=\"panel-title\"><span *ngIf=\"neighborhood._id == null\">Nuevo barrio</span>\n                     <span *ngIf=\"neighborhood._id != null\">Editar barrio</span> </h4>\n              </div>\n              <div class=\"panel-body\">\n                  <form role=\"form\"  autocomplete=\"off\" (ngSubmit)=\"f.form.valid && onSubmit(f)\" #f=\"ngForm\" novalidate>\n                     <div class=\"row\">\n                            <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !code.valid}\">\n                                <label for=\"code\" class=\"control-label\">Código</label>\n                                <input type=\"text\" placeholder=\"\" name=\"code\" #code=\"ngModel\" [(ngModel)]=\"neighborhood.code\" class=\"form-control\" required>\n                                <span class=\"help-block\" *ngIf=\"f.submitted && !code.valid\">El código es requerido.</span>\n                            </div>\n                            <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !country.valid}\">\n                                <label for=\"neighborhood\" class=\"control-label\">País</label>\n                                <select class=\"form-control\" [(ngModel)]=\"neighborhood.country._id\" name=\"country\" id=\"country\" #country=\"ngModel\" required>\n                                    <option *ngFor=\"let country of countryList\" value=\"{{country._id}}\">{{country.description}}</option>\n                                </select>\n                                    <span class=\"help-block\" *ngIf=\"f.submitted && !country.valid\">El país es requerido.</span>\n                            </div>\n                            <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !department.valid}\">\n                                    <label for=\"neighborhood\" class=\"control-label\">Departamento</label>\n                                    <select class=\"form-control\" [(ngModel)]=\"neighborhood.department._id\" name=\"department\" id=\"department\" #department=\"ngModel\" required>\n                                        <option *ngFor=\"let department of departmentList\" value=\"{{department._id}}\">{{department.description}}</option>\n                                    </select>\n                                        <span class=\"help-block\" *ngIf=\"f.submitted && !country.valid\">El departamento es requerido.</span>\n                                </div>\n                            <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !city.valid}\">\n                            <label for=\"neighborhood\" class=\"control-label\">Ciudad</label>\n                                <select class=\"form-control\" [(ngModel)]=\"neighborhood.city._id\" name=\"city\" id=\"city\" #city=\"ngModel\" required>\n                                    <option *ngFor=\"let city of cityList\" value=\"{{city._id}}\">{{city.description}}</option>\n                                </select>\n                                <span class=\"help-block\" *ngIf=\"f.submitted && !city.valid\">La ciudad es requerida.</span>\n                            </div>\n                            <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !sidewalk.valid}\">                           \n                            <label for=\"neighborhood\" class=\"control-label\">Vereda</label>\n                                <select class=\"form-control\" [(ngModel)]=\"neighborhood.sidewalk._id\" name=\"sidewalk\" id=\"sidewalk\" #sidewalk=\"ngModel\" required>\n                                    <option *ngFor=\"let sidewalk of sidewalkList\" value=\"{{sidewalk._id}}\">{{sidewalk.description}}</option>\n                                </select>\n                                <span class=\"help-block\" *ngIf=\"f.submitted && !sidewalk.valid\">La vereda es requerida.</span>\n                            </div>\n                           <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !commune.valid}\">\n                                 <label for=\"neighborhood\" class=\"control-label\">Comuna</label>\n                                    <select class=\"form-control\" [(ngModel)]=\"neighborhood.commune._id\" name=\"commune\" id=\"commune\" #commune=\"ngModel\" required>\n                                       <option *ngFor=\"let commune of communeList\" value=\"{{commune._id}}\">{{commune.description}}</option>\n                                    </select>\n                                    <span class=\"help-block\" *ngIf=\"f.submitted && !commune.valid\">La comuna es requerido.</span>\n                              </div>\n                               <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !description.valid}\">\n                                  <label for=\"description\" class=\"control-label\">Descripción</label>\n                                  <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"description\" #description=\"ngModel\" [(ngModel)]=\"neighborhood.description\" required>\n                                  <span class=\"help-block\" *ngIf=\"f.submitted && !description.valid\">La descripción es requerido.</span>\n                               </div>\n                               <div class=\"form-group col-md-12\">\n                                  <button type=\"submit\" class=\"btn btn-primary float-right\">Guardar</button>\n                               </div>\n                     </div>\n                    \n                        </form>\n              </div>\n          </div>\n       \n     \n      </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood-form/neighborhood-form.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood-form/neighborhood-form.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL25laWdoYm9yaG9vZC9uZWlnaGJvcmhvb2QtZm9ybS9uZWlnaGJvcmhvb2QtZm9ybS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood-form/neighborhood-form.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood-form/neighborhood-form.component.ts ***!
  \*************************************************************************************/
/*! exports provided: NeighborhoodFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NeighborhoodFormComponent", function() { return NeighborhoodFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _neighborhood__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../neighborhood */ "./src/app/admin/neighborhood/neighborhood.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _neighborhood_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../neighborhood.service */ "./src/app/admin/neighborhood/neighborhood.service.ts");
/* harmony import */ var _commune_commune__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../commune/commune */ "./src/app/admin/commune/commune.ts");
/* harmony import */ var _commune_commune_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../commune/commune.service */ "./src/app/admin/commune/commune.service.ts");
/* harmony import */ var _city_city__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../city/city */ "./src/app/admin/city/city.ts");
/* harmony import */ var _country_country__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../country/country */ "./src/app/admin/country/country.ts");
/* harmony import */ var _department_department__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../department/department */ "./src/app/admin/department/department.ts");
/* harmony import */ var _sidewalk_sidewalk__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../sidewalk/sidewalk */ "./src/app/admin/sidewalk/sidewalk.ts");











var NeighborhoodFormComponent = /** @class */ (function () {
    function NeighborhoodFormComponent(_router, _activatedRoute, _neighborhoodService, _communeService) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._neighborhoodService = _neighborhoodService;
        this._communeService = _communeService;
        this.neighborhood = new _neighborhood__WEBPACK_IMPORTED_MODULE_2__["Neighborhood"](null, null, null, new _country_country__WEBPACK_IMPORTED_MODULE_8__["Country"](null, null, null), new _department_department__WEBPACK_IMPORTED_MODULE_9__["Department"](null, null, null, null), new _city_city__WEBPACK_IMPORTED_MODULE_7__["City"](null, null, null, null, null), new _sidewalk_sidewalk__WEBPACK_IMPORTED_MODULE_10__["Sidewalk"](null, null, null, null, null, null), new _commune_commune__WEBPACK_IMPORTED_MODULE_5__["Commune"](null, null, null, null, null, null, null));
        this.communeList = [];
    }
    NeighborhoodFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this._activatedRoute.params.subscribe(function (params) {
            if (params['id']) {
                _this.id = params['id'];
                _this.getNeighborhood();
            }
        });
        this.getAllCommunes();
    };
    NeighborhoodFormComponent.prototype.getNeighborhood = function () {
        var _this = this;
        this._neighborhoodService.getNeighborhood(this.id).then(function (data) {
            _this.neighborhood = new _neighborhood__WEBPACK_IMPORTED_MODULE_2__["Neighborhood"](data._id, data.code, data.description, data.country, data.department, data.city, data.sidewalk, data.commune);
        }).catch(function (error) { return console.log(error); });
    };
    NeighborhoodFormComponent.prototype.getAllCommunes = function () {
        var _this = this;
        this._communeService.getCommunes()
            .then(function (data) {
            _this.communeList = data;
        })
            .catch(function (error) { return console.log(error); });
    };
    NeighborhoodFormComponent.prototype.onSubmit = function (f) {
        var _this = this;
        this._neighborhoodService
            .saveNeighborhood(this.neighborhood)
            .then(function (data) {
            _this._router.navigate(['/admin/neighborhoods']);
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    NeighborhoodFormComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    NeighborhoodFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-neighborhood-form',
            template: __webpack_require__(/*! ./neighborhood-form.component.html */ "./src/app/admin/neighborhood/neighborhood-form/neighborhood-form.component.html"),
            providers: [_neighborhood_service__WEBPACK_IMPORTED_MODULE_4__["NeighborhoodService"], _commune_commune_service__WEBPACK_IMPORTED_MODULE_6__["CommuneService"]],
            styles: [__webpack_require__(/*! ./neighborhood-form.component.scss */ "./src/app/admin/neighborhood/neighborhood-form/neighborhood-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _neighborhood_service__WEBPACK_IMPORTED_MODULE_4__["NeighborhoodService"],
            _commune_commune_service__WEBPACK_IMPORTED_MODULE_6__["CommuneService"]])
    ], NeighborhoodFormComponent);
    return NeighborhoodFormComponent;
}());



/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood-list/neighborhood-list.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood-list/neighborhood-list.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-7 col-6\">\n        <div class=\"page-title\">\n            <h3 class=\"breadcrumb-header\">Barrios</h3>\n          </div>\n    </div>\n\n    <div class=\"col-sm-5 col-6 text-right\">\n        <a class=\"btn btn-primary btn-rounded btn-title\" routerLink=\"new\"><i class=\"fa fa-plus\"></i> Nueva barrio</a>\n    </div>\n</div>\n\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n    <div class=\"col-md-12\">\n        <div class=\"panel panel-white\">\n            <div class=\"panel-heading clearfix\">\n                <h4 class=\"panel-title\">Lista</h4>\n            </div>\n            <div class=\"panel-body\">\n               <div class=\"table-responsive\">\n                  <table datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" class=\"display table\">\n                      <thead>\n                        <tr>\n                          <th>Código</th>\n                          <th>Descripción</th>\n                          <th>País</th>\n                          <th>Departamento</th>\n                          <th>Ciudad</th>\n                          <th>Vereda</th>\n                          <th>Comuna</th>                          \n                          <th>Opciones</th>\n                        </tr>\n                      </thead>\n                      <tbody>\n                        <tr *ngFor=\"let item of neighborhoodList\">                                                    \n                          <td>{{ item.code }}</td>\n                          <td>{{ item.description }}</td>\n                          <td>{{ item.country.description }}</td>\n                          <td>{{ item.department.description }}</td>\n                          <td>{{ item.city.description }}</td>\n                          <td>{{ item.sidewalk.description }}</td>\n                          <td>{{ item.commune.description }}</td>\n                          <td>\n                            <a class=\"btn btn-default btn-xs\" routerLink=\"edit/{{item._id}}\">\n                              <i class=\"fa fa-edit\"></i> Editar</a>\n                          </td>\n                        </tr>\n                      </tbody>                    \n                    </table>         \n                </div>\n            </div>\n        </div>    \n    </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood-list/neighborhood-list.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood-list/neighborhood-list.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL25laWdoYm9yaG9vZC9uZWlnaGJvcmhvb2QtbGlzdC9uZWlnaGJvcmhvb2QtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood-list/neighborhood-list.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood-list/neighborhood-list.component.ts ***!
  \*************************************************************************************/
/*! exports provided: NeighborhoodListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NeighborhoodListComponent", function() { return NeighborhoodListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _neighborhood_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../neighborhood.service */ "./src/app/admin/neighborhood/neighborhood.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var NeighborhoodListComponent = /** @class */ (function () {
    function NeighborhoodListComponent(_neighborhoodService) {
        this._neighborhoodService = _neighborhoodService;
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.neighborhoodList = [];
    }
    NeighborhoodListComponent.prototype.ngOnInit = function () {
        this.dtOptions = {
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        };
        this.getAllCountries();
    };
    NeighborhoodListComponent.prototype.getAllCountries = function () {
        var _this = this;
        this._neighborhoodService.getNeighborhoods()
            .then(function (data) {
            _this.neighborhoodList = data;
            _this.dtTrigger.next();
        })
            .catch(function (error) { return console.log(error); });
    };
    NeighborhoodListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-neighborhood-list',
            template: __webpack_require__(/*! ./neighborhood-list.component.html */ "./src/app/admin/neighborhood/neighborhood-list/neighborhood-list.component.html"),
            providers: [_neighborhood_service__WEBPACK_IMPORTED_MODULE_2__["NeighborhoodService"]],
            styles: [__webpack_require__(/*! ./neighborhood-list.component.scss */ "./src/app/admin/neighborhood/neighborhood-list/neighborhood-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_neighborhood_service__WEBPACK_IMPORTED_MODULE_2__["NeighborhoodService"]])
    ], NeighborhoodListComponent);
    return NeighborhoodListComponent;
}());



/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood.component.html":
/*!****************************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL25laWdoYm9yaG9vZC9uZWlnaGJvcmhvb2QuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood.component.ts ***!
  \**************************************************************/
/*! exports provided: NeighborhoodComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NeighborhoodComponent", function() { return NeighborhoodComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NeighborhoodComponent = /** @class */ (function () {
    function NeighborhoodComponent() {
    }
    NeighborhoodComponent.prototype.ngOnInit = function () {
    };
    NeighborhoodComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-neighborhood',
            template: __webpack_require__(/*! ./neighborhood.component.html */ "./src/app/admin/neighborhood/neighborhood.component.html"),
            styles: [__webpack_require__(/*! ./neighborhood.component.scss */ "./src/app/admin/neighborhood/neighborhood.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NeighborhoodComponent);
    return NeighborhoodComponent;
}());



/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood.module.ts ***!
  \***********************************************************/
/*! exports provided: NeighborhoodModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NeighborhoodModule", function() { return NeighborhoodModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _neighborhood_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./neighborhood.component */ "./src/app/admin/neighborhood/neighborhood.component.ts");
/* harmony import */ var _neighborhood_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./neighborhood.routing */ "./src/app/admin/neighborhood/neighborhood.routing.ts");
/* harmony import */ var _neighborhood_list_neighborhood_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./neighborhood-list/neighborhood-list.component */ "./src/app/admin/neighborhood/neighborhood-list/neighborhood-list.component.ts");
/* harmony import */ var _neighborhood_form_neighborhood_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./neighborhood-form/neighborhood-form.component */ "./src/app/admin/neighborhood/neighborhood-form/neighborhood-form.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");









var NeighborhoodModule = /** @class */ (function () {
    function NeighborhoodModule() {
    }
    NeighborhoodModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _neighborhood_routing__WEBPACK_IMPORTED_MODULE_5__["NeighborhoodRoutingModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"]
            ],
            declarations: [_neighborhood_component__WEBPACK_IMPORTED_MODULE_4__["NeighborhoodComponent"], _neighborhood_list_neighborhood_list_component__WEBPACK_IMPORTED_MODULE_6__["NeighborhoodListComponent"], _neighborhood_form_neighborhood_form_component__WEBPACK_IMPORTED_MODULE_7__["NeighborhoodFormComponent"]]
        })
    ], NeighborhoodModule);
    return NeighborhoodModule;
}());



/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood.routing.ts":
/*!************************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood.routing.ts ***!
  \************************************************************/
/*! exports provided: NeighborhoodRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NeighborhoodRoutingModule", function() { return NeighborhoodRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _neighborhood_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./neighborhood.component */ "./src/app/admin/neighborhood/neighborhood.component.ts");
/* harmony import */ var _neighborhood_list_neighborhood_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./neighborhood-list/neighborhood-list.component */ "./src/app/admin/neighborhood/neighborhood-list/neighborhood-list.component.ts");
/* harmony import */ var _neighborhood_form_neighborhood_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./neighborhood-form/neighborhood-form.component */ "./src/app/admin/neighborhood/neighborhood-form/neighborhood-form.component.ts");






var routes = [
    {
        path: '',
        component: _neighborhood_component__WEBPACK_IMPORTED_MODULE_3__["NeighborhoodComponent"],
        children: [{
                path: '',
                component: _neighborhood_list_neighborhood_list_component__WEBPACK_IMPORTED_MODULE_4__["NeighborhoodListComponent"],
            }, {
                path: 'new',
                component: _neighborhood_form_neighborhood_form_component__WEBPACK_IMPORTED_MODULE_5__["NeighborhoodFormComponent"],
            }, {
                path: 'edit/:id',
                component: _neighborhood_form_neighborhood_form_component__WEBPACK_IMPORTED_MODULE_5__["NeighborhoodFormComponent"],
            }
        ]
    }
];
var NeighborhoodRoutingModule = /** @class */ (function () {
    function NeighborhoodRoutingModule() {
    }
    NeighborhoodRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], NeighborhoodRoutingModule);
    return NeighborhoodRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=neighborhood-neighborhood-module.js.map