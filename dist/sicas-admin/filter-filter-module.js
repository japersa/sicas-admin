(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["filter-filter-module"],{

/***/ "./src/app/admin/filter/filter-form/filter-form.component.css":
/*!********************************************************************!*\
  !*** ./src/app/admin/filter/filter-form/filter-form.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2ZpbHRlci9maWx0ZXItZm9ybS9maWx0ZXItZm9ybS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/filter/filter-form/filter-form.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/admin/filter/filter-form/filter-form.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n  <h3 class=\"breadcrumb-header\"> Filtros </h3>\n</div>\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n<div class=\"col-md-12\">\n  <div class=\"panel panel-white\">\n      <div class=\"panel-heading clearfix\">\n          <h4 class=\"panel-title\"><span *ngIf=\"filter._id == null\">Nuevo filtro</span>\n             <span *ngIf=\"filter._id != null\">Editar filtro</span> </h4>\n      </div>\n      <div class=\"panel-body\">\n          <form role=\"form\"  autocomplete=\"off\" (ngSubmit)=\"f.form.valid && onSubmit(f)\" #f=\"ngForm\" novalidate>\n             <div class=\"row\">                   \n              <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !filter.valid}\">\n                <label for=\"title\" class=\"control-label\">Título</label>\n                <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"title\" #title=\"ngModel\" [(ngModel)]=\"filter.title\" required>\n                <span class=\"help-block\" *ngIf=\"f.submitted && !title.valid\">El título es requerido.</span>\n             </div>         \n              <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !description.valid}\">\n                <label for=\"description\" class=\"control-label\">Descripción</label>\n                <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"description\" #description=\"ngModel\" [(ngModel)]=\"filter.description\" required>\n                <span class=\"help-block\" *ngIf=\"f.submitted && !description.valid\">La descripción es requerido.</span>\n              </div>\n              <div class=\"form-group col-md-12\">\n                <button type=\"submit\" class=\"btn btn-primary float-right\">Guardar</button>\n              </div>\n             </div>            \n          </form>\n      </div>\n  </div>\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/filter/filter-form/filter-form.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/admin/filter/filter-form/filter-form.component.ts ***!
  \*******************************************************************/
/*! exports provided: FilterFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterFormComponent", function() { return FilterFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _filter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../filter */ "./src/app/admin/filter/filter.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _filter_filter_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../filter/filter.service */ "./src/app/admin/filter/filter.service.ts");





var FilterFormComponent = /** @class */ (function () {
    function FilterFormComponent(_router, _activatedRoute, _filterService) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._filterService = _filterService;
        this.filter = new _filter__WEBPACK_IMPORTED_MODULE_2__["Filter"](null, null, null);
    }
    FilterFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this._activatedRoute.params.subscribe(function (params) {
            if (params['id']) {
                _this.id = params['id'];
                _this.getFilter();
            }
        });
    };
    FilterFormComponent.prototype.getFilter = function () {
        var _this = this;
        this._filterService.getFilter(this.id).then(function (data) {
            _this.filter = new _filter__WEBPACK_IMPORTED_MODULE_2__["Filter"](data._id, data.title, data.description);
        }).catch(function (error) { return console.log(error); });
    };
    FilterFormComponent.prototype.onSubmit = function (f) {
        var _this = this;
        this._filterService
            .saveFilter(this.filter)
            .then(function (data) {
            _this._router.navigate(['/admin/filters']);
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    FilterFormComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    FilterFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-filter-form',
            template: __webpack_require__(/*! ./filter-form.component.html */ "./src/app/admin/filter/filter-form/filter-form.component.html"),
            providers: [_filter_filter_service__WEBPACK_IMPORTED_MODULE_4__["FilterService"]],
            styles: [__webpack_require__(/*! ./filter-form.component.css */ "./src/app/admin/filter/filter-form/filter-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _filter_filter_service__WEBPACK_IMPORTED_MODULE_4__["FilterService"]])
    ], FilterFormComponent);
    return FilterFormComponent;
}());



/***/ }),

/***/ "./src/app/admin/filter/filter-list/filter-list.component.css":
/*!********************************************************************!*\
  !*** ./src/app/admin/filter/filter-list/filter-list.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2ZpbHRlci9maWx0ZXItbGlzdC9maWx0ZXItbGlzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/filter/filter-list/filter-list.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/admin/filter/filter-list/filter-list.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row\">\n  <div class=\"col-sm-7 col-6\">\n      <div class=\"page-title\">\n          <h3 class=\"breadcrumb-header\">Filtros</h3>\n        </div>\n  </div>\n\n  <div class=\"col-sm-5 col-6 text-right\">\n      <a class=\"btn btn-primary btn-rounded btn-title\" routerLink=\"new\"><i class=\"fa fa-plus\"></i> Nuevo filtro</a>\n  </div>\n</div>\n\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n<div class=\"col-md-12\">\n  <div class=\"panel panel-white\">\n      <div class=\"panel-heading clearfix\">\n          <h4 class=\"panel-title\">Lista</h4>\n      </div>\n      <div class=\"panel-body\">\n         <div class=\"table-responsive\">\n            <table datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" class=\"display table\">\n                <thead>\n                  <tr>\n                    <th>Título</th>\n                    <th>Descripción</th>\n                    <th>Opciones</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let item of genderList\">\n                    <td>{{ item.title }}</td>\n                    <td>{{ item.description }}</td>\n                    <td>\n                      <a class=\"btn btn-default btn-xs\" routerLink=\"edit/{{item._id}}\">\n                        <i class=\"fa fa-edit\"></i> Editar</a>\n                    </td>\n                  </tr>\n                </tbody>                \n              </table>     \n          </div>\n      </div>\n  </div>\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/filter/filter-list/filter-list.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/admin/filter/filter-list/filter-list.component.ts ***!
  \*******************************************************************/
/*! exports provided: FilterListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterListComponent", function() { return FilterListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _filter_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../filter.service */ "./src/app/admin/filter/filter.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var FilterListComponent = /** @class */ (function () {
    function FilterListComponent(_filterService) {
        this._filterService = _filterService;
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.filterList = [];
    }
    FilterListComponent.prototype.ngOnInit = function () {
        this.dtOptions = {
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        };
        this.getAllFilters();
    };
    FilterListComponent.prototype.getAllFilters = function () {
        var _this = this;
        this._filterService.getFilters()
            .then(function (data) {
            _this.filterList = data;
            _this.dtTrigger.next();
        })
            .catch(function (error) { return console.log(error); });
    };
    FilterListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-filter-list',
            template: __webpack_require__(/*! ./filter-list.component.html */ "./src/app/admin/filter/filter-list/filter-list.component.html"),
            providers: [_filter_service__WEBPACK_IMPORTED_MODULE_2__["FilterService"]],
            styles: [__webpack_require__(/*! ./filter-list.component.css */ "./src/app/admin/filter/filter-list/filter-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_filter_service__WEBPACK_IMPORTED_MODULE_2__["FilterService"]])
    ], FilterListComponent);
    return FilterListComponent;
}());



/***/ }),

/***/ "./src/app/admin/filter/filter.component.css":
/*!***************************************************!*\
  !*** ./src/app/admin/filter/filter.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2ZpbHRlci9maWx0ZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/filter/filter.component.html":
/*!****************************************************!*\
  !*** ./src/app/admin/filter/filter.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin/filter/filter.component.ts":
/*!**************************************************!*\
  !*** ./src/app/admin/filter/filter.component.ts ***!
  \**************************************************/
/*! exports provided: FilterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterComponent", function() { return FilterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FilterComponent = /** @class */ (function () {
    function FilterComponent() {
    }
    FilterComponent.prototype.ngOnInit = function () {
    };
    FilterComponent.prototype.ngAfterViewInit = function () {
    };
    FilterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-filter',
            template: __webpack_require__(/*! ./filter.component.html */ "./src/app/admin/filter/filter.component.html"),
            styles: [__webpack_require__(/*! ./filter.component.css */ "./src/app/admin/filter/filter.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FilterComponent);
    return FilterComponent;
}());



/***/ }),

/***/ "./src/app/admin/filter/filter.module.ts":
/*!***********************************************!*\
  !*** ./src/app/admin/filter/filter.module.ts ***!
  \***********************************************/
/*! exports provided: FilterModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterModule", function() { return FilterModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _filter_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./filter.component */ "./src/app/admin/filter/filter.component.ts");
/* harmony import */ var _filter_list_filter_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./filter-list/filter-list.component */ "./src/app/admin/filter/filter-list/filter-list.component.ts");
/* harmony import */ var _filter_form_filter_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./filter-form/filter-form.component */ "./src/app/admin/filter/filter-form/filter-form.component.ts");
/* harmony import */ var _filter_routing__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./filter.routing */ "./src/app/admin/filter/filter.routing.ts");









var FilterModule = /** @class */ (function () {
    function FilterModule() {
    }
    FilterModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _filter_routing__WEBPACK_IMPORTED_MODULE_8__["FilterRoutingModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"]
            ],
            declarations: [_filter_component__WEBPACK_IMPORTED_MODULE_5__["FilterComponent"], _filter_list_filter_list_component__WEBPACK_IMPORTED_MODULE_6__["FilterListComponent"], _filter_form_filter_form_component__WEBPACK_IMPORTED_MODULE_7__["FilterFormComponent"]]
        })
    ], FilterModule);
    return FilterModule;
}());



/***/ }),

/***/ "./src/app/admin/filter/filter.routing.ts":
/*!************************************************!*\
  !*** ./src/app/admin/filter/filter.routing.ts ***!
  \************************************************/
/*! exports provided: FilterRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterRoutingModule", function() { return FilterRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _filter_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./filter.component */ "./src/app/admin/filter/filter.component.ts");
/* harmony import */ var _filter_list_filter_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./filter-list/filter-list.component */ "./src/app/admin/filter/filter-list/filter-list.component.ts");
/* harmony import */ var _filter_form_filter_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./filter-form/filter-form.component */ "./src/app/admin/filter/filter-form/filter-form.component.ts");






var routes = [
    {
        path: '',
        component: _filter_component__WEBPACK_IMPORTED_MODULE_3__["FilterComponent"],
        children: [{
                path: '',
                component: _filter_list_filter_list_component__WEBPACK_IMPORTED_MODULE_4__["FilterListComponent"],
            }, {
                path: 'new',
                component: _filter_form_filter_form_component__WEBPACK_IMPORTED_MODULE_5__["FilterFormComponent"],
            }, {
                path: 'edit/:id',
                component: _filter_form_filter_form_component__WEBPACK_IMPORTED_MODULE_5__["FilterFormComponent"],
            }
        ]
    }
];
var FilterRoutingModule = /** @class */ (function () {
    function FilterRoutingModule() {
    }
    FilterRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], FilterRoutingModule);
    return FilterRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/filter/filter.service.ts":
/*!************************************************!*\
  !*** ./src/app/admin/filter/filter.service.ts ***!
  \************************************************/
/*! exports provided: FilterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterService", function() { return FilterService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var FilterService = /** @class */ (function () {
    function FilterService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    FilterService.prototype.getFilters = function () {
        var url = this.apiEndpoint + "/filters";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    FilterService.prototype.getFilter = function (id) {
        var url = this.apiEndpoint + "/filters/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    FilterService.prototype.saveFilter = function (filter) {
        if (filter._id) {
            return this.updateFilter(filter);
        }
        return this.addFilter(filter);
    };
    FilterService.prototype.addFilter = function (filter) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/filters";
        return this.http.post(url, filter, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    FilterService.prototype.updateFilter = function (filter) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/filters/" + filter._id;
        return this.http
            .patch(url, filter, {
            headers: headers
        })
            .toPromise()
            .then(function () { return filter; })
            .catch(this.handleError);
    };
    FilterService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    FilterService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], FilterService);
    return FilterService;
}());



/***/ }),

/***/ "./src/app/admin/filter/filter.ts":
/*!****************************************!*\
  !*** ./src/app/admin/filter/filter.ts ***!
  \****************************************/
/*! exports provided: Filter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Filter", function() { return Filter; });
var Filter = /** @class */ (function () {
    function Filter(_id, title, description) {
        this._id = _id;
        this.title = title;
        this.description = description;
    }
    return Filter;
}());



/***/ })

}]);
//# sourceMappingURL=filter-filter-module.js.map