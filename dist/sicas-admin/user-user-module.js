(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-user-module"],{

/***/ "./src/app/admin/user/user-form/user-form.component.css":
/*!**************************************************************!*\
  !*** ./src/app/admin/user/user-form/user-form.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3VzZXIvdXNlci1mb3JtL3VzZXItZm9ybS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/user/user-form/user-form.component.html":
/*!***************************************************************!*\
  !*** ./src/app/admin/user/user-form/user-form.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"page-title\">\n      <h3 class=\"breadcrumb-header\"> Usuarios </h3>\n  </div>\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n  <div class=\"col-md-12\">\n      <div class=\"panel panel-white\">\n          <div class=\"panel-heading clearfix\">\n              <h4 class=\"panel-title\">              <span *ngIf=\"user._id == null\">Nuevo usuario</span>\n               <span *ngIf=\"user._id != null\">Editar usuario</span>     </h4>\n          </div>\n          <div class=\"panel-body\">\n                <form role=\"form\"  autocomplete=\"off\" (ngSubmit)=\"f.form.valid && onSubmit(f)\" #f=\"ngForm\" novalidate>\n                       <div class=\"row\">\n                          <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !name.valid}\">\n                            <label for=\"name\">Nombre</label>\n                            <input type=\"text\" class=\"form-control\" id=\"name\" [(ngModel)]=\"user.name\" name=\"name\" #name=\"ngModel\" required>\n                            <span class=\"help-block\" *ngIf=\"f.submitted && !name.valid\">El nombre es requerido.</span>\n                          </div>\n                          <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !email.valid}\">\n                            <label for=\"email\">Correo</label>\n                            <input type=\"text\" class=\"form-control\" id=\"email\" [(ngModel)]=\"user.email\" name=\"email\" #email=\"ngModel\" required>\n                            <span class=\"help-block\" *ngIf=\"f.submitted && !email.valid\">El correo es requerido.</span>\n                          </div>\n                          <div class=\"form-group col-md-12\" *ngIf=\"user.id == null\" [ngClass]=\"{'has-error': f.submitted && !password.valid}\">\n                            <label for=\"password\">Contraseña</label>\n                            <input type=\"password\" class=\"form-control\" id=\"password\" [(ngModel)]=\"user.password\" name=\"password\" #password=\"ngModel\"\n                              required>\n                            <span class=\"help-block\" *ngIf=\"f.submitted && !password.valid\">La contraseña es requerida.</span>\n                          </div>\n                          <div class=\"form-group col-md-12\">\n                              <label for=\"password\">Roles</label>\n                              <ng-select [items]=\"roles\" multiple=\"true\"\n                              bindLabel=\"name\" bindValue=\"_id\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"roles_ids\">\n                        </ng-select>\n                        </div>\n                        <div class=\"form-group col-md-12\">\n                                <button type=\"submit\" class=\"btn btn-primary float-right\">Guardar</button>\n                             </div>\n                      </div>\n                  \n                        </form>\n                    </div>\n                \n      </div>\n   \n \n  </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/user/user-form/user-form.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/admin/user/user-form/user-form.component.ts ***!
  \*************************************************************/
/*! exports provided: UserFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserFormComponent", function() { return UserFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../user */ "./src/app/admin/user/user.ts");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../user.service */ "./src/app/admin/user/user.service.ts");
/* harmony import */ var _role_role_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../role/role.service */ "./src/app/admin/role/role.service.ts");






var UserFormComponent = /** @class */ (function () {
    function UserFormComponent(_router, _activatedRoute, _userService, _roleService) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._userService = _userService;
        this._roleService = _roleService;
        this.user = new _user__WEBPACK_IMPORTED_MODULE_3__["User"](null, null, null, null, null);
        this.roles_ids = [];
        this.roles = [];
    }
    UserFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this._activatedRoute.params.subscribe(function (params) {
            if (params['id']) {
                _this.id = params['id'];
                _this.getUser();
            }
        });
        this.getAllRoles();
    };
    UserFormComponent.prototype.getUser = function () {
        var _this = this;
        this._userService.getUser(this.id).then(function (data) {
            _this.user = new _user__WEBPACK_IMPORTED_MODULE_3__["User"](data._id, data.name, data.email, null, data.roles);
            _this.checkUserRoles();
        }).catch(function (error) { return console.log(error); });
    };
    UserFormComponent.prototype.getAllRoles = function () {
        var _this = this;
        this._roleService.getRoles()
            .then(function (data) {
            _this.roles = data;
            _this.checkUserRoles();
        })
            .catch(function (error) { return console.log(error); });
    };
    UserFormComponent.prototype.checkUserRoles = function () {
        var _this = this;
        this.roles.forEach(function (element) {
            _this.user.roles.forEach(function (role) {
                if (element._id == role._id) {
                    _this.roles_ids.push(element._id);
                }
            });
        });
    };
    UserFormComponent.prototype.setUserRoles = function () {
        var _this = this;
        this.user.roles = [];
        this.roles_ids.forEach(function (element) {
            _this.roles.forEach(function (role) {
                if (element == role._id) {
                    _this.user.roles.push(role);
                }
            });
        });
    };
    UserFormComponent.prototype.onSubmit = function (f) {
        var _this = this;
        this.setUserRoles();
        console.log(this.roles_ids);
        this._userService
            .saveUser(this.user)
            .then(function (data) {
            _this._router.navigate(['/admin/users']);
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    UserFormComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    UserFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-form',
            template: __webpack_require__(/*! ./user-form.component.html */ "./src/app/admin/user/user-form/user-form.component.html"),
            providers: [_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"], _role_role_service__WEBPACK_IMPORTED_MODULE_5__["RoleService"]],
            styles: [__webpack_require__(/*! ./user-form.component.css */ "./src/app/admin/user/user-form/user-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
            _role_role_service__WEBPACK_IMPORTED_MODULE_5__["RoleService"]])
    ], UserFormComponent);
    return UserFormComponent;
}());



/***/ }),

/***/ "./src/app/admin/user/user-list/user-list.component.html":
/*!***************************************************************!*\
  !*** ./src/app/admin/user/user-list/user-list.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n        <div class=\"col-sm-7 col-6\">\n            <div class=\"page-title\">\n                <h3 class=\"breadcrumb-header\">Usuarios</h3>\n              </div>\n        </div>\n    \n        <div class=\"col-sm-5 col-6 text-right\">\n            <a class=\"btn btn-primary btn-rounded btn-title\" routerLink=\"new\"><i class=\"fa fa-plus\"></i> Nuevo usuario</a>\n        </div>\n    </div>\n    \n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n<div class=\"col-md-12\">\n    <div class=\"panel panel-white\">\n        <div class=\"panel-heading clearfix\">\n            <h4 class=\"panel-title\">Lista</h4>\n        </div>\n        <div class=\"panel-body\">\n           <div class=\"table-responsive\">\n              <table datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" class=\"display table\">\n                  <thead>\n                    <tr>\n                        <th>Nombre</th>\n                        <th>Correo</th>\n                        <th>Roles</th>\n                        <th>Opciones</th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                      <tr *ngFor=\"let item of userList\">\n                          <td>{{ item.name }}</td>\n                          <td>{{ item.email }}</td>\n                          <td>\n                              <span class=\"label label-success label-spaced\" *ngFor=\"let role of item.roles\">\n                                 {{role.name}}\n                              </span>\n                          </td>\n                      <td>\n                        <a class=\"btn btn-default btn-xs\" routerLink=\"edit/{{item._id}}\">\n                          <i class=\"fa fa-edit\"></i> Editar</a>\n                      </td>\n                    </tr>\n                  </tbody>\n                \n                </table>\n     \n            </div>\n        </div>\n    </div>\n \n\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/user/user-list/user-list.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/admin/user/user-list/user-list.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3VzZXIvdXNlci1saXN0L3VzZXItbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/user/user-list/user-list.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/admin/user/user-list/user-list.component.ts ***!
  \*************************************************************/
/*! exports provided: UserListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserListComponent", function() { return UserListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user.service */ "./src/app/admin/user/user.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var UserListComponent = /** @class */ (function () {
    function UserListComponent(_userService) {
        this._userService = _userService;
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.userList = [];
    }
    UserListComponent.prototype.ngOnInit = function () {
        this.dtOptions = {
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        };
        this.getAllUsers();
    };
    UserListComponent.prototype.getAllUsers = function () {
        var _this = this;
        this._userService.getUsers()
            .then(function (data) {
            _this.userList = data;
            _this.dtTrigger.next();
        })
            .catch(function (error) { return console.log(error); });
    };
    UserListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-list',
            template: __webpack_require__(/*! ./user-list.component.html */ "./src/app/admin/user/user-list/user-list.component.html"),
            providers: [_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]],
            styles: [__webpack_require__(/*! ./user-list.component.scss */ "./src/app/admin/user/user-list/user-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]])
    ], UserListComponent);
    return UserListComponent;
}());



/***/ }),

/***/ "./src/app/admin/user/user.component.html":
/*!************************************************!*\
  !*** ./src/app/admin/user/user.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin/user/user.component.scss":
/*!************************************************!*\
  !*** ./src/app/admin/user/user.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3VzZXIvdXNlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/user/user.component.ts":
/*!**********************************************!*\
  !*** ./src/app/admin/user/user.component.ts ***!
  \**********************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UserComponent = /** @class */ (function () {
    function UserComponent() {
    }
    UserComponent.prototype.ngOnInit = function () {
    };
    UserComponent.prototype.ngAfterViewInit = function () {
    };
    UserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! ./user.component.html */ "./src/app/admin/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.scss */ "./src/app/admin/user/user.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/app/admin/user/user.module.ts":
/*!*******************************************!*\
  !*** ./src/app/admin/user/user.module.ts ***!
  \*******************************************/
/*! exports provided: UserModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModule", function() { return UserModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _user_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user.component */ "./src/app/admin/user/user.component.ts");
/* harmony import */ var _user_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user.routing */ "./src/app/admin/user/user.routing.ts");
/* harmony import */ var _user_list_user_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-list/user-list.component */ "./src/app/admin/user/user-list/user-list.component.ts");
/* harmony import */ var _user_form_user_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./user-form/user-form.component */ "./src/app/admin/user/user-form/user-form.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");










var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _user_routing__WEBPACK_IMPORTED_MODULE_5__["UserRoutingModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"],
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_9__["NgSelectModule"]
            ],
            declarations: [_user_component__WEBPACK_IMPORTED_MODULE_4__["UserComponent"], _user_list_user_list_component__WEBPACK_IMPORTED_MODULE_6__["UserListComponent"], _user_form_user_form_component__WEBPACK_IMPORTED_MODULE_7__["UserFormComponent"]]
        })
    ], UserModule);
    return UserModule;
}());



/***/ }),

/***/ "./src/app/admin/user/user.routing.ts":
/*!********************************************!*\
  !*** ./src/app/admin/user/user.routing.ts ***!
  \********************************************/
/*! exports provided: UserRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserRoutingModule", function() { return UserRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user.component */ "./src/app/admin/user/user.component.ts");
/* harmony import */ var _user_list_user_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user-list/user-list.component */ "./src/app/admin/user/user-list/user-list.component.ts");
/* harmony import */ var _user_form_user_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-form/user-form.component */ "./src/app/admin/user/user-form/user-form.component.ts");






var routes = [
    {
        path: '',
        component: _user_component__WEBPACK_IMPORTED_MODULE_3__["UserComponent"],
        children: [{
                path: '',
                component: _user_list_user_list_component__WEBPACK_IMPORTED_MODULE_4__["UserListComponent"],
            }, {
                path: 'new',
                component: _user_form_user_form_component__WEBPACK_IMPORTED_MODULE_5__["UserFormComponent"],
            }, {
                path: 'edit/:id',
                component: _user_form_user_form_component__WEBPACK_IMPORTED_MODULE_5__["UserFormComponent"],
            }
        ]
    }
];
var UserRoutingModule = /** @class */ (function () {
    function UserRoutingModule() {
    }
    UserRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], UserRoutingModule);
    return UserRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/user/user.service.ts":
/*!********************************************!*\
  !*** ./src/app/admin/user/user.service.ts ***!
  \********************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    UserService.prototype.getUsers = function () {
        var url = this.apiEndpoint + "/users";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    UserService.prototype.getUser = function (id) {
        var url = this.apiEndpoint + "/users/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    UserService.prototype.saveUser = function (user) {
        if (user._id) {
            return this.updateUser(user);
        }
        return this.addUser(user);
    };
    UserService.prototype.addUser = function (user) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/users";
        return this.http.post(url, user, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    UserService.prototype.updateUser = function (user) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/users/" + user._id;
        return this.http
            .patch(url, user, {
            headers: headers
        })
            .toPromise()
            .then(function () { return user; })
            .catch(this.handleError);
    };
    UserService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/admin/user/user.ts":
/*!************************************!*\
  !*** ./src/app/admin/user/user.ts ***!
  \************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User(_id, name, email, password, roles) {
        this._id = _id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }
    return User;
}());



/***/ })

}]);
//# sourceMappingURL=user-user-module.js.map