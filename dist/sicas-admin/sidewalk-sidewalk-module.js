(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sidewalk-sidewalk-module"],{

/***/ "./src/app/admin/sidewalk/sidewalk-form/sidewalk-form.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk-form/sidewalk-form.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "      <div class=\"page-title\">\n          <h3 class=\"breadcrumb-header\"> Veredas </h3>\n      </div>\n  <div id=\"main-wrapper\">\n  \n  <div class=\"row\">\n      <div class=\"col-md-12\">\n          <div class=\"panel panel-white\">\n              <div class=\"panel-heading clearfix\">\n                  <h4 class=\"panel-title\"><span *ngIf=\"sidewalk._id == null\">Nueva vereda</span>\n                     <span *ngIf=\"sidewalk._id != null\">Editar vereda</span> </h4>\n              </div>\n              <div class=\"panel-body\">\n                  <form role=\"form\"  autocomplete=\"off\" (ngSubmit)=\"f.form.valid && onSubmit(f)\" #f=\"ngForm\" novalidate>\n                     <div class=\"row\">\n                            <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !code.valid}\">\n                                <label for=\"code\" class=\"control-label\">Código</label>\n                                <input type=\"text\" placeholder=\"\" name=\"code\" #code=\"ngModel\" [(ngModel)]=\"sidewalk.code\" class=\"form-control\" required>\n                                <span class=\"help-block\" *ngIf=\"f.submitted && !code.valid\">El código es requerido.</span>\n                            </div>\n                            <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !country.valid}\">\n                                <label for=\"country\" class=\"control-label\">País</label>\n                                <select class=\"form-control\" [(ngModel)]=\"sidewalk.country._id\" name=\"country\" id=\"country\" #country=\"ngModel\" required>\n                                    <option *ngFor=\"let country of countryList\" value=\"{{country._id}}\">{{country.description}}</option>\n                                </select>\n                                    <span class=\"help-block\" *ngIf=\"f.submitted && !country.valid\">El país es requerido.</span>\n                            </div>\n                                <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !department.valid}\">\n                                        <label for=\"department\" class=\"control-label\">Departamento</label>\n                                        <select class=\"form-control\" [(ngModel)]=\"sidewalk.department._id\" name=\"department\" id=\"department\" #department=\"ngModel\" required>\n                                            <option *ngFor=\"let department of departmentList\" value=\"{{department._id}}\">{{department.description}}</option>\n                                        </select>\n                                            <span class=\"help-block\" *ngIf=\"f.submitted && !department.valid\">El departamento es requerido.</span>\n                                    </div>\n                                <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !city.valid}\">\n                                <label for=\"city\" class=\"control-label\">Ciudad</label>\n                                    <select class=\"form-control\" [(ngModel)]=\"sidewalk.city._id\" name=\"city\" id=\"city\" #city=\"ngModel\" required>\n                                        <option *ngFor=\"let city of cityList\" value=\"{{city._id}}\">{{city.description}}</option>\n                                    </select>\n                                    <span class=\"help-block\" *ngIf=\"f.submitted && !city.valid\">La ciudad es requerida.</span>\n                                </div>                           \n                               <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !description.valid}\">\n                                  <label for=\"description\" class=\"control-label\">Descripción</label>\n                                  <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"description\" #description=\"ngModel\" [(ngModel)]=\"sidewalk.description\" required>\n                                  <span class=\"help-block\" *ngIf=\"f.submitted && !description.valid\">La descripción es requerido.</span>\n                               </div>\n                               <div class=\"form-group col-md-12\">\n                                  <button type=\"submit\" class=\"btn btn-primary float-right\">Guardar</button>\n                               </div>\n                     </div>\n                    \n                        </form>\n              </div>\n          </div>\n       \n     \n      </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk-form/sidewalk-form.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk-form/sidewalk-form.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3NpZGV3YWxrL3NpZGV3YWxrLWZvcm0vc2lkZXdhbGstZm9ybS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk-form/sidewalk-form.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk-form/sidewalk-form.component.ts ***!
  \*************************************************************************/
/*! exports provided: SidewalkFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidewalkFormComponent", function() { return SidewalkFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _sidewalk__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../sidewalk */ "./src/app/admin/sidewalk/sidewalk.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _sidewalk_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../sidewalk.service */ "./src/app/admin/sidewalk/sidewalk.service.ts");
/* harmony import */ var _city_city_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../city/city.service */ "./src/app/admin/city/city.service.ts");
/* harmony import */ var _city_city__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../city/city */ "./src/app/admin/city/city.ts");
/* harmony import */ var _country_country__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../country/country */ "./src/app/admin/country/country.ts");
/* harmony import */ var _department_department__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../department/department */ "./src/app/admin/department/department.ts");









var SidewalkFormComponent = /** @class */ (function () {
    function SidewalkFormComponent(_router, _activatedRoute, _sidewalkService, _cityService) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._sidewalkService = _sidewalkService;
        this._cityService = _cityService;
        this.sidewalk = new _sidewalk__WEBPACK_IMPORTED_MODULE_2__["Sidewalk"](null, null, new _country_country__WEBPACK_IMPORTED_MODULE_7__["Country"](null, null, null), new _department_department__WEBPACK_IMPORTED_MODULE_8__["Department"](null, null, null, null), new _city_city__WEBPACK_IMPORTED_MODULE_6__["City"](null, null, null, null, null), null);
        this.cityList = [];
    }
    SidewalkFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this._activatedRoute.params.subscribe(function (params) {
            if (params['id']) {
                _this.id = params['id'];
                _this.getSidewalk();
            }
        });
        this.getAllCities();
    };
    SidewalkFormComponent.prototype.getSidewalk = function () {
        var _this = this;
        this._sidewalkService.getSidewalk(this.id).then(function (data) {
            _this.sidewalk = new _sidewalk__WEBPACK_IMPORTED_MODULE_2__["Sidewalk"](data._id, data.code, data.country, data.department, data.city, data.description);
        }).catch(function (error) { return console.log(error); });
    };
    SidewalkFormComponent.prototype.getAllCities = function () {
        var _this = this;
        this._cityService.getCities()
            .then(function (data) {
            _this.cityList = data;
        })
            .catch(function (error) { return console.log(error); });
    };
    SidewalkFormComponent.prototype.onSubmit = function (f) {
        var _this = this;
        this._sidewalkService
            .saveSidewalk(this.sidewalk)
            .then(function (data) {
            _this._router.navigate(['/admin/sidewalks']);
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    SidewalkFormComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    SidewalkFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sidewalk-form',
            template: __webpack_require__(/*! ./sidewalk-form.component.html */ "./src/app/admin/sidewalk/sidewalk-form/sidewalk-form.component.html"),
            providers: [_sidewalk_service__WEBPACK_IMPORTED_MODULE_4__["SidewalkService"], _city_city_service__WEBPACK_IMPORTED_MODULE_5__["CityService"]],
            styles: [__webpack_require__(/*! ./sidewalk-form.component.scss */ "./src/app/admin/sidewalk/sidewalk-form/sidewalk-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _sidewalk_service__WEBPACK_IMPORTED_MODULE_4__["SidewalkService"],
            _city_city_service__WEBPACK_IMPORTED_MODULE_5__["CityService"]])
    ], SidewalkFormComponent);
    return SidewalkFormComponent;
}());



/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk-list/sidewalk-list.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk-list/sidewalk-list.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "    <div class=\"row\">\n        <div class=\"col-sm-7 col-6\">\n            <div class=\"page-title\">\n                <h3 class=\"breadcrumb-header\">Veredas</h3>\n              </div>\n        </div>\n    \n        <div class=\"col-sm-5 col-6 text-right\">\n            <a class=\"btn btn-primary btn-rounded btn-title\" routerLink=\"new\"><i class=\"fa fa-plus\"></i> Nueva vereda</a>\n        </div>\n    </div>\n\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n    <div class=\"col-md-12\">\n        <div class=\"panel panel-white\">\n            <div class=\"panel-heading clearfix\">\n                <h4 class=\"panel-title\">Lista</h4>\n            </div>\n            <div class=\"panel-body\">\n               <div class=\"table-responsive\">\n                  <table datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" class=\"display table\">\n                      <thead>\n                        <tr>\n                          <th>Código</th>\n                          <th>País</th>\n                          <th>Departamento</th>                          \n                          <th>Ciudad</th>\n                          <th>Descripción</th>                                                    \n                          <th>Opciones</th>\n                        </tr>\n                      </thead>\n                      <tbody>\n                        <tr *ngFor=\"let item of sidewalkList\">\n                          <td>{{ item.code }}</td>\n                          <td>{{ item.country.description }}</td>\n                          <td>{{ item.department.description }}</td>                          \n                          <td>{{ item.city.description }}</td>\n                          <td>{{ item.description }}</td>                          \n                          <td>\n                            <a class=\"btn btn-default btn-xs\" routerLink=\"edit/{{item._id}}\">\n                              <i class=\"fa fa-edit\"></i> Editar</a>\n                          </td>\n                        </tr>\n                      </tbody>\n                    \n                    </table>\n         \n                </div>\n            </div>\n        </div>\n     \n   \n    </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk-list/sidewalk-list.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk-list/sidewalk-list.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3NpZGV3YWxrL3NpZGV3YWxrLWxpc3Qvc2lkZXdhbGstbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk-list/sidewalk-list.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk-list/sidewalk-list.component.ts ***!
  \*************************************************************************/
/*! exports provided: SidewalkListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidewalkListComponent", function() { return SidewalkListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _sidewalk_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../sidewalk.service */ "./src/app/admin/sidewalk/sidewalk.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var SidewalkListComponent = /** @class */ (function () {
    function SidewalkListComponent(_sidewalkService) {
        this._sidewalkService = _sidewalkService;
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.sidewalkList = [];
    }
    SidewalkListComponent.prototype.ngOnInit = function () {
        this.dtOptions = {
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        };
        this.getAllSidewalks();
    };
    SidewalkListComponent.prototype.getAllSidewalks = function () {
        var _this = this;
        this._sidewalkService.getSidewalks()
            .then(function (data) {
            _this.sidewalkList = data;
            _this.dtTrigger.next();
        })
            .catch(function (error) { return console.log(error); });
    };
    SidewalkListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sidewalk-list',
            template: __webpack_require__(/*! ./sidewalk-list.component.html */ "./src/app/admin/sidewalk/sidewalk-list/sidewalk-list.component.html"),
            providers: [_sidewalk_service__WEBPACK_IMPORTED_MODULE_2__["SidewalkService"]],
            styles: [__webpack_require__(/*! ./sidewalk-list.component.scss */ "./src/app/admin/sidewalk/sidewalk-list/sidewalk-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_sidewalk_service__WEBPACK_IMPORTED_MODULE_2__["SidewalkService"]])
    ], SidewalkListComponent);
    return SidewalkListComponent;
}());



/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk.component.html":
/*!********************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk.component.scss":
/*!********************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3NpZGV3YWxrL3NpZGV3YWxrLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk.component.ts":
/*!******************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk.component.ts ***!
  \******************************************************/
/*! exports provided: SidewalkComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidewalkComponent", function() { return SidewalkComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SidewalkComponent = /** @class */ (function () {
    function SidewalkComponent() {
    }
    SidewalkComponent.prototype.ngOnInit = function () {
    };
    SidewalkComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sidewalk',
            template: __webpack_require__(/*! ./sidewalk.component.html */ "./src/app/admin/sidewalk/sidewalk.component.html"),
            styles: [__webpack_require__(/*! ./sidewalk.component.scss */ "./src/app/admin/sidewalk/sidewalk.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SidewalkComponent);
    return SidewalkComponent;
}());



/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk.module.ts":
/*!***************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk.module.ts ***!
  \***************************************************/
/*! exports provided: SidewalkModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidewalkModule", function() { return SidewalkModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _sidewalk_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./sidewalk.component */ "./src/app/admin/sidewalk/sidewalk.component.ts");
/* harmony import */ var _sidewalk_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sidewalk.routing */ "./src/app/admin/sidewalk/sidewalk.routing.ts");
/* harmony import */ var _sidewalk_list_sidewalk_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sidewalk-list/sidewalk-list.component */ "./src/app/admin/sidewalk/sidewalk-list/sidewalk-list.component.ts");
/* harmony import */ var _sidewalk_form_sidewalk_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./sidewalk-form/sidewalk-form.component */ "./src/app/admin/sidewalk/sidewalk-form/sidewalk-form.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");









var SidewalkModule = /** @class */ (function () {
    function SidewalkModule() {
    }
    SidewalkModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _sidewalk_routing__WEBPACK_IMPORTED_MODULE_5__["SidewalkRoutingModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"]
            ],
            declarations: [_sidewalk_component__WEBPACK_IMPORTED_MODULE_4__["SidewalkComponent"], _sidewalk_list_sidewalk_list_component__WEBPACK_IMPORTED_MODULE_6__["SidewalkListComponent"], _sidewalk_form_sidewalk_form_component__WEBPACK_IMPORTED_MODULE_7__["SidewalkFormComponent"]]
        })
    ], SidewalkModule);
    return SidewalkModule;
}());



/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk.routing.ts":
/*!****************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk.routing.ts ***!
  \****************************************************/
/*! exports provided: SidewalkRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidewalkRoutingModule", function() { return SidewalkRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _sidewalk_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sidewalk.component */ "./src/app/admin/sidewalk/sidewalk.component.ts");
/* harmony import */ var _sidewalk_list_sidewalk_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./sidewalk-list/sidewalk-list.component */ "./src/app/admin/sidewalk/sidewalk-list/sidewalk-list.component.ts");
/* harmony import */ var _sidewalk_form_sidewalk_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sidewalk-form/sidewalk-form.component */ "./src/app/admin/sidewalk/sidewalk-form/sidewalk-form.component.ts");






var routes = [
    {
        path: '',
        component: _sidewalk_component__WEBPACK_IMPORTED_MODULE_3__["SidewalkComponent"],
        children: [{
                path: '',
                component: _sidewalk_list_sidewalk_list_component__WEBPACK_IMPORTED_MODULE_4__["SidewalkListComponent"],
            }, {
                path: 'new',
                component: _sidewalk_form_sidewalk_form_component__WEBPACK_IMPORTED_MODULE_5__["SidewalkFormComponent"],
            }, {
                path: 'edit/:id',
                component: _sidewalk_form_sidewalk_form_component__WEBPACK_IMPORTED_MODULE_5__["SidewalkFormComponent"],
            }
        ]
    }
];
var SidewalkRoutingModule = /** @class */ (function () {
    function SidewalkRoutingModule() {
    }
    SidewalkRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], SidewalkRoutingModule);
    return SidewalkRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=sidewalk-sidewalk-module.js.map