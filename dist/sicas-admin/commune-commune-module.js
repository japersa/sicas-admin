(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["commune-commune-module"],{

/***/ "./src/app/admin/commune/commune-form/commune-form.component.html":
/*!************************************************************************!*\
  !*** ./src/app/admin/commune/commune-form/commune-form.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n      <h3 class=\"breadcrumb-header\"> Comunas </h3>\n  </div>\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n  <div class=\"col-md-12\">\n      <div class=\"panel panel-white\">\n          <div class=\"panel-heading clearfix\">\n              <h4 class=\"panel-title\"><span *ngIf=\"commune._id == null\">Nueva comuna</span>\n                 <span *ngIf=\"commune._id != null\">Editar comuna</span> </h4>\n          </div>\n          <div class=\"panel-body\">\n              <form role=\"form\"  autocomplete=\"off\" (ngSubmit)=\"f.form.valid && onSubmit(f)\" #f=\"ngForm\" novalidate>\n                 <div class=\"row\">\n                    <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !code.valid}\">\n                        <label for=\"code\" class=\"control-label\">Código</label>\n                        <input type=\"text\" placeholder=\"\" name=\"code\" #code=\"ngModel\" [(ngModel)]=\"commune.code\" class=\"form-control\" required>\n                        <span class=\"help-block\" *ngIf=\"f.submitted && !code.valid\">El código es requerido.</span>\n                    </div>\n                    <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !country.valid}\">\n                        <label for=\"country\" class=\"control-label\">País</label>\n                        <select class=\"form-control\" [(ngModel)]=\"commune.country._id\" name=\"country\" id=\"country\" #country=\"ngModel\" required>\n                            <option *ngFor=\"let country of countryList\" value=\"{{country._id}}\">{{country.description}}</option>\n                        </select>\n                            <span class=\"help-block\" *ngIf=\"f.submitted && !country.valid\">El país es requerido.</span>\n                    </div>\n                    <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !department.valid}\">\n                          <label for=\"department\" class=\"control-label\">Departamento</label>\n                          <select class=\"form-control\" [(ngModel)]=\"commune.department._id\" name=\"department\" id=\"department\" #department=\"ngModel\" required>\n                              <option *ngFor=\"let department of departmentList\" value=\"{{department._id}}\">{{department.description}}</option>\n                          </select>\n                              <span class=\"help-block\" *ngIf=\"f.submitted && !department.valid\">El departamento es requerido.</span>\n                    </div>\n                    <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !city.valid}\">\n                    <label for=\"city\" class=\"control-label\">Ciudad</label>\n                        <select class=\"form-control\" [(ngModel)]=\"commune.city._id\" name=\"city\" id=\"city\" #city=\"ngModel\" required>\n                            <option *ngFor=\"let city of cityList\" value=\"{{city._id}}\">{{city.description}}</option>\n                        </select>\n                        <span class=\"help-block\" *ngIf=\"f.submitted && !city.valid\">La ciudad es requerida.</span>\n                    </div>\n                    <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !sidewalk.valid}\">                           \n                    <label for=\"sidewalk\" class=\"control-label\">Vereda</label>\n                        <select class=\"form-control\" [(ngModel)]=\"commune.sidewalk._id\" name=\"sidewalk\" id=\"sidewalk\" #sidewalk=\"ngModel\" required>\n                            <option *ngFor=\"let sidewalk of sidewalkList\" value=\"{{sidewalk._id}}\">{{sidewalk.description}}</option>\n                        </select>\n                        <span class=\"help-block\" *ngIf=\"f.submitted && !sidewalk.valid\">La vereda es requerida.</span>\n                    </div>\n                    <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !description.valid}\">\n                      <label for=\"description\" class=\"control-label\">Descripción</label>\n                      <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"description\" #description=\"ngModel\" [(ngModel)]=\"commune.description\" required>\n                      <span class=\"help-block\" *ngIf=\"f.submitted && !description.valid\">La descripción es requerido.</span>\n                    </div>\n                    <div class=\"form-group col-md-12\">\n                      <button type=\"submit\" class=\"btn btn-primary float-right\">Guardar</button>\n                    </div>\n                 </div>\n                \n                    </form>\n          </div>\n      </div>\n   \n \n  </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/commune/commune-form/commune-form.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/admin/commune/commune-form/commune-form.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2NvbW11bmUvY29tbXVuZS1mb3JtL2NvbW11bmUtZm9ybS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/commune/commune-form/commune-form.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/admin/commune/commune-form/commune-form.component.ts ***!
  \**********************************************************************/
/*! exports provided: CommuneFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommuneFormComponent", function() { return CommuneFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _commune__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../commune */ "./src/app/admin/commune/commune.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _commune_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../commune.service */ "./src/app/admin/commune/commune.service.ts");
/* harmony import */ var _city_city__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../city/city */ "./src/app/admin/city/city.ts");
/* harmony import */ var _city_city_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../city/city.service */ "./src/app/admin/city/city.service.ts");
/* harmony import */ var _department_department__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../department/department */ "./src/app/admin/department/department.ts");
/* harmony import */ var _sidewalk_sidewalk__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../sidewalk/sidewalk */ "./src/app/admin/sidewalk/sidewalk.ts");
/* harmony import */ var _country_country__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../country/country */ "./src/app/admin/country/country.ts");










var CommuneFormComponent = /** @class */ (function () {
    function CommuneFormComponent(_router, _activatedRoute, _communeService, _cityService) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._communeService = _communeService;
        this._cityService = _cityService;
        this.commune = new _commune__WEBPACK_IMPORTED_MODULE_2__["Commune"](null, null, null, new _country_country__WEBPACK_IMPORTED_MODULE_9__["Country"](null, null, null), new _department_department__WEBPACK_IMPORTED_MODULE_7__["Department"](null, null, null, null), new _city_city__WEBPACK_IMPORTED_MODULE_5__["City"](null, null, null, null, null), new _sidewalk_sidewalk__WEBPACK_IMPORTED_MODULE_8__["Sidewalk"](null, null, null, null, null, null));
        this.cityList = [];
    }
    CommuneFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this._activatedRoute.params.subscribe(function (params) {
            if (params['id']) {
                _this.id = params['id'];
                _this.getCommune();
            }
        });
        this.getAllDepartments();
    };
    CommuneFormComponent.prototype.getCommune = function () {
        var _this = this;
        this._communeService.getCommune(this.id).then(function (data) {
            _this.commune = new _commune__WEBPACK_IMPORTED_MODULE_2__["Commune"](data._id, data.code, data.description, data.country, data.department, data.city, data.sidewalk);
        }).catch(function (error) { return console.log(error); });
    };
    CommuneFormComponent.prototype.getAllDepartments = function () {
        var _this = this;
        this._cityService.getCities()
            .then(function (data) {
            _this.cityList = data;
        })
            .catch(function (error) { return console.log(error); });
    };
    CommuneFormComponent.prototype.onSubmit = function (f) {
        var _this = this;
        this._communeService
            .saveCommune(this.commune)
            .then(function (data) {
            _this._router.navigate(['/admin/communes']);
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    CommuneFormComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    CommuneFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-commune-form',
            template: __webpack_require__(/*! ./commune-form.component.html */ "./src/app/admin/commune/commune-form/commune-form.component.html"),
            providers: [_commune_service__WEBPACK_IMPORTED_MODULE_4__["CommuneService"], _city_city_service__WEBPACK_IMPORTED_MODULE_6__["CityService"]],
            styles: [__webpack_require__(/*! ./commune-form.component.scss */ "./src/app/admin/commune/commune-form/commune-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _commune_service__WEBPACK_IMPORTED_MODULE_4__["CommuneService"],
            _city_city_service__WEBPACK_IMPORTED_MODULE_6__["CityService"]])
    ], CommuneFormComponent);
    return CommuneFormComponent;
}());



/***/ }),

/***/ "./src/app/admin/commune/commune-list/commune-list.component.html":
/*!************************************************************************!*\
  !*** ./src/app/admin/commune/commune-list/commune-list.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-7 col-6\">\n        <div class=\"page-title\">\n            <h3 class=\"breadcrumb-header\">Comunas</h3>\n          </div>\n    </div>\n\n    <div class=\"col-sm-5 col-6 text-right\">\n        <a class=\"btn btn-primary btn-rounded btn-title\" routerLink=\"new\"><i class=\"fa fa-plus\"></i> Nueva comuna</a>\n    </div>\n</div>\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n<div class=\"col-md-12\">\n    <div class=\"panel panel-white\">\n        <div class=\"panel-heading clearfix\">\n            <h4 class=\"panel-title\">Lista</h4>\n        </div>\n        <div class=\"panel-body\">\n           <div class=\"table-responsive\">\n              <table datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" class=\"display table\">\n                  <thead>\n                    <tr>\n                      <th>Código</th>\n                      <th>Descripción</th>\n                      <th>País</th>\n                      <th>Departamento</th>                      \n                      <th>Ciudad</th>\n                      <th>Vereda</th>\n                      <th>Opciones</th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <tr *ngFor=\"let item of communeList\">\n                      <td>{{ item.code }}</td>\n                      <td>{{ item.description }}</td>\n                      <td>{{ item.country.description }}</td>\n                      <td>{{ item.department.description }}</td>                      \n                      <td>{{ item.city.description }}</td>\n                      <td>{{ item.sidewalk.description }}</td>\n                      <td>\n                        <a class=\"btn btn-default btn-xs\" routerLink=\"edit/{{item._id}}\">\n                          <i class=\"fa fa-edit\"></i> Editar</a>\n                      </td>\n                    </tr>\n                  </tbody>\n                \n                </table>\n     \n            </div>\n        </div>\n    </div>\n \n\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/commune/commune-list/commune-list.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/admin/commune/commune-list/commune-list.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2NvbW11bmUvY29tbXVuZS1saXN0L2NvbW11bmUtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/commune/commune-list/commune-list.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/admin/commune/commune-list/commune-list.component.ts ***!
  \**********************************************************************/
/*! exports provided: CommuneListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommuneListComponent", function() { return CommuneListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _commune_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../commune.service */ "./src/app/admin/commune/commune.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var CommuneListComponent = /** @class */ (function () {
    function CommuneListComponent(_communeService) {
        this._communeService = _communeService;
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.communeList = [];
    }
    CommuneListComponent.prototype.ngOnInit = function () {
        this.dtOptions = {
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        };
        this.getAllCountries();
    };
    CommuneListComponent.prototype.getAllCountries = function () {
        var _this = this;
        this._communeService.getCommunes()
            .then(function (data) {
            _this.communeList = data;
            _this.dtTrigger.next();
        })
            .catch(function (error) { return console.log(error); });
    };
    CommuneListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-commune-list',
            template: __webpack_require__(/*! ./commune-list.component.html */ "./src/app/admin/commune/commune-list/commune-list.component.html"),
            providers: [_commune_service__WEBPACK_IMPORTED_MODULE_2__["CommuneService"]],
            styles: [__webpack_require__(/*! ./commune-list.component.scss */ "./src/app/admin/commune/commune-list/commune-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_commune_service__WEBPACK_IMPORTED_MODULE_2__["CommuneService"]])
    ], CommuneListComponent);
    return CommuneListComponent;
}());



/***/ }),

/***/ "./src/app/admin/commune/commune.component.html":
/*!******************************************************!*\
  !*** ./src/app/admin/commune/commune.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin/commune/commune.component.scss":
/*!******************************************************!*\
  !*** ./src/app/admin/commune/commune.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2NvbW11bmUvY29tbXVuZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/commune/commune.component.ts":
/*!****************************************************!*\
  !*** ./src/app/admin/commune/commune.component.ts ***!
  \****************************************************/
/*! exports provided: CommuneComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommuneComponent", function() { return CommuneComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CommuneComponent = /** @class */ (function () {
    function CommuneComponent() {
    }
    CommuneComponent.prototype.ngOnInit = function () {
    };
    CommuneComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-commune',
            template: __webpack_require__(/*! ./commune.component.html */ "./src/app/admin/commune/commune.component.html"),
            styles: [__webpack_require__(/*! ./commune.component.scss */ "./src/app/admin/commune/commune.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CommuneComponent);
    return CommuneComponent;
}());



/***/ }),

/***/ "./src/app/admin/commune/commune.module.ts":
/*!*************************************************!*\
  !*** ./src/app/admin/commune/commune.module.ts ***!
  \*************************************************/
/*! exports provided: CommuneModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommuneModule", function() { return CommuneModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _commune_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./commune.component */ "./src/app/admin/commune/commune.component.ts");
/* harmony import */ var _commune_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./commune.routing */ "./src/app/admin/commune/commune.routing.ts");
/* harmony import */ var _commune_list_commune_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./commune-list/commune-list.component */ "./src/app/admin/commune/commune-list/commune-list.component.ts");
/* harmony import */ var _commune_form_commune_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./commune-form/commune-form.component */ "./src/app/admin/commune/commune-form/commune-form.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");









var CommuneModule = /** @class */ (function () {
    function CommuneModule() {
    }
    CommuneModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _commune_routing__WEBPACK_IMPORTED_MODULE_5__["CommuneRoutingModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"]
            ],
            declarations: [_commune_component__WEBPACK_IMPORTED_MODULE_4__["CommuneComponent"], _commune_list_commune_list_component__WEBPACK_IMPORTED_MODULE_6__["CommuneListComponent"], _commune_form_commune_form_component__WEBPACK_IMPORTED_MODULE_7__["CommuneFormComponent"]]
        })
    ], CommuneModule);
    return CommuneModule;
}());



/***/ }),

/***/ "./src/app/admin/commune/commune.routing.ts":
/*!**************************************************!*\
  !*** ./src/app/admin/commune/commune.routing.ts ***!
  \**************************************************/
/*! exports provided: CommuneRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommuneRoutingModule", function() { return CommuneRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _commune_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./commune.component */ "./src/app/admin/commune/commune.component.ts");
/* harmony import */ var _commune_list_commune_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./commune-list/commune-list.component */ "./src/app/admin/commune/commune-list/commune-list.component.ts");
/* harmony import */ var _commune_form_commune_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./commune-form/commune-form.component */ "./src/app/admin/commune/commune-form/commune-form.component.ts");






var routes = [
    {
        path: '',
        component: _commune_component__WEBPACK_IMPORTED_MODULE_3__["CommuneComponent"],
        children: [{
                path: '',
                component: _commune_list_commune_list_component__WEBPACK_IMPORTED_MODULE_4__["CommuneListComponent"],
            }, {
                path: 'new',
                component: _commune_form_commune_form_component__WEBPACK_IMPORTED_MODULE_5__["CommuneFormComponent"],
            }, {
                path: 'edit/:id',
                component: _commune_form_commune_form_component__WEBPACK_IMPORTED_MODULE_5__["CommuneFormComponent"],
            }
        ]
    }
];
var CommuneRoutingModule = /** @class */ (function () {
    function CommuneRoutingModule() {
    }
    CommuneRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], CommuneRoutingModule);
    return CommuneRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=commune-commune-module.js.map