(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["role-role-module"],{

/***/ "./src/app/admin/role/role-form/role-form.component.css":
/*!**************************************************************!*\
  !*** ./src/app/admin/role/role-form/role-form.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3JvbGUvcm9sZS1mb3JtL3JvbGUtZm9ybS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/role/role-form/role-form.component.html":
/*!***************************************************************!*\
  !*** ./src/app/admin/role/role-form/role-form.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n      <h3 class=\"breadcrumb-header\"> Roles </h3>\n   </div>\n   <div id=\"main-wrapper\">\n      <form role=\"form\"  autocomplete=\"off\" (ngSubmit)=\"f.form.valid && onSubmit(f)\" #f=\"ngForm\" novalidate>\n      <div class=\"row\">\n         <div class=\"col-md-12\">\n            <div class=\"panel panel-white\">\n               <div class=\"panel-heading clearfix\">\n                  <h4 class=\"panel-title\">         <span *ngIf=\"role._id == null\">Nuevo rol</span>\n                     <span *ngIf=\"role._id != null\">Editar rol</span>    \n                  </h4>\n               </div>\n               <div class=\"panel-body\">\n                  <div class=\"row\">\n                     <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !name.valid}\">\n                        <label for=\"name\">Nombre</label>\n                        <input type=\"text\" class=\"form-control\" id=\"name\" [(ngModel)]=\"role.name\" name=\"name\" #name=\"ngModel\"\n                        required>\n                        <span class=\"help-block\" *ngIf=\"f.submitted && !name.valid\">El nombre es requerido.</span>\n                     </div>\n                     <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !description.valid}\">\n                        <label for=\"description\">Descripción</label>\n                        <textarea class=\"form-control\" rows=\"3\" placeholder=\"\" id=\"description\" [(ngModel)]=\"role.description\"\n                        name=\"description\" #description=\"ngModel\" required></textarea>\n                        <span class=\"help-block\" *ngIf=\"f.submitted && !description.valid\">La descripción es requerida.</span>\n                     </div>\n                  </div>\n               </div>\n            </div>\n         </div>\n         <div class=\"col-md-12\">\n            <div class=\"panel panel-white\">\n               <div class=\"panel-heading clearfix\">\n                  <h4 class=\"panel-title\"> Permisos</h4>\n               </div>\n               <div class=\"panel-body\">\n                  <div class=\"row\">\n                     <div class=\"col-md-12\">\n                        <input type=\"text\"class=\"form-control\" #search placeholder=\"Buscar permiso\" />\n                     </div>\n                  </div>\n                  \n               <label>\n                <input  class=\"form-check-input\"\n                  type=\"checkbox\"\n                  id=\"permission-all\"\n                  (change)=\"selectAll($event)\">Todos los permisos\n               </label>\n              \n                  <div class=\"row\">\n                       \n                     <div class=\"col-md-4\" *ngFor=\"let item of permissions | searchfilter: 'description' : search.value; let i = index;\">\n                        <label class=\"checkbox-inline\">\n                        <input\n                        class=\"form-check-input\"\n                        type=\"checkbox\"\n                        id=\"permission-{{item.description}}\"\n                        name=\"tagOptions\"\n                        (change)=\"changeCheckbox($event, i)\"\n                        [checked]=\"item.checked\">\n                        {{item.description}}\n                        </label>\n                     </div>\n                  </div>\n               </div>\n            </div>\n         </div>\n      </div>\n      <div class=\"row\">\n         <div class=\"col-md-12\">\n            <button type=\"submit\" class=\"btn btn-primary pull-right\">Guardar</button>\n         </div>\n      </div>\n      </form>\n   </div>"

/***/ }),

/***/ "./src/app/admin/role/role-form/role-form.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/admin/role/role-form/role-form.component.ts ***!
  \*************************************************************/
/*! exports provided: RoleFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleFormComponent", function() { return RoleFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _role_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../role.service */ "./src/app/admin/role/role.service.ts");
/* harmony import */ var _role__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../role */ "./src/app/admin/role/role.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _permission_permission_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../permission/permission.service */ "./src/app/admin/permission/permission.service.ts");






var RoleFormComponent = /** @class */ (function () {
    function RoleFormComponent(_router, _activatedRoute, _roleService, _permissionService) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._roleService = _roleService;
        this._permissionService = _permissionService;
        this.role = new _role__WEBPACK_IMPORTED_MODULE_3__["Role"](null, null, null, null);
        this.permissions = [];
    }
    RoleFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this._activatedRoute.params.subscribe(function (params) {
            if (params['id']) {
                _this.id = params['id'];
                _this.getRole();
            }
            _this.getAllPermissions();
        });
    };
    RoleFormComponent.prototype.getRole = function () {
        var _this = this;
        this._roleService.getRole(this.id).then(function (data) {
            console.log(data);
            _this.role = new _role__WEBPACK_IMPORTED_MODULE_3__["Role"](data._id, data.name, data.description, data.permissions);
            _this.checkPermission();
        }).catch(function (error) { return console.log(error); });
    };
    RoleFormComponent.prototype.checkPermission = function () {
        var _this = this;
        if (this.role.permissions != null && this.permissions != null) {
            this.role.permissions.forEach(function (permissionRole) {
                _this.permissions.forEach(function (permission, index) {
                    if (permissionRole._id == permission._id) {
                        _this.permissions[index].checked = true;
                    }
                });
            });
        }
    };
    RoleFormComponent.prototype.changeCheckbox = function (event, index) {
        this.permissions[index].checked = event.target.checked;
    };
    RoleFormComponent.prototype.selectAll = function (event) {
        for (var i = 0; i < this.permissions.length; i++) {
            this.permissions[i].checked = event.target.checked;
        }
    };
    RoleFormComponent.prototype.getAllPermissions = function () {
        var _this = this;
        this._permissionService.getPermissions()
            .then(function (data) {
            _this.permissions = data;
            _this.checkPermission();
        })
            .catch(function (error) { return console.log(error); });
    };
    RoleFormComponent.prototype.onSubmit = function (f) {
        var _this = this;
        this.role.permissions = this.permissions.filter(function (permission) {
            return permission.checked;
        });
        this._roleService
            .saveRole(this.role)
            .then(function (data) {
            _this._router.navigate(['/admin/roles']);
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    RoleFormComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    RoleFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-form',
            template: __webpack_require__(/*! ./role-form.component.html */ "./src/app/admin/role/role-form/role-form.component.html"),
            providers: [_role_service__WEBPACK_IMPORTED_MODULE_2__["RoleService"], _permission_permission_service__WEBPACK_IMPORTED_MODULE_5__["PermissionService"]],
            styles: [__webpack_require__(/*! ./role-form.component.css */ "./src/app/admin/role/role-form/role-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _role_service__WEBPACK_IMPORTED_MODULE_2__["RoleService"],
            _permission_permission_service__WEBPACK_IMPORTED_MODULE_5__["PermissionService"]])
    ], RoleFormComponent);
    return RoleFormComponent;
}());



/***/ }),

/***/ "./src/app/admin/role/role-list/role-list.component.html":
/*!***************************************************************!*\
  !*** ./src/app/admin/role/role-list/role-list.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-7 col-6\">\n        <div class=\"page-title\">\n            <h3 class=\"breadcrumb-header\">Roles</h3>\n          </div>\n    </div>\n\n    <div class=\"col-sm-5 col-6 text-right\">\n        <a class=\"btn btn-primary btn-rounded btn-title\" routerLink=\"new\"><i class=\"fa fa-plus\"></i> Nuevo rol</a>\n    </div>\n</div>\n\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n<div class=\"col-md-12\">\n    <div class=\"panel panel-white\">\n        <div class=\"panel-heading clearfix\">\n            <h4 class=\"panel-title\">Lista</h4>\n        </div>\n        <div class=\"panel-body\">\n           <div class=\"table-responsive\">\n              <table datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" class=\"display table\">\n                  <thead>\n                    <tr>\n                      <th>Nombre</th>\n                      <th>Descripción</th>\n                      <th>Opciones</th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <tr *ngFor=\"let item of roleList\">\n                      <td>{{ item.name }}</td>\n                      <td>{{ item.description }}</td>\n                      <td>\n                        <a class=\"btn btn-default btn-xs\" routerLink=\"edit/{{item._id}}\">\n                          <i class=\"fa fa-edit\"></i> Editar</a>\n                      </td>\n                    </tr>\n                  </tbody>\n                \n                </table>\n     \n            </div>\n        </div>\n    </div>\n \n\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/role/role-list/role-list.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/admin/role/role-list/role-list.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3JvbGUvcm9sZS1saXN0L3JvbGUtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/role/role-list/role-list.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/admin/role/role-list/role-list.component.ts ***!
  \*************************************************************/
/*! exports provided: RoleListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleListComponent", function() { return RoleListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _role_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../role.service */ "./src/app/admin/role/role.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var RoleListComponent = /** @class */ (function () {
    function RoleListComponent(_roleService) {
        this._roleService = _roleService;
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.roleList = [];
    }
    RoleListComponent.prototype.ngOnInit = function () {
        this.dtOptions = {
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        };
        this.getAllCountries();
    };
    RoleListComponent.prototype.getAllCountries = function () {
        var _this = this;
        this._roleService.getRoles()
            .then(function (data) {
            _this.roleList = data;
            _this.dtTrigger.next();
        })
            .catch(function (error) { return console.log(error); });
    };
    RoleListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-list',
            template: __webpack_require__(/*! ./role-list.component.html */ "./src/app/admin/role/role-list/role-list.component.html"),
            providers: [_role_service__WEBPACK_IMPORTED_MODULE_2__["RoleService"]],
            styles: [__webpack_require__(/*! ./role-list.component.scss */ "./src/app/admin/role/role-list/role-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_role_service__WEBPACK_IMPORTED_MODULE_2__["RoleService"]])
    ], RoleListComponent);
    return RoleListComponent;
}());



/***/ }),

/***/ "./src/app/admin/role/role.component.html":
/*!************************************************!*\
  !*** ./src/app/admin/role/role.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin/role/role.component.scss":
/*!************************************************!*\
  !*** ./src/app/admin/role/role.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3JvbGUvcm9sZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/role/role.component.ts":
/*!**********************************************!*\
  !*** ./src/app/admin/role/role.component.ts ***!
  \**********************************************/
/*! exports provided: RoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleComponent", function() { return RoleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var RoleComponent = /** @class */ (function () {
    function RoleComponent() {
    }
    RoleComponent.prototype.ngOnInit = function () {
    };
    RoleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role',
            template: __webpack_require__(/*! ./role.component.html */ "./src/app/admin/role/role.component.html"),
            styles: [__webpack_require__(/*! ./role.component.scss */ "./src/app/admin/role/role.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], RoleComponent);
    return RoleComponent;
}());



/***/ }),

/***/ "./src/app/admin/role/role.module.ts":
/*!*******************************************!*\
  !*** ./src/app/admin/role/role.module.ts ***!
  \*******************************************/
/*! exports provided: RoleModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleModule", function() { return RoleModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _role_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./role.component */ "./src/app/admin/role/role.component.ts");
/* harmony import */ var _role_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./role.routing */ "./src/app/admin/role/role.routing.ts");
/* harmony import */ var _role_list_role_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./role-list/role-list.component */ "./src/app/admin/role/role-list/role-list.component.ts");
/* harmony import */ var _role_form_role_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./role-form/role-form.component */ "./src/app/admin/role/role-form/role-form.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _searchfilter_pipe__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../searchfilter.pipe */ "./src/app/admin/searchfilter.pipe.ts");










var RoleModule = /** @class */ (function () {
    function RoleModule() {
    }
    RoleModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _role_routing__WEBPACK_IMPORTED_MODULE_5__["RoleRoutingModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"]
            ],
            declarations: [_role_component__WEBPACK_IMPORTED_MODULE_4__["RoleComponent"], _role_list_role_list_component__WEBPACK_IMPORTED_MODULE_6__["RoleListComponent"], _role_form_role_form_component__WEBPACK_IMPORTED_MODULE_7__["RoleFormComponent"], _searchfilter_pipe__WEBPACK_IMPORTED_MODULE_9__["SearchFilterPipe"]]
        })
    ], RoleModule);
    return RoleModule;
}());



/***/ }),

/***/ "./src/app/admin/role/role.routing.ts":
/*!********************************************!*\
  !*** ./src/app/admin/role/role.routing.ts ***!
  \********************************************/
/*! exports provided: RoleRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleRoutingModule", function() { return RoleRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _role_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./role.component */ "./src/app/admin/role/role.component.ts");
/* harmony import */ var _role_form_role_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./role-form/role-form.component */ "./src/app/admin/role/role-form/role-form.component.ts");
/* harmony import */ var _role_list_role_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./role-list/role-list.component */ "./src/app/admin/role/role-list/role-list.component.ts");






var routes = [
    {
        path: '',
        component: _role_component__WEBPACK_IMPORTED_MODULE_3__["RoleComponent"],
        children: [{
                path: '',
                component: _role_list_role_list_component__WEBPACK_IMPORTED_MODULE_5__["RoleListComponent"],
            }, {
                path: 'new',
                component: _role_form_role_form_component__WEBPACK_IMPORTED_MODULE_4__["RoleFormComponent"],
            }, {
                path: 'edit/:id',
                component: _role_form_role_form_component__WEBPACK_IMPORTED_MODULE_4__["RoleFormComponent"],
            }
        ]
    }
];
var RoleRoutingModule = /** @class */ (function () {
    function RoleRoutingModule() {
    }
    RoleRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], RoleRoutingModule);
    return RoleRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/role/role.ts":
/*!************************************!*\
  !*** ./src/app/admin/role/role.ts ***!
  \************************************/
/*! exports provided: Role */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Role", function() { return Role; });
var Role = /** @class */ (function () {
    function Role(_id, name, description, permissions) {
        this._id = _id;
        this.name = name;
        this.description = description;
        this.permissions = permissions;
    }
    return Role;
}());



/***/ }),

/***/ "./src/app/admin/searchfilter.pipe.ts":
/*!********************************************!*\
  !*** ./src/app/admin/searchfilter.pipe.ts ***!
  \********************************************/
/*! exports provided: SearchFilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchFilterPipe", function() { return SearchFilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SearchFilterPipe = /** @class */ (function () {
    function SearchFilterPipe() {
    }
    SearchFilterPipe.prototype.transform = function (value, keys, term) {
        if (!term)
            return value;
        return (value || []).filter(function (item) { return keys.split(',').some(function (key) { return item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key]); }); });
    };
    SearchFilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'searchfilter'
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], SearchFilterPipe);
    return SearchFilterPipe;
}());



/***/ })

}]);
//# sourceMappingURL=role-role-module.js.map