(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dependence-dependence-module"],{

/***/ "./src/app/admin/dependence/dependence-form/dependence-form.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/admin/dependence/dependence-form/dependence-form.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n      <h3 class=\"breadcrumb-header\"> Dependecias </h3>\n  </div>\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n  <div class=\"col-md-12\">\n      <div class=\"panel panel-white\">\n          <div class=\"panel-heading clearfix\">\n              <h4 class=\"panel-title\">         <span *ngIf=\"dependence._id == null\">Nuevo dependecia</span>\n               <span *ngIf=\"dependence._id != null\">Editar dependecia</span>    </h4>\n          </div>\n          <div class=\"panel-body\">\n              <form role=\"form\"  autocomplete=\"off\" (ngSubmit)=\"f.form.valid && onSubmit(f)\" #f=\"ngForm\" novalidate>\n                 <div class=\"row\">\n                        <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !title.valid}\">\n                            <label for=\"title\" class=\"control-label\">Título</label>\n                            <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"title\" #title=\"ngModel\" [(ngModel)]=\"dependence.title\" required />\n                            <span class=\"help-block\" *ngIf=\"f.submitted && !title.valid\">El título es requerido.</span>  \n                        </div>\n                        <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !title.valid}\">\n                            <label for=\"description\" class=\"control-label\">Descripción</label>\n                            <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"description\" #description=\"ngModel\" [(ngModel)]=\"dependence.description\" required>\n                            <span class=\"help-block\" *ngIf=\"f.submitted && !description.valid\">La descripción es requerido.</span>\n                        </div>\n                        <div class=\"form-group col-md-12\">\n                            <button type=\"submit\" class=\"btn btn-primary float-right\">Guardar</button>\n                        </div>\n                  </div>                \n                </form>\n          </div>\n      </div>\n  </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/dependence/dependence-form/dependence-form.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/admin/dependence/dependence-form/dependence-form.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2RlcGVuZGVuY2UvZGVwZW5kZW5jZS1mb3JtL2RlcGVuZGVuY2UtZm9ybS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/dependence/dependence-form/dependence-form.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/admin/dependence/dependence-form/dependence-form.component.ts ***!
  \*******************************************************************************/
/*! exports provided: DependenceFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DependenceFormComponent", function() { return DependenceFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _dependence__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../dependence */ "./src/app/admin/dependence/dependence.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dependence_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../dependence.service */ "./src/app/admin/dependence/dependence.service.ts");





var DependenceFormComponent = /** @class */ (function () {
    function DependenceFormComponent(_router, _activatedRoute, _dependenceService) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._dependenceService = _dependenceService;
        this.dependence = new _dependence__WEBPACK_IMPORTED_MODULE_2__["Dependence"](null, null, null);
    }
    DependenceFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this._activatedRoute.params.subscribe(function (params) {
            if (params['id']) {
                _this.id = params['id'];
                _this.getDependence();
            }
        });
    };
    DependenceFormComponent.prototype.getDependence = function () {
        var _this = this;
        this._dependenceService.getDependence(this.id).then(function (data) {
            _this.dependence = new _dependence__WEBPACK_IMPORTED_MODULE_2__["Dependence"](data._id, data.title, data.description);
        }).catch(function (error) { return console.log(error); });
    };
    DependenceFormComponent.prototype.onSubmit = function (f) {
        var _this = this;
        this._dependenceService
            .saveDependence(this.dependence)
            .then(function (data) {
            _this._router.navigate(['/admin/dependences']);
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DependenceFormComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    DependenceFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dependence-form',
            template: __webpack_require__(/*! ./dependence-form.component.html */ "./src/app/admin/dependence/dependence-form/dependence-form.component.html"),
            providers: [_dependence_service__WEBPACK_IMPORTED_MODULE_4__["DependenceService"]],
            styles: [__webpack_require__(/*! ./dependence-form.component.scss */ "./src/app/admin/dependence/dependence-form/dependence-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _dependence_service__WEBPACK_IMPORTED_MODULE_4__["DependenceService"]])
    ], DependenceFormComponent);
    return DependenceFormComponent;
}());



/***/ }),

/***/ "./src/app/admin/dependence/dependence-list/dependence-list.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/admin/dependence/dependence-list/dependence-list.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-7 col-6\">\n        <div class=\"page-title\">\n            <h3 class=\"breadcrumb-header\">Dependencias</h3>\n          </div>\n    </div>\n\n    <div class=\"col-sm-5 col-6 text-right\">\n        <a class=\"btn btn-primary btn-rounded btn-title\" routerLink=\"new\"><i class=\"fa fa-plus\"></i> Nuevo dependencia</a>\n    </div>\n</div>\n\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n<div class=\"col-md-12\">\n    <div class=\"panel panel-white\">\n        <div class=\"panel-heading clearfix\">\n            <h4 class=\"panel-title\">Lista</h4>\n        </div>\n        <div class=\"panel-body\">\n           <div class=\"table-responsive\">\n              <table datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" class=\"display table\">\n                  <thead>\n                    <tr>\n                      <th>ID</th>\n                      <th>Título</th>\n                      <th>Descripción</th>\n                      <th>Opciones</th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <tr *ngFor=\"let item of dependenceList\">\n                      <td>{{ item._id }}</td>\n                      <td>{{ item.title }}</td>\n                      <td>{{ item.description }}</td>\n                      <td>\n                        <a class=\"btn btn-default btn-xs\" routerLink=\"edit/{{item._id}}\">\n                          <i class=\"fa fa-edit\"></i> Editar</a>\n                      </td>\n                    </tr>\n                  </tbody>\n                \n                </table>\n     \n            </div>\n        </div>\n    </div>\n \n\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/dependence/dependence-list/dependence-list.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/admin/dependence/dependence-list/dependence-list.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2RlcGVuZGVuY2UvZGVwZW5kZW5jZS1saXN0L2RlcGVuZGVuY2UtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/dependence/dependence-list/dependence-list.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/admin/dependence/dependence-list/dependence-list.component.ts ***!
  \*******************************************************************************/
/*! exports provided: DependenceListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DependenceListComponent", function() { return DependenceListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _dependence_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../dependence.service */ "./src/app/admin/dependence/dependence.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var DependenceListComponent = /** @class */ (function () {
    function DependenceListComponent(_dependenceService) {
        this._dependenceService = _dependenceService;
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.dependenceList = [];
    }
    DependenceListComponent.prototype.ngOnInit = function () {
        this.dtOptions = {
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        };
        this.getAllDependences();
    };
    DependenceListComponent.prototype.getAllDependences = function () {
        var _this = this;
        this._dependenceService.getDependences()
            .then(function (data) {
            _this.dependenceList = data;
            _this.dtTrigger.next();
        })
            .catch(function (error) { return console.log(error); });
    };
    DependenceListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dependence-list',
            template: __webpack_require__(/*! ./dependence-list.component.html */ "./src/app/admin/dependence/dependence-list/dependence-list.component.html"),
            providers: [_dependence_service__WEBPACK_IMPORTED_MODULE_2__["DependenceService"]],
            styles: [__webpack_require__(/*! ./dependence-list.component.scss */ "./src/app/admin/dependence/dependence-list/dependence-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_dependence_service__WEBPACK_IMPORTED_MODULE_2__["DependenceService"]])
    ], DependenceListComponent);
    return DependenceListComponent;
}());



/***/ }),

/***/ "./src/app/admin/dependence/dependence.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin/dependence/dependence.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin/dependence/dependence.component.scss":
/*!************************************************************!*\
  !*** ./src/app/admin/dependence/dependence.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2RlcGVuZGVuY2UvZGVwZW5kZW5jZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/dependence/dependence.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/dependence/dependence.component.ts ***!
  \**********************************************************/
/*! exports provided: DependenceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DependenceComponent", function() { return DependenceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DependenceComponent = /** @class */ (function () {
    function DependenceComponent() {
    }
    DependenceComponent.prototype.ngOnInit = function () {
    };
    DependenceComponent.prototype.ngAfterViewInit = function () {
    };
    DependenceComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dependence',
            template: __webpack_require__(/*! ./dependence.component.html */ "./src/app/admin/dependence/dependence.component.html"),
            styles: [__webpack_require__(/*! ./dependence.component.scss */ "./src/app/admin/dependence/dependence.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DependenceComponent);
    return DependenceComponent;
}());



/***/ }),

/***/ "./src/app/admin/dependence/dependence.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/admin/dependence/dependence.module.ts ***!
  \*******************************************************/
/*! exports provided: DependenceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DependenceModule", function() { return DependenceModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _dependence_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dependence.component */ "./src/app/admin/dependence/dependence.component.ts");
/* harmony import */ var _dependence_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dependence.routing */ "./src/app/admin/dependence/dependence.routing.ts");
/* harmony import */ var _dependence_list_dependence_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dependence-list/dependence-list.component */ "./src/app/admin/dependence/dependence-list/dependence-list.component.ts");
/* harmony import */ var _dependence_form_dependence_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dependence-form/dependence-form.component */ "./src/app/admin/dependence/dependence-form/dependence-form.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");









var DependenceModule = /** @class */ (function () {
    function DependenceModule() {
    }
    DependenceModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _dependence_routing__WEBPACK_IMPORTED_MODULE_5__["DependenceRoutingModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"]
            ],
            declarations: [_dependence_component__WEBPACK_IMPORTED_MODULE_4__["DependenceComponent"], _dependence_list_dependence_list_component__WEBPACK_IMPORTED_MODULE_6__["DependenceListComponent"], _dependence_form_dependence_form_component__WEBPACK_IMPORTED_MODULE_7__["DependenceFormComponent"]]
        })
    ], DependenceModule);
    return DependenceModule;
}());



/***/ }),

/***/ "./src/app/admin/dependence/dependence.routing.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/dependence/dependence.routing.ts ***!
  \********************************************************/
/*! exports provided: DependenceRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DependenceRoutingModule", function() { return DependenceRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dependence_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dependence.component */ "./src/app/admin/dependence/dependence.component.ts");
/* harmony import */ var _dependence_list_dependence_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dependence-list/dependence-list.component */ "./src/app/admin/dependence/dependence-list/dependence-list.component.ts");
/* harmony import */ var _dependence_form_dependence_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dependence-form/dependence-form.component */ "./src/app/admin/dependence/dependence-form/dependence-form.component.ts");






var routes = [
    {
        path: '',
        component: _dependence_component__WEBPACK_IMPORTED_MODULE_3__["DependenceComponent"],
        children: [{
                path: '',
                component: _dependence_list_dependence_list_component__WEBPACK_IMPORTED_MODULE_4__["DependenceListComponent"],
            }, {
                path: 'new',
                component: _dependence_form_dependence_form_component__WEBPACK_IMPORTED_MODULE_5__["DependenceFormComponent"],
            }, {
                path: 'edit/:id',
                component: _dependence_form_dependence_form_component__WEBPACK_IMPORTED_MODULE_5__["DependenceFormComponent"],
            }
        ]
    }
];
var DependenceRoutingModule = /** @class */ (function () {
    function DependenceRoutingModule() {
    }
    DependenceRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], DependenceRoutingModule);
    return DependenceRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/dependence/dependence.service.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/dependence/dependence.service.ts ***!
  \********************************************************/
/*! exports provided: DependenceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DependenceService", function() { return DependenceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var DependenceService = /** @class */ (function () {
    function DependenceService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    DependenceService.prototype.getDependences = function () {
        var url = this.apiEndpoint + "/dependences";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DependenceService.prototype.getDependence = function (id) {
        var url = this.apiEndpoint + "/dependences/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DependenceService.prototype.saveDependence = function (dependence) {
        if (dependence._id) {
            return this.updateDependence(dependence);
        }
        return this.addDependence(dependence);
    };
    DependenceService.prototype.addDependence = function (dependence) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/dependences";
        return this.http.post(url, dependence, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DependenceService.prototype.updateDependence = function (dependence) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/dependences/" + dependence._id;
        return this.http
            .patch(url, dependence, {
            headers: headers
        })
            .toPromise()
            .then(function () { return dependence; })
            .catch(this.handleError);
    };
    DependenceService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    DependenceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], DependenceService);
    return DependenceService;
}());



/***/ }),

/***/ "./src/app/admin/dependence/dependence.ts":
/*!************************************************!*\
  !*** ./src/app/admin/dependence/dependence.ts ***!
  \************************************************/
/*! exports provided: Dependence */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Dependence", function() { return Dependence; });
var Dependence = /** @class */ (function () {
    function Dependence(_id, title, description) {
        this._id = _id;
        this.title = title;
        this.description = description;
    }
    return Dependence;
}());



/***/ })

}]);
//# sourceMappingURL=dependence-dependence-module.js.map