(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["professional-professional-module"],{

/***/ "./src/app/admin/professional/professional-form/professional-form.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/admin/professional/professional-form/professional-form.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n      <h3 class=\"breadcrumb-header\"> Profesionales </h3>\n  </div>\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n  <div class=\"col-md-12\">\n      <div class=\"panel panel-white\">\n          <div class=\"panel-heading clearfix\">\n              <h4 class=\"panel-title\"><span *ngIf=\"professional._id == null\">Nuevo profesional</span>\n                 <span *ngIf=\"professional._id != null\">Editar profesional</span> </h4>\n          </div>\n          <div class=\"panel-body\">\n              <form role=\"form\"  autocomplete=\"off\" (ngSubmit)=\"f.form.valid && onSubmit(f)\" #f=\"ngForm\" novalidate>\n                 <div class=\"row\">\n                       <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !charge.valid}\">\n                             <label for=\"professional\" class=\"control-label\">Cargo</label>\n                                <select class=\"form-control\" [(ngModel)]=\"professional.charge._id\" name=\"charge\" id=\"charge\" #charge=\"ngModel\" required>\n                                   <option *ngFor=\"let charge of chargeList\" value=\"{{charge._id}}\">{{charge.description}}</option>\n                                </select>\n                                <span class=\"help-block\" *ngIf=\"f.submitted && !charge.valid\">El cargo es requerido.</span>\n                          </div>\n                           <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !description.valid}\">\n                              <label for=\"description\" class=\"control-label\">Descripción</label>\n                              <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"description\" #description=\"ngModel\" [(ngModel)]=\"professional.description\" required>\n                              <span class=\"help-block\" *ngIf=\"f.submitted && !description.valid\">La descripción es requerido.</span>\n                           </div>\n                           <div class=\"form-group col-md-12\">\n                              <button type=\"submit\" class=\"btn btn-primary float-right\">Guardar</button>\n                           </div>\n                 </div>\n                \n                    </form>\n          </div>\n      </div>\n   \n \n  </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/professional/professional-form/professional-form.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/admin/professional/professional-form/professional-form.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3Byb2Zlc3Npb25hbC9wcm9mZXNzaW9uYWwtZm9ybS9wcm9mZXNzaW9uYWwtZm9ybS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/professional/professional-form/professional-form.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/admin/professional/professional-form/professional-form.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ProfessionalFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfessionalFormComponent", function() { return ProfessionalFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _professional__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../professional */ "./src/app/admin/professional/professional.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _professional_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../professional.service */ "./src/app/admin/professional/professional.service.ts");
/* harmony import */ var _charge_charge__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../charge/charge */ "./src/app/admin/charge/charge.ts");
/* harmony import */ var _charge_charge_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../charge/charge.service */ "./src/app/admin/charge/charge.service.ts");







var ProfessionalFormComponent = /** @class */ (function () {
    function ProfessionalFormComponent(_router, _activatedRoute, _professionalService, _chargeService) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._professionalService = _professionalService;
        this._chargeService = _chargeService;
        this.professional = new _professional__WEBPACK_IMPORTED_MODULE_2__["Professional"](null, null, new _charge_charge__WEBPACK_IMPORTED_MODULE_5__["Charge"](null, null, null));
        this.chargeList = [];
    }
    ProfessionalFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this._activatedRoute.params.subscribe(function (params) {
            if (params['id']) {
                _this.id = params['id'];
                _this.getProfessional();
            }
        });
        this.getAllCharges();
    };
    ProfessionalFormComponent.prototype.getProfessional = function () {
        var _this = this;
        this._professionalService.getProfessional(this.id).then(function (data) {
            _this.professional = new _professional__WEBPACK_IMPORTED_MODULE_2__["Professional"](data._id, data.description, data.charge);
        }).catch(function (error) { return console.log(error); });
    };
    ProfessionalFormComponent.prototype.getAllCharges = function () {
        var _this = this;
        this._chargeService.getCharges()
            .then(function (data) {
            _this.chargeList = data;
        })
            .catch(function (error) { return console.log(error); });
    };
    ProfessionalFormComponent.prototype.onSubmit = function (f) {
        var _this = this;
        this._professionalService
            .saveProfessional(this.professional)
            .then(function (data) {
            _this._router.navigate(['/admin/professionals']);
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    ProfessionalFormComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    ProfessionalFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-professional-form',
            template: __webpack_require__(/*! ./professional-form.component.html */ "./src/app/admin/professional/professional-form/professional-form.component.html"),
            providers: [_professional_service__WEBPACK_IMPORTED_MODULE_4__["ProfessionalService"], _charge_charge_service__WEBPACK_IMPORTED_MODULE_6__["ChargeService"]],
            styles: [__webpack_require__(/*! ./professional-form.component.scss */ "./src/app/admin/professional/professional-form/professional-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _professional_service__WEBPACK_IMPORTED_MODULE_4__["ProfessionalService"],
            _charge_charge_service__WEBPACK_IMPORTED_MODULE_6__["ChargeService"]])
    ], ProfessionalFormComponent);
    return ProfessionalFormComponent;
}());



/***/ }),

/***/ "./src/app/admin/professional/professional-list/professional-list.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/admin/professional/professional-list/professional-list.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-7 col-6\">\n        <div class=\"page-title\">\n            <h3 class=\"breadcrumb-header\">Profesionales</h3>\n          </div>\n    </div>\n\n    <div class=\"col-sm-5 col-6 text-right\">\n        <a class=\"btn btn-primary btn-rounded btn-title\" routerLink=\"new\"><i class=\"fa fa-plus\"></i> Nuevo profesional</a>\n    </div>\n</div>\n\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n<div class=\"col-md-12\">\n    <div class=\"panel panel-white\">\n        <div class=\"panel-heading clearfix\">\n            <h4 class=\"panel-title\">Lista</h4>\n        </div>\n        <div class=\"panel-body\">\n           <div class=\"table-responsive\">\n              <table datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" class=\"display table\">\n                  <thead>\n                      <tr>\n                        <th>Código</th>\n                        <th>Descripción</th>\n                        <th>Cargo</th>\n                        <th>Opciones</th>\n                      </tr>\n                    </thead>\n                    <tbody>\n                      <tr *ngFor=\"let item of professionalList\">\n                        <td> {{ item._id }}</td>\n                        <td>{{ item.description }}</td>\n                        <td>{{ item.charge.description }}</td>\n                      <td>\n                        <a class=\"btn btn-default btn-xs\" routerLink=\"edit/{{item._id}}\">\n                          <i class=\"fa fa-edit\"></i> Editar</a>\n                      </td>\n                    </tr>\n                  </tbody>\n                \n                </table>\n     \n            </div>\n        </div>\n    </div>\n \n\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/professional/professional-list/professional-list.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/admin/professional/professional-list/professional-list.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3Byb2Zlc3Npb25hbC9wcm9mZXNzaW9uYWwtbGlzdC9wcm9mZXNzaW9uYWwtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/professional/professional-list/professional-list.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/admin/professional/professional-list/professional-list.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ProfessionalListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfessionalListComponent", function() { return ProfessionalListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _professional_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../professional.service */ "./src/app/admin/professional/professional.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var ProfessionalListComponent = /** @class */ (function () {
    function ProfessionalListComponent(_professionalService) {
        this._professionalService = _professionalService;
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.professionalList = [];
    }
    ProfessionalListComponent.prototype.ngOnInit = function () {
        this.dtOptions = {
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        };
        this.getAllCountries();
    };
    ProfessionalListComponent.prototype.getAllCountries = function () {
        var _this = this;
        this._professionalService.getProfessionals()
            .then(function (data) {
            _this.professionalList = data;
            _this.dtTrigger.next();
        })
            .catch(function (error) { return console.log(error); });
    };
    ProfessionalListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-professional-list',
            template: __webpack_require__(/*! ./professional-list.component.html */ "./src/app/admin/professional/professional-list/professional-list.component.html"),
            providers: [_professional_service__WEBPACK_IMPORTED_MODULE_2__["ProfessionalService"]],
            styles: [__webpack_require__(/*! ./professional-list.component.scss */ "./src/app/admin/professional/professional-list/professional-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_professional_service__WEBPACK_IMPORTED_MODULE_2__["ProfessionalService"]])
    ], ProfessionalListComponent);
    return ProfessionalListComponent;
}());



/***/ }),

/***/ "./src/app/admin/professional/professional.component.html":
/*!****************************************************************!*\
  !*** ./src/app/admin/professional/professional.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin/professional/professional.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/admin/professional/professional.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3Byb2Zlc3Npb25hbC9wcm9mZXNzaW9uYWwuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/professional/professional.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/admin/professional/professional.component.ts ***!
  \**************************************************************/
/*! exports provided: ProfessionalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfessionalComponent", function() { return ProfessionalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ProfessionalComponent = /** @class */ (function () {
    function ProfessionalComponent() {
    }
    ProfessionalComponent.prototype.ngOnInit = function () {
    };
    ProfessionalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-professional',
            template: __webpack_require__(/*! ./professional.component.html */ "./src/app/admin/professional/professional.component.html"),
            styles: [__webpack_require__(/*! ./professional.component.scss */ "./src/app/admin/professional/professional.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProfessionalComponent);
    return ProfessionalComponent;
}());



/***/ }),

/***/ "./src/app/admin/professional/professional.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/admin/professional/professional.module.ts ***!
  \***********************************************************/
/*! exports provided: ProfessionalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfessionalModule", function() { return ProfessionalModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _professional_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./professional.component */ "./src/app/admin/professional/professional.component.ts");
/* harmony import */ var _professional_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./professional.routing */ "./src/app/admin/professional/professional.routing.ts");
/* harmony import */ var _professional_list_professional_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./professional-list/professional-list.component */ "./src/app/admin/professional/professional-list/professional-list.component.ts");
/* harmony import */ var _professional_form_professional_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./professional-form/professional-form.component */ "./src/app/admin/professional/professional-form/professional-form.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");









var ProfessionalModule = /** @class */ (function () {
    function ProfessionalModule() {
    }
    ProfessionalModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _professional_routing__WEBPACK_IMPORTED_MODULE_5__["ProfessionalRoutingModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"]
            ],
            declarations: [_professional_component__WEBPACK_IMPORTED_MODULE_4__["ProfessionalComponent"], _professional_list_professional_list_component__WEBPACK_IMPORTED_MODULE_6__["ProfessionalListComponent"], _professional_form_professional_form_component__WEBPACK_IMPORTED_MODULE_7__["ProfessionalFormComponent"]]
        })
    ], ProfessionalModule);
    return ProfessionalModule;
}());



/***/ }),

/***/ "./src/app/admin/professional/professional.routing.ts":
/*!************************************************************!*\
  !*** ./src/app/admin/professional/professional.routing.ts ***!
  \************************************************************/
/*! exports provided: ProfessionalRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfessionalRoutingModule", function() { return ProfessionalRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _professional_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./professional.component */ "./src/app/admin/professional/professional.component.ts");
/* harmony import */ var _professional_form_professional_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./professional-form/professional-form.component */ "./src/app/admin/professional/professional-form/professional-form.component.ts");
/* harmony import */ var _professional_list_professional_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./professional-list/professional-list.component */ "./src/app/admin/professional/professional-list/professional-list.component.ts");






var routes = [
    {
        path: '',
        component: _professional_component__WEBPACK_IMPORTED_MODULE_3__["ProfessionalComponent"],
        children: [{
                path: '',
                component: _professional_list_professional_list_component__WEBPACK_IMPORTED_MODULE_5__["ProfessionalListComponent"],
            }, {
                path: 'new',
                component: _professional_form_professional_form_component__WEBPACK_IMPORTED_MODULE_4__["ProfessionalFormComponent"],
            }, {
                path: 'edit/:id',
                component: _professional_form_professional_form_component__WEBPACK_IMPORTED_MODULE_4__["ProfessionalFormComponent"],
            }
        ]
    }
];
var ProfessionalRoutingModule = /** @class */ (function () {
    function ProfessionalRoutingModule() {
    }
    ProfessionalRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], ProfessionalRoutingModule);
    return ProfessionalRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/professional/professional.service.ts":
/*!************************************************************!*\
  !*** ./src/app/admin/professional/professional.service.ts ***!
  \************************************************************/
/*! exports provided: ProfessionalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfessionalService", function() { return ProfessionalService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var ProfessionalService = /** @class */ (function () {
    function ProfessionalService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    ProfessionalService.prototype.getProfessionals = function () {
        var url = this.apiEndpoint + "/professionals";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfessionalService.prototype.getProfessional = function (id) {
        var url = this.apiEndpoint + "/professionals/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfessionalService.prototype.saveProfessional = function (professional) {
        if (professional._id) {
            return this.updateProfessional(professional);
        }
        return this.addProfessional(professional);
    };
    ProfessionalService.prototype.addProfessional = function (professional) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/professionals";
        return this.http.post(url, professional, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfessionalService.prototype.updateProfessional = function (professional) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/professionals/" + professional._id;
        return this.http
            .patch(url, professional, {
            headers: headers
        })
            .toPromise()
            .then(function () { return professional; })
            .catch(this.handleError);
    };
    ProfessionalService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    ProfessionalService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], ProfessionalService);
    return ProfessionalService;
}());



/***/ }),

/***/ "./src/app/admin/professional/professional.ts":
/*!****************************************************!*\
  !*** ./src/app/admin/professional/professional.ts ***!
  \****************************************************/
/*! exports provided: Professional */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Professional", function() { return Professional; });
var Professional = /** @class */ (function () {
    function Professional(_id, description, charge) {
        this._id = _id;
        this.description = description;
        this.charge = charge;
    }
    return Professional;
}());



/***/ })

}]);
//# sourceMappingURL=professional-professional-module.js.map