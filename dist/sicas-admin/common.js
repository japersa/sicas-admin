(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/admin/charge/charge.service.ts":
/*!************************************************!*\
  !*** ./src/app/admin/charge/charge.service.ts ***!
  \************************************************/
/*! exports provided: ChargeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChargeService", function() { return ChargeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var ChargeService = /** @class */ (function () {
    function ChargeService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    ChargeService.prototype.getCharges = function () {
        var url = this.apiEndpoint + "/charges";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ChargeService.prototype.getCharge = function (id) {
        var url = this.apiEndpoint + "/charges/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ChargeService.prototype.saveCharge = function (department) {
        if (department._id) {
            return this.updateCharge(department);
        }
        return this.addCharge(department);
    };
    ChargeService.prototype.addCharge = function (department) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/charges";
        return this.http.post(url, department, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ChargeService.prototype.updateCharge = function (department) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/charges/" + department._id;
        return this.http
            .patch(url, department, {
            headers: headers
        })
            .toPromise()
            .then(function () { return department; })
            .catch(this.handleError);
    };
    ChargeService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    ChargeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], ChargeService);
    return ChargeService;
}());



/***/ }),

/***/ "./src/app/admin/charge/charge.ts":
/*!****************************************!*\
  !*** ./src/app/admin/charge/charge.ts ***!
  \****************************************/
/*! exports provided: Charge */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Charge", function() { return Charge; });
var Charge = /** @class */ (function () {
    function Charge(_id, code, description) {
        this._id = _id;
        this.code = code;
        this.description = description;
    }
    return Charge;
}());



/***/ }),

/***/ "./src/app/admin/city/city.service.ts":
/*!********************************************!*\
  !*** ./src/app/admin/city/city.service.ts ***!
  \********************************************/
/*! exports provided: CityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CityService", function() { return CityService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var CityService = /** @class */ (function () {
    function CityService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    CityService.prototype.getCities = function () {
        var url = this.apiEndpoint + "/cities";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CityService.prototype.getCitiesDepartment = function (id) {
        var url = this.apiEndpoint + "/cities/department/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CityService.prototype.getCity = function (id) {
        var url = this.apiEndpoint + "/cities/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CityService.prototype.saveCity = function (city) {
        if (city._id) {
            return this.updateCity(city);
        }
        return this.addCity(city);
    };
    CityService.prototype.addCity = function (city) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/cities";
        return this.http.post(url, city, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CityService.prototype.updateCity = function (city) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/cities/" + city._id;
        return this.http
            .patch(url, city, {
            headers: headers
        })
            .toPromise()
            .then(function () { return city; })
            .catch(this.handleError);
    };
    CityService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    CityService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], CityService);
    return CityService;
}());



/***/ }),

/***/ "./src/app/admin/city/city.ts":
/*!************************************!*\
  !*** ./src/app/admin/city/city.ts ***!
  \************************************/
/*! exports provided: City */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "City", function() { return City; });
var City = /** @class */ (function () {
    function City(_id, code, country, department, description) {
        this._id = _id;
        this.code = code;
        this.country = country;
        this.department = department;
        this.description = description;
    }
    return City;
}());



/***/ }),

/***/ "./src/app/admin/commune/commune.service.ts":
/*!**************************************************!*\
  !*** ./src/app/admin/commune/commune.service.ts ***!
  \**************************************************/
/*! exports provided: CommuneService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommuneService", function() { return CommuneService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var CommuneService = /** @class */ (function () {
    function CommuneService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    CommuneService.prototype.getCommunes = function () {
        var url = this.apiEndpoint + "/communes";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CommuneService.prototype.getCommunesCity = function (id) {
        var url = this.apiEndpoint + "/communes/city/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CommuneService.prototype.getCommune = function (id) {
        var url = this.apiEndpoint + "/communes/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CommuneService.prototype.saveCommune = function (commune) {
        if (commune._id) {
            return this.updateCommune(commune);
        }
        return this.addCommune(commune);
    };
    CommuneService.prototype.addCommune = function (commune) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/communes";
        return this.http.post(url, commune, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CommuneService.prototype.updateCommune = function (commune) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/communes/" + commune._id;
        return this.http
            .patch(url, commune, {
            headers: headers
        })
            .toPromise()
            .then(function () { return commune; })
            .catch(this.handleError);
    };
    CommuneService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    CommuneService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], CommuneService);
    return CommuneService;
}());



/***/ }),

/***/ "./src/app/admin/commune/commune.ts":
/*!******************************************!*\
  !*** ./src/app/admin/commune/commune.ts ***!
  \******************************************/
/*! exports provided: Commune */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Commune", function() { return Commune; });
var Commune = /** @class */ (function () {
    function Commune(_id, code, description, country, department, city, sidewalk) {
        this._id = _id;
        this.code = code;
        this.description = description;
        this.country = country;
        this.department = department;
        this.city = city;
        this.sidewalk = sidewalk;
    }
    return Commune;
}());



/***/ }),

/***/ "./src/app/admin/country/country.service.ts":
/*!**************************************************!*\
  !*** ./src/app/admin/country/country.service.ts ***!
  \**************************************************/
/*! exports provided: CountryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountryService", function() { return CountryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var CountryService = /** @class */ (function () {
    function CountryService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    CountryService.prototype.getCountries = function () {
        var url = this.apiEndpoint + "/countries";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CountryService.prototype.getCountry = function (id) {
        var url = this.apiEndpoint + "/countries/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CountryService.prototype.saveCountry = function (country) {
        if (country._id) {
            return this.updateCountry(country);
        }
        return this.addCountry(country);
    };
    CountryService.prototype.addCountry = function (country) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/countries";
        return this.http.post(url, country, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CountryService.prototype.updateCountry = function (country) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/countries/" + country._id;
        return this.http
            .patch(url, country, {
            headers: headers
        })
            .toPromise()
            .then(function () { return country; })
            .catch(this.handleError);
    };
    CountryService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    CountryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], CountryService);
    return CountryService;
}());



/***/ }),

/***/ "./src/app/admin/country/country.ts":
/*!******************************************!*\
  !*** ./src/app/admin/country/country.ts ***!
  \******************************************/
/*! exports provided: Country */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Country", function() { return Country; });
var Country = /** @class */ (function () {
    function Country(_id, code, description) {
        this._id = _id;
        this.code = code;
        this.description = description;
    }
    return Country;
}());



/***/ }),

/***/ "./src/app/admin/department/department.service.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/department/department.service.ts ***!
  \********************************************************/
/*! exports provided: DepartmentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DepartmentService", function() { return DepartmentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var DepartmentService = /** @class */ (function () {
    function DepartmentService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    DepartmentService.prototype.getDepartments = function () {
        var url = this.apiEndpoint + "/departments";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DepartmentService.prototype.getDepartmentsCountry = function (id) {
        var url = this.apiEndpoint + "/departments/country/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DepartmentService.prototype.getDepartment = function (id) {
        var url = this.apiEndpoint + "/departments/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DepartmentService.prototype.saveDepartment = function (department) {
        if (department._id) {
            return this.updateDepartment(department);
        }
        return this.addDepartment(department);
    };
    DepartmentService.prototype.addDepartment = function (department) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/departments";
        return this.http.post(url, department, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DepartmentService.prototype.updateDepartment = function (department) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/departments/" + department._id;
        return this.http
            .patch(url, department, {
            headers: headers
        })
            .toPromise()
            .then(function () { return department; })
            .catch(this.handleError);
    };
    DepartmentService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    DepartmentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], DepartmentService);
    return DepartmentService;
}());



/***/ }),

/***/ "./src/app/admin/department/department.ts":
/*!************************************************!*\
  !*** ./src/app/admin/department/department.ts ***!
  \************************************************/
/*! exports provided: Department */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Department", function() { return Department; });
var Department = /** @class */ (function () {
    function Department(_id, code, description, country) {
        this._id = _id;
        this.code = code;
        this.description = description;
        this.country = country;
    }
    return Department;
}());



/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood.service.ts":
/*!************************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood.service.ts ***!
  \************************************************************/
/*! exports provided: NeighborhoodService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NeighborhoodService", function() { return NeighborhoodService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var NeighborhoodService = /** @class */ (function () {
    function NeighborhoodService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    NeighborhoodService.prototype.getNeighborhoods = function () {
        var url = this.apiEndpoint + "/neighborhoods";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    NeighborhoodService.prototype.getNeighborhoodsCommune = function (id) {
        var url = this.apiEndpoint + "/neighborhoods/commune/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    NeighborhoodService.prototype.getNeighborhood = function (id) {
        var url = this.apiEndpoint + "/neighborhoods/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    NeighborhoodService.prototype.saveNeighborhood = function (neighborhood) {
        if (neighborhood._id) {
            return this.updateNeighborhood(neighborhood);
        }
        return this.addNeighborhood(neighborhood);
    };
    NeighborhoodService.prototype.addNeighborhood = function (neighborhood) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/neighborhoods";
        return this.http.post(url, neighborhood, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    NeighborhoodService.prototype.updateNeighborhood = function (neighborhood) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/neighborhoods/" + neighborhood._id;
        return this.http
            .patch(url, neighborhood, {
            headers: headers
        })
            .toPromise()
            .then(function () { return neighborhood; })
            .catch(this.handleError);
    };
    NeighborhoodService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    NeighborhoodService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], NeighborhoodService);
    return NeighborhoodService;
}());



/***/ }),

/***/ "./src/app/admin/neighborhood/neighborhood.ts":
/*!****************************************************!*\
  !*** ./src/app/admin/neighborhood/neighborhood.ts ***!
  \****************************************************/
/*! exports provided: Neighborhood */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Neighborhood", function() { return Neighborhood; });
var Neighborhood = /** @class */ (function () {
    function Neighborhood(_id, code, description, country, department, city, sidewalk, commune) {
        this._id = _id;
        this.code = code;
        this.description = description;
        this.country = country;
        this.department = department;
        this.city = city;
        this.sidewalk = sidewalk;
        this.commune = commune;
    }
    return Neighborhood;
}());



/***/ }),

/***/ "./src/app/admin/permission/permission.service.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/permission/permission.service.ts ***!
  \********************************************************/
/*! exports provided: PermissionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionService", function() { return PermissionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var PermissionService = /** @class */ (function () {
    function PermissionService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    PermissionService.prototype.getPermissions = function () {
        var url = this.apiEndpoint + "/permissions";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    PermissionService.prototype.getPermission = function (id) {
        var url = this.apiEndpoint + "/permissions/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    PermissionService.prototype.savePermission = function (permission) {
        if (permission._id) {
            return this.updatePermission(permission);
        }
        return this.addPermission(permission);
    };
    PermissionService.prototype.addPermission = function (permission) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/permissions";
        return this.http.post(url, permission, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    PermissionService.prototype.updatePermission = function (permission) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/permissions/" + permission._id;
        return this.http
            .patch(url, permission, {
            headers: headers
        })
            .toPromise()
            .then(function () { return permission; })
            .catch(this.handleError);
    };
    PermissionService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    PermissionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], PermissionService);
    return PermissionService;
}());



/***/ }),

/***/ "./src/app/admin/role/role.service.ts":
/*!********************************************!*\
  !*** ./src/app/admin/role/role.service.ts ***!
  \********************************************/
/*! exports provided: RoleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleService", function() { return RoleService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var RoleService = /** @class */ (function () {
    function RoleService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    RoleService.prototype.getRoles = function () {
        var url = this.apiEndpoint + "/roles";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    RoleService.prototype.getRole = function (id) {
        var url = this.apiEndpoint + "/roles/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    RoleService.prototype.saveRole = function (role) {
        if (role._id) {
            return this.updateRole(role);
        }
        return this.addRole(role);
    };
    RoleService.prototype.addRole = function (role) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/roles";
        return this.http.post(url, role, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    RoleService.prototype.updateRole = function (role) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/roles/" + role._id;
        return this.http
            .patch(url, role, {
            headers: headers
        })
            .toPromise()
            .then(function () { return role; })
            .catch(this.handleError);
    };
    RoleService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    RoleService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], RoleService);
    return RoleService;
}());



/***/ }),

/***/ "./src/app/admin/sector/sector.service.ts":
/*!************************************************!*\
  !*** ./src/app/admin/sector/sector.service.ts ***!
  \************************************************/
/*! exports provided: SectorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SectorService", function() { return SectorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var SectorService = /** @class */ (function () {
    function SectorService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    SectorService.prototype.getSectors = function () {
        var url = this.apiEndpoint + "/sectors";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SectorService.prototype.getSector = function (id) {
        var url = this.apiEndpoint + "/sectors/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SectorService.prototype.saveSector = function (sector) {
        if (sector._id) {
            return this.updateSector(sector);
        }
        return this.addSector(sector);
    };
    SectorService.prototype.addSector = function (sector) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/sectors";
        return this.http.post(url, sector, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SectorService.prototype.updateSector = function (sector) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/sectors/" + sector._id;
        return this.http
            .patch(url, sector, {
            headers: headers
        })
            .toPromise()
            .then(function () { return sector; })
            .catch(this.handleError);
    };
    SectorService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    SectorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], SectorService);
    return SectorService;
}());



/***/ }),

/***/ "./src/app/admin/sector/sector.ts":
/*!****************************************!*\
  !*** ./src/app/admin/sector/sector.ts ***!
  \****************************************/
/*! exports provided: Sector */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Sector", function() { return Sector; });
var Sector = /** @class */ (function () {
    function Sector(_id, code, description) {
        this._id = _id;
        this.code = code;
        this.description = description;
    }
    return Sector;
}());



/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk.service.ts":
/*!****************************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk.service.ts ***!
  \****************************************************/
/*! exports provided: SidewalkService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidewalkService", function() { return SidewalkService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var SidewalkService = /** @class */ (function () {
    function SidewalkService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    SidewalkService.prototype.getSidewalks = function () {
        var url = this.apiEndpoint + "/sidewalks";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SidewalkService.prototype.getSidewalksCity = function (id) {
        var url = this.apiEndpoint + "/sidewalks/city/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SidewalkService.prototype.getSidewalk = function (id) {
        var url = this.apiEndpoint + "/sidewalks/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SidewalkService.prototype.saveSidewalk = function (sidewalk) {
        if (sidewalk._id) {
            return this.updateSidewalk(sidewalk);
        }
        return this.addSidewalk(sidewalk);
    };
    SidewalkService.prototype.addSidewalk = function (sidewalk) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/sidewalks";
        return this.http.post(url, sidewalk, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SidewalkService.prototype.updateSidewalk = function (sidewalk) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/sidewalks/" + sidewalk._id;
        return this.http
            .patch(url, sidewalk, {
            headers: headers
        })
            .toPromise()
            .then(function () { return sidewalk; })
            .catch(this.handleError);
    };
    SidewalkService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    SidewalkService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], SidewalkService);
    return SidewalkService;
}());



/***/ }),

/***/ "./src/app/admin/sidewalk/sidewalk.ts":
/*!********************************************!*\
  !*** ./src/app/admin/sidewalk/sidewalk.ts ***!
  \********************************************/
/*! exports provided: Sidewalk */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Sidewalk", function() { return Sidewalk; });
var Sidewalk = /** @class */ (function () {
    function Sidewalk(_id, code, country, department, city, description) {
        this._id = _id;
        this.code = code;
        this.country = country;
        this.department = department;
        this.city = city;
        this.description = description;
    }
    return Sidewalk;
}());



/***/ }),

/***/ "./src/app/admin/subsidy-type/subsidy-type.service.ts":
/*!************************************************************!*\
  !*** ./src/app/admin/subsidy-type/subsidy-type.service.ts ***!
  \************************************************************/
/*! exports provided: SubsidyTypeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubsidyTypeService", function() { return SubsidyTypeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var SubsidyTypeService = /** @class */ (function () {
    function SubsidyTypeService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    SubsidyTypeService.prototype.getSubsidyTypes = function () {
        var url = this.apiEndpoint + "/subsidy-types";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SubsidyTypeService.prototype.getSubsidyType = function (id) {
        var url = this.apiEndpoint + "/subsidy-types/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SubsidyTypeService.prototype.saveSubsidyType = function (department) {
        if (department._id) {
            return this.updateSubsidyType(department);
        }
        return this.addSubsidyType(department);
    };
    SubsidyTypeService.prototype.addSubsidyType = function (department) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/subsidy-types";
        return this.http.post(url, department, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SubsidyTypeService.prototype.updateSubsidyType = function (department) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/subsidy-types/" + department._id;
        return this.http
            .patch(url, department, {
            headers: headers
        })
            .toPromise()
            .then(function () { return department; })
            .catch(this.handleError);
    };
    SubsidyTypeService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    SubsidyTypeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], SubsidyTypeService);
    return SubsidyTypeService;
}());



/***/ }),

/***/ "./src/app/admin/subsidy-type/subsidy-type.ts":
/*!****************************************************!*\
  !*** ./src/app/admin/subsidy-type/subsidy-type.ts ***!
  \****************************************************/
/*! exports provided: SubsidyType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubsidyType", function() { return SubsidyType; });
var SubsidyType = /** @class */ (function () {
    function SubsidyType(_id, description) {
        this._id = _id;
        this.description = description;
    }
    return SubsidyType;
}());



/***/ }),

/***/ "./src/app/admin/survey-type/survey-type.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type.service.ts ***!
  \**********************************************************/
/*! exports provided: SurveyTypeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyTypeService", function() { return SurveyTypeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var SurveyTypeService = /** @class */ (function () {
    function SurveyTypeService(http) {
        this.http = http;
        this.apiEndpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    SurveyTypeService.prototype.getSurveyTypes = function () {
        var url = this.apiEndpoint + "/survey-types";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SurveyTypeService.prototype.getSurveyType = function (id) {
        var url = this.apiEndpoint + "/survey-types/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SurveyTypeService.prototype.saveSurveyType = function (department) {
        if (department._id) {
            return this.updateSurveyType(department);
        }
        return this.addSurveyType(department);
    };
    SurveyTypeService.prototype.addSurveyType = function (department) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/survey-types";
        return this.http.post(url, department, {
            headers: headers
        })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SurveyTypeService.prototype.updateSurveyType = function (department) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var url = this.apiEndpoint + "/survey-types/" + department._id;
        return this.http
            .patch(url, department, {
            headers: headers
        })
            .toPromise()
            .then(function () { return department; })
            .catch(this.handleError);
    };
    SurveyTypeService.prototype.handleError = function (error) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    };
    SurveyTypeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], SurveyTypeService);
    return SurveyTypeService;
}());



/***/ }),

/***/ "./src/app/admin/survey-type/survey-type.ts":
/*!**************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type.ts ***!
  \**************************************************/
/*! exports provided: SurveyType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyType", function() { return SurveyType; });
var SurveyType = /** @class */ (function () {
    function SurveyType(_id, modifiable, multiple, field, answer) {
        this._id = _id;
        this.modifiable = modifiable;
        this.multiple = multiple;
        this.field = field;
        this.answer = answer;
    }
    return SurveyType;
}());



/***/ })

}]);
//# sourceMappingURL=common.js.map