(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["question-manager-question-manager-module"],{

/***/ "./src/app/admin/question-manager/question-manager.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/admin/question-manager/question-manager.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3F1ZXN0aW9uLW1hbmFnZXIvcXVlc3Rpb24tbWFuYWdlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/question-manager/question-manager.component.html":
/*!************************************************************************!*\
  !*** ./src/app/admin/question-manager/question-manager.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-7 col-6\">\n        <div class=\"page-title\">\n            <h3 class=\"breadcrumb-header\">Gestor de preguntas</h3>\n          </div>\n    </div>\n</div>\n\n\n<div id=\"main-wrapper\">\n\n  <div class=\"row\">\n  <div class=\"col-md-7\">\n      <div class=\"panel panel-white\">\n          <div class=\"panel-heading clearfix\">\n              <h4 class=\"panel-title\">Lista\n              </h4>\n              <div style=\"margin-top: -50px; float: right;\">\n                <a class=\"btn btn-primary btn-rounded btn-action-panel\" style=\"margin-right: 0;\" data-toggle=\"modal\" data-target=\"#questionModal2\" ><i class=\"fa fa-plus\"></i> Nueva categoria</a>\n             </div>\n          </div>\n          <div class=\"panel-body\">\n            <div class=\"panel-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">\n              <p class=\"text-center\" *ngIf=\"data.length == 0\"><i class=\"fa fa-warning fa-3x\"></i> <br> No existen categorías </p>\n              <app-ui-tree [data]=\"data\" [key]=\"key\"></app-ui-tree>\n\n                                          \n             </div>   \n          </div>\n      </div>\n   \n  \n  </div>\n  <div class=\"col-md-5\">   \n    <div class=\"panel border-info mb-3\">\n      <div class=\"panel-header text-center\">\n        <div class=\"panel-heading clearfix \">\n          <i class=\"fa fa-info-circle fa-3x\"></i>\n        </div> \n        <h3>INFORMACIÓN</h3>\n      </div> \n      <hr>\n    <div>\n      <p>A continuación se presenta la ayuda para la administración de preguntas</p>\n    </div> \n\n    <div id=\"accordion2\">\n        <div class=\"panel panel-default\">\n          <div class=\"panel-heading\" role=\"tab\" id=\"headingOne1\">\n              <h4 class=\"panel-title\">\n                  <a data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"#a1\" aria-expanded=\"false\" aria-controls=\"collapseOne\" class=\"collapsed\">\n                      Insertar una pregunta\n                  </a>\n              </h4>\n          </div>\n          <div id=\"a1\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingOne1\" aria-expanded=\"false\" style=\"height: 0px;\">\n              <div class=\"panel-body\">\n                  Para insertar una pregunta debe hacer clic en el ícono <i class=\"fa fa-plus fa-xs\"></i> de la parte superior derecha \n              </div>\n          </div>\n      </div>\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\" role=\"tab\" id=\"headingtwo2\">\n            <h4 class=\"panel-title\">\n                <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#a2\" aria-expanded=\"false\" aria-controls=\"collapseTwo\" class=\"collapsed\">\n                    Editar una pregunta\n                </a>\n            </h4>\n        </div>\n        <div id=\"a2\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingtwo2\" aria-expanded=\"false\" style=\"height: 0px;\">\n            <div class=\"panel-body\">\n                Para editar una pregunta debe hacer clic...\n            </div>\n        </div>\n    </div>\n\n    <div class=\"panel panel-default\">\n      <div class=\"panel-heading\" role=\"tab\" id=\"headingThree3\">\n          <h4 class=\"panel-title\">\n              <a data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"#a3\" aria-expanded=\"false\" aria-controls=\"collapseThree\" class=\"collapsed\">\n                  Borrar una pregunta\n              </a>\n          </h4>\n      </div>\n      <div id=\"a3\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree3\" aria-expanded=\"false\" style=\"height: 0px;\">\n          <div class=\"panel-body\">\n              Para borrar una pregunta debe hacer clic...\n          </div>\n      </div>\n  </div>\n\n      </div>\n    </div>\n    </div> \n  </div>\n  </div>\n\n\n\n<div class=\"modal fade in\" id=\"questionModal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n              <h4 class=\"modal-title\" id=\"myModalLabel\">\n                    <span>Nueva Categoría</span>\n              </h4>\n          </div>\n          <div class=\"modal-body\">\n            <label for=\"name\">Categoría</label>\n            <input id=\"idCategory\" type=\"text\" focus class=\"form-control\" [(ngModel)]=\"category\">\n          </div>\n          <div class=\"modal-footer\">\n              <button type=\"button\" (click)=\"confirmAddQuestion(category)\" class=\"btn btn-primary\" data-dismiss=\"modal\">Aceptar</button> &nbsp;\n             <button type=\"button\" (click)=\"closeModal()\" class=\"btn btn-default\">Cancelar</button>\n          </div>\n      </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/admin/question-manager/question-manager.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/admin/question-manager/question-manager.component.ts ***!
  \**********************************************************************/
/*! exports provided: QuestionManagerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionManagerComponent", function() { return QuestionManagerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var QuestionManagerComponent = /** @class */ (function () {
    function QuestionManagerComponent() {
        this.key = 'categories';
        this.data = [];
        this.questionsList = [];
        this.index = null;
        this.patern = null;
    }
    QuestionManagerComponent.prototype.ngOnInit = function () {
    };
    QuestionManagerComponent.prototype.AddCategory = function (category) {
        this.data.push({ name: category, categories: [] });
        $('#idCategory').val('');
        this.index = null;
        console.log(category);
    };
    QuestionManagerComponent.prototype.addQuestion = function (indice) {
        console.log(indice);
        this.openSurveyModal();
        this.index = indice;
        this.patern = this.questionsList[indice].name;
        console.log(this.patern);
    };
    QuestionManagerComponent.prototype.confirmAddQuestion = function (question) {
        this.data.push({
            name: question,
            categories: []
        });
        $('#idCategory').val('');
        this.closeSurveyModal();
    };
    QuestionManagerComponent.prototype.deleteCategory = function (index) {
        if (index > -1) {
            this.questionsList.splice(index, 1);
        }
    };
    QuestionManagerComponent.prototype.openModal = function () {
        $('#questionModal2').modal('show');
    };
    QuestionManagerComponent.prototype.closeModal = function () {
        $('#questionModal2').modal('hide');
        $('#idCategory').val('');
        this.index = null;
    };
    QuestionManagerComponent.prototype.openSurveyModal = function () {
        $('#surveyModal').modal('show');
    };
    QuestionManagerComponent.prototype.closeSurveyModal = function () {
        $('#surveyModal').modal('hide');
    };
    QuestionManagerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-question-manager',
            template: __webpack_require__(/*! ./question-manager.component.html */ "./src/app/admin/question-manager/question-manager.component.html"),
            styles: [__webpack_require__(/*! ./question-manager.component.css */ "./src/app/admin/question-manager/question-manager.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], QuestionManagerComponent);
    return QuestionManagerComponent;
}());



/***/ }),

/***/ "./src/app/admin/question-manager/question-manager.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/admin/question-manager/question-manager.module.ts ***!
  \*******************************************************************/
/*! exports provided: QuestionManagerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionManagerModule", function() { return QuestionManagerModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _question_manager_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./question-manager.component */ "./src/app/admin/question-manager/question-manager.component.ts");
/* harmony import */ var _question_manager_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./question-manager.routing */ "./src/app/admin/question-manager/question-manager.routing.ts");
/* harmony import */ var _ui_tree_ui_tree_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ui-tree/ui-tree.component */ "./src/app/admin/question-manager/ui-tree/ui-tree.component.ts");
/* harmony import */ var _ui_tree_child_ui_tree_child_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ui-tree-child/ui-tree-child.component */ "./src/app/admin/question-manager/ui-tree-child/ui-tree-child.component.ts");








var QuestionManagerModule = /** @class */ (function () {
    function QuestionManagerModule() {
    }
    QuestionManagerModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _question_manager_routing__WEBPACK_IMPORTED_MODULE_5__["QuestionManagerRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]
            ],
            declarations: [_question_manager_component__WEBPACK_IMPORTED_MODULE_4__["QuestionManagerComponent"], _ui_tree_ui_tree_component__WEBPACK_IMPORTED_MODULE_6__["UiTreeComponent"], _ui_tree_child_ui_tree_child_component__WEBPACK_IMPORTED_MODULE_7__["UiTreeChildComponent"]]
        })
    ], QuestionManagerModule);
    return QuestionManagerModule;
}());



/***/ }),

/***/ "./src/app/admin/question-manager/question-manager.routing.ts":
/*!********************************************************************!*\
  !*** ./src/app/admin/question-manager/question-manager.routing.ts ***!
  \********************************************************************/
/*! exports provided: QuestionManagerRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionManagerRoutingModule", function() { return QuestionManagerRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _question_manager_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./question-manager.component */ "./src/app/admin/question-manager/question-manager.component.ts");




var routes = [
    {
        path: 'generate/:id',
        component: _question_manager_component__WEBPACK_IMPORTED_MODULE_3__["QuestionManagerComponent"],
    },
];
var QuestionManagerRoutingModule = /** @class */ (function () {
    function QuestionManagerRoutingModule() {
    }
    QuestionManagerRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], QuestionManagerRoutingModule);
    return QuestionManagerRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/question-manager/ui-tree-child/ui-tree-child.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/admin/question-manager/ui-tree-child/ui-tree-child.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3F1ZXN0aW9uLW1hbmFnZXIvdWktdHJlZS1jaGlsZC91aS10cmVlLWNoaWxkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/question-manager/ui-tree-child/ui-tree-child.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/admin/question-manager/ui-tree-child/ui-tree-child.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"items.length\">\n  <div class=\"dd\" id=\"nestable\" *ngFor=\"let item of items\">\n    <ol class=\"dd-list\">\n        <li class=\"dd-item\" data-id=\"1\">\n            <div class=\"dd-handle\">\n            <div class=\"row\">\n               <div class=\"col-md-8\">\n                  {{item.name}}\n               </div> \n               <div class=\"col-md-4 text-right\">\n                   <button (click)=\"openModalQuestion(item)\" class=\"btn btn btn-primary btn-xs\"><i class=\"fa fa-plus fa-xs\"></i></button> &nbsp;\n                  <button (click)=\"addFilter(indice)\" class=\"btn btn btn-default btn-xs\"><i class=\"fa fa-filter fa-xs\"></i></button> &nbsp;                           \n                   <button (click)=\"editCategory(item.name)\" class=\"btn btn btn-default btn-xs\"><i class=\"fa fa-edit fa-xs\"></i></button> &nbsp;  \n                  <button (click)=\"deleteCategory(indice)\" class=\"btn btn btn-danger btn-xs\"><i class=\"fa fa-close fa-xs\"></i></button>                           \n               </div>                             \n               </div>  \n              </div>\n            <app-ui-tree-child *ngIf=\"item[key].length\" [key]=\"key\" [data]=\"item[key]\"></app-ui-tree-child>\n        </li>                                     \n    </ol>\n  </div>\n</div>\n\n\n\n<div class=\"modal fade in\" id=\"questionModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n              <h4 class=\"modal-title\" id=\"myModalLabel\">\n                    <span>Nueva pregunta</span></h4>\n          </div>\n          <div class=\"modal-body\">\n            <label for=\"name\">Pregunta</label>\n            <input id=\"idCategory\" type=\"text\" focus class=\"form-control\" [(ngModel)]=\"question.name\">\n          </div>\n          <div class=\"modal-footer\">\n             <button type=\"button\" (click)=\"confirmQuestion(question)\" class=\"btn btn-primary\">Guardar</button> &nbsp;\n             <button type=\"button\" (click)=\"closeModal()\" class=\"btn btn-default\">Cancelar</button>\n          </div>\n      </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/question-manager/ui-tree-child/ui-tree-child.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/admin/question-manager/ui-tree-child/ui-tree-child.component.ts ***!
  \*********************************************************************************/
/*! exports provided: UiTreeChildComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UiTreeChildComponent", function() { return UiTreeChildComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UiTreeChildComponent = /** @class */ (function () {
    function UiTreeChildComponent() {
        this.item = {
            name: "",
            categories: []
        };
        this.question = {
            name: "",
            categories: []
        };
        this.index = null;
        this.patern = null;
        this.category = {
            name: "",
            categories: []
        };
    }
    UiTreeChildComponent.prototype.ngOnInit = function () {
    };
    UiTreeChildComponent.prototype.openModal = function (question) {
        console.log(question);
        this.item = question;
        this.category = {
            name: question.name,
            categories: question.categories
        };
        $('#editCategoryModal').modal('show');
    };
    UiTreeChildComponent.prototype.confirmCategory = function (question) {
        console.log(question);
        this.item.name = question.name;
        this.item.categories = question.categories;
        this.category = {
            name: "",
            categories: []
        };
        this.closeSurveyModal();
    };
    UiTreeChildComponent.prototype.closeSurveyModal = function () {
        $('#editCategoryModal').modal('hide');
    };
    UiTreeChildComponent.prototype.openModalQuestion = function (category) {
        this.category = category;
        $('#questionModal').modal('show');
    };
    UiTreeChildComponent.prototype.confirmQuestion = function (question) {
        console.log(question);
        this.category.categories.push(question);
        this.question = {
            name: "",
            categories: []
        };
        this.category = {
            name: "",
            categories: []
        };
        this.closeQuestionModal();
    };
    UiTreeChildComponent.prototype.closeQuestionModal = function () {
        $('#questionModal').modal('hide');
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('data'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], UiTreeChildComponent.prototype, "items", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('key'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], UiTreeChildComponent.prototype, "key", void 0);
    UiTreeChildComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ui-tree-child',
            template: __webpack_require__(/*! ./ui-tree-child.component.html */ "./src/app/admin/question-manager/ui-tree-child/ui-tree-child.component.html"),
            styles: [__webpack_require__(/*! ./ui-tree-child.component.css */ "./src/app/admin/question-manager/ui-tree-child/ui-tree-child.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UiTreeChildComponent);
    return UiTreeChildComponent;
}());



/***/ }),

/***/ "./src/app/admin/question-manager/ui-tree/ui-tree.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/admin/question-manager/ui-tree/ui-tree.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3F1ZXN0aW9uLW1hbmFnZXIvdWktdHJlZS91aS10cmVlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/question-manager/ui-tree/ui-tree.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/admin/question-manager/ui-tree/ui-tree.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul style=\"padding-left: inherit;\" *ngIf=\"items.length\">\n<li *ngFor=\"let item of items; let indice = index\" style=\"list-style:none;\">\n    <div class=\"panel panel-default\">\n        <div class=\"panel-heading\" role=\"tab\" [attr.id]=\"'headingOne' + indice\">\n                                \n          <h4 class=\"panel-title\">\n              <div class=\"row\">\n               <a data-toggle=\"collapse\" data-parent=\"#accordion\" [attr.href]=\"'#'+indice\" aria-expanded=\"false\" class=\"collapsed\">\n                      \n              <div class=\"col-md-8\">\n                  <span class=\"label label-primary\">{{indice+1}}</span>\n                  <span style=\"text-transform: uppercase;padding-left: 10px\">{{item.name}}</span>\n              </div> \n              </a> \n              <div class=\"col-md-4 text-right\">\n                  <button (click)=\"openModalQuestion(item)\" class=\"btn btn btn-primary btn-xs\"><i class=\"fa fa-plus fa-xs\"></i></button> &nbsp;  \n                  <button (click)=\"openModal(item)\" class=\"btn btn btn-default btn-xs\"><i class=\"fa fa-edit fa-xs\"></i></button> &nbsp;  \n                  <button (click)=\"deleteCategory(indice)\" class=\"btn btn btn-danger btn-xs\"><i class=\"fa fa-close fa-xs\"></i></button>                            \n                </div>                             \n              </div>                                                       \n            </h4>                                                  \n        </div>\n      </div>\n        <div [attr.id]=\"indice\" class=\"panel-collapse collapse\" role=\"tabpanel\"  aria-expanded=\"false\" style=\"height: 0px;\">\n          <app-ui-tree-child *ngIf=\"item[key].length\" [key]=\"key\" [data]=\"item[key]\"></app-ui-tree-child>\n        </div>\n</li>\n</ul>\n\n<div class=\"modal fade in\" id=\"editCategoryModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n              <h4 class=\"modal-title\" id=\"myModalLabel\">\n                    <span>Editar Categoría</span></h4>\n          </div>\n          <div class=\"modal-body\">\n            <label for=\"name\">Categoría</label>\n            <input id=\"idCategory\" type=\"text\" focus class=\"form-control\" [(ngModel)]=\"category.name\">\n          </div>\n          <div class=\"modal-footer\">\n             <button type=\"button\" (click)=\"confirmCategory(category)\" class=\"btn btn-primary\">Guardar</button> &nbsp;\n             <button type=\"button\" (click)=\"closeModal()\" class=\"btn btn-default\">Cancelar</button>\n          </div>\n      </div>\n  </div>\n</div>\n\n<div class=\"modal fade in\" id=\"questionModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n              <h4 class=\"modal-title\" id=\"myModalLabel\">\n                    <span>Nueva pregunta</span></h4>\n          </div>\n          <div class=\"modal-body\">\n            <label for=\"name\">Pregunta</label>\n            <input id=\"idCategory\" type=\"text\" focus class=\"form-control\" [(ngModel)]=\"question.name\">\n          </div>\n          <div class=\"modal-footer\">\n             <button type=\"button\" (click)=\"confirmQuestion(question)\" class=\"btn btn-primary\">Guardar</button> &nbsp;\n             <button type=\"button\" (click)=\"closeModal()\" class=\"btn btn-default\">Cancelar</button>\n          </div>\n      </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/question-manager/ui-tree/ui-tree.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/admin/question-manager/ui-tree/ui-tree.component.ts ***!
  \*********************************************************************/
/*! exports provided: UiTreeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UiTreeComponent", function() { return UiTreeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UiTreeComponent = /** @class */ (function () {
    function UiTreeComponent() {
        this.itemw = {
            name: "",
            categories: []
        };
        this.question = {
            name: "",
            categories: []
        };
        this.index = null;
        this.patern = null;
        this.category = {
            name: "",
            categories: []
        };
    }
    UiTreeComponent.prototype.ngOnInit = function () {
    };
    UiTreeComponent.prototype.openModal = function (category) {
        console.log(category);
        this.itemw = category;
        this.category = {
            name: category.name,
            categories: category.categories
        };
        $('#editCategoryModal').modal('show');
    };
    UiTreeComponent.prototype.confirmCategory = function (question) {
        console.log(question);
        this.itemw.name = question.name;
        this.itemw.categories = question.categories;
        this.category = {
            name: "",
            categories: []
        };
        this.closeSurveyModal();
    };
    UiTreeComponent.prototype.closeSurveyModal = function () {
        $('#editCategoryModal').modal('hide');
    };
    UiTreeComponent.prototype.openModalQuestion = function (category) {
        this.category = {
            name: category.name,
            categories: category.categories
        };
        console.log(category);
        $('#questionModal').modal('show');
    };
    UiTreeComponent.prototype.confirmQuestion = function (question) {
        console.log(question);
        console.log(this.category);
        this.category.categories.push(question);
        this.question = {
            name: "",
            categories: []
        };
        this.closeQuestionModal();
    };
    UiTreeComponent.prototype.closeQuestionModal = function () {
        $('#questionModal').modal('hide');
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('data'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], UiTreeComponent.prototype, "items", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('key'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], UiTreeComponent.prototype, "key", void 0);
    UiTreeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ui-tree',
            template: __webpack_require__(/*! ./ui-tree.component.html */ "./src/app/admin/question-manager/ui-tree/ui-tree.component.html"),
            styles: [__webpack_require__(/*! ./ui-tree.component.css */ "./src/app/admin/question-manager/ui-tree/ui-tree.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UiTreeComponent);
    return UiTreeComponent;
}());



/***/ })

}]);
//# sourceMappingURL=question-manager-question-manager-module.js.map