(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["condition-condition-module"],{

/***/ "./src/app/admin/condition/condition-form/condition-form.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/admin/condition/condition-form/condition-form.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2NvbmRpdGlvbi9jb25kaXRpb24tZm9ybS9jb25kaXRpb24tZm9ybS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/condition/condition-form/condition-form.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/admin/condition/condition-form/condition-form.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n  <h3 class=\"breadcrumb-header\"> Editor de condiciones </h3>\n</div>\n<div id=\"main-wrapper\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n  <div class=\"panel panel-white\">\n      <div class=\"panel-heading clearfix\">\n          <h4 class=\"panel-title\"><span>Nueva condición</span></h4>         \n      </div>\n      <div class=\"panel-body\">\n          <form role=\"form\"  autocomplete=\"off\">\n             <div class=\"row\">                      \n                        <div class=\"form-group col-md-12\">\n                            <label for=\"conditionSql\">Condición SQL</label>\n                            <span><input type=\"text\" placeholder=\"\" name=\"conditionSql\" class=\"form-control\"></span>                            \n                        </div>\n                        <br>\n                       <h4>Asistente para el ingreso de la condición</h4>\n                       <br>\n                       <p>Diligencie los campos requeridos y verifique en el recuadro superior el resultado final.</p>\n                       <div class=\"form-group col-md-3\">\n                        <label for=\"cabezaHogar\" class=\"control-label\">Cabeza de hogar</label>\n                        <select class=\"form-control\" name=\"cabezaHogar\" id=\"cabezaHogar\">\n                          <option value=\"0\">N/A</option>  \n                          <option value=\"1\">Sí</option>\n                          <option value=\"2\">No</option>\n                        </select>                          \n                       </div>\n                       <div class=\"form-group col-md-3\">\n                        <label for=\"\" class=\"control-label\">Género</label>\n                        <select class=\"form-control\" name=\"\" id=\"\" required>\n                          <option value=\"0\">N/A</option>  \n                          <option value=\"1\">Masculino</option>\n                          <option value=\"2\">Femenino</option>\n                        </select>                          \n                       </div>\n                       <div class=\"form-group col-md-3\">\n                        <label for=\"\" class=\"control-label\">Régimen de salud</label>\n                        <select class=\"form-control\" name=\"\" id=\"\" required>\n                          <option value=\"0\">N/A</option>\n                        </select>                          \n                       </div>                           \n                       <div class=\"form-group col-md-3\">\n                        <label for=\"\" class=\"control-label\">Eps</label>\n                        <select class=\"form-control\" name=\"\" id=\"\" required>\n                          <option value=\"0\">N/A</option>\n                        </select>                          \n                       </div>                      \n                       <div class=\"form-group col-md-3\">\n                        <label for=\"\" class=\"control-label\">Estudia</label>\n                        <select class=\"form-control\" name=\"\" id=\"\" required>\n                          <option value=\"0\">N/A</option>\n                          <option value=\"1\">Sí</option>\n                          <option value=\"2\">No</option>\n                        </select>                          \n                       </div>                        \n                       <div class=\"form-group col-md-3\">\n                        <label for=\"\" class=\"control-label\">Estado civil</label>\n                        <select class=\"form-control\" name=\"\" id=\"\" required>\n                          <option value=\"0\">N/A</option>\n                          <option value=\"1\">Soltero</option>\n                          <option value=\"2\">Casado</option>\n                        </select>                          \n                       </div>                                  \n                       <div class=\"form-group col-md-3\">\n                        <label for=\"\" class=\"control-label\">Situación laboral</label>\n                        <select class=\"form-control\" name=\"\" id=\"\" required>\n                          <option value=\"0\">N/A</option>\n                          <option value=\"1\">Empleado</option>\n                          <option value=\"2\">Desempleado</option>\n                        </select>                          \n                       </div>              \n                       <div class=\"form-group col-md-3\">\n                        <label for=\"\" class=\"control-label\">Posición familiar</label>\n                        <select class=\"form-control\" name=\"\" id=\"\" required>\n                          <option value=\"0\">N/A</option>                                                    \n                        </select>                          \n                       </div>   \n                       <div class=\"form-group col-md-12\">\n                        <h4>Fecha de nacimiento</h4>\n                        <div>\n                          <span>Mayor de <input type=\"text\"> años y <input type=\"text\"> meses</span><br><br>                           \n                          <span>Menor de <input type=\"text\"> años y <input type=\"text\"> meses </span>\n                        </div>                       \n                       </div>                                                   \n                       <div class=\"form-group col-md-12\">\n                          <button type=\"submit\" class=\"btn btn-primary float-right\">Guardar</button>\n                       </div>\n             </div>            \n            </form>\n      </div>\n  </div>\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/condition/condition-form/condition-form.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/admin/condition/condition-form/condition-form.component.ts ***!
  \****************************************************************************/
/*! exports provided: ConditionFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConditionFormComponent", function() { return ConditionFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ConditionFormComponent = /** @class */ (function () {
    function ConditionFormComponent() {
    }
    ConditionFormComponent.prototype.ngOnInit = function () {
    };
    ConditionFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-condition-form',
            template: __webpack_require__(/*! ./condition-form.component.html */ "./src/app/admin/condition/condition-form/condition-form.component.html"),
            styles: [__webpack_require__(/*! ./condition-form.component.css */ "./src/app/admin/condition/condition-form/condition-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConditionFormComponent);
    return ConditionFormComponent;
}());



/***/ }),

/***/ "./src/app/admin/condition/condition.component.css":
/*!*********************************************************!*\
  !*** ./src/app/admin/condition/condition.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2NvbmRpdGlvbi9jb25kaXRpb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/condition/condition.component.html":
/*!**********************************************************!*\
  !*** ./src/app/admin/condition/condition.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-condition-form></app-condition-form>\n"

/***/ }),

/***/ "./src/app/admin/condition/condition.component.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/condition/condition.component.ts ***!
  \********************************************************/
/*! exports provided: ConditionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConditionComponent", function() { return ConditionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ConditionComponent = /** @class */ (function () {
    function ConditionComponent() {
    }
    ConditionComponent.prototype.ngOnInit = function () {
    };
    ConditionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-condition',
            template: __webpack_require__(/*! ./condition.component.html */ "./src/app/admin/condition/condition.component.html"),
            styles: [__webpack_require__(/*! ./condition.component.css */ "./src/app/admin/condition/condition.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConditionComponent);
    return ConditionComponent;
}());



/***/ }),

/***/ "./src/app/admin/condition/condition.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/admin/condition/condition.module.ts ***!
  \*****************************************************/
/*! exports provided: ConditionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConditionModule", function() { return ConditionModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _condition_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./condition.component */ "./src/app/admin/condition/condition.component.ts");
/* harmony import */ var _condition_form_condition_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./condition-form/condition-form.component */ "./src/app/admin/condition/condition-form/condition-form.component.ts");
/* harmony import */ var _condition_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./condition.routing */ "./src/app/admin/condition/condition.routing.ts");






var ConditionModule = /** @class */ (function () {
    function ConditionModule() {
    }
    ConditionModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _condition_routing__WEBPACK_IMPORTED_MODULE_5__["ConditionRoutingModule"]
            ],
            declarations: [_condition_component__WEBPACK_IMPORTED_MODULE_3__["ConditionComponent"], _condition_form_condition_form_component__WEBPACK_IMPORTED_MODULE_4__["ConditionFormComponent"]]
        })
    ], ConditionModule);
    return ConditionModule;
}());



/***/ }),

/***/ "./src/app/admin/condition/condition.routing.ts":
/*!******************************************************!*\
  !*** ./src/app/admin/condition/condition.routing.ts ***!
  \******************************************************/
/*! exports provided: ConditionRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConditionRoutingModule", function() { return ConditionRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _condition_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./condition.component */ "./src/app/admin/condition/condition.component.ts");




var routes = [
    {
        path: '',
        component: _condition_component__WEBPACK_IMPORTED_MODULE_3__["ConditionComponent"]
    }
];
var ConditionRoutingModule = /** @class */ (function () {
    function ConditionRoutingModule() {
    }
    ConditionRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], ConditionRoutingModule);
    return ConditionRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=condition-condition-module.js.map