(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["survey-type-survey-type-module"],{

/***/ "./src/app/admin/survey-type/survey-type-form/survey-type-form.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type-form/survey-type-form.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n      <h3 class=\"breadcrumb-header\"> Tipos de encuestas </h3>\n  </div>\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n  <div class=\"col-md-12\">\n      <div class=\"panel panel-white\">\n          <div class=\"panel-heading clearfix\">\n              <h4 class=\"panel-title\">         <span *ngIf=\"surveyType._id == null\">Nuevo tipo de encuesta</span>\n               <span *ngIf=\"surveyType._id != null\">Editar tipo de encuesta</span>    </h4>\n          </div>\n          <div class=\"panel-body\">\n              <form role=\"form\"  autocomplete=\"off\" (ngSubmit)=\"f.form.valid && onSubmit(f)\" #f=\"ngForm\" novalidate>\n                 <div class=\"row\">\n                           <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !title.valid}\">\n                              <label for=\"title\" class=\"control-label\">Título</label>\n                              <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"title\" #title=\"ngModel\" [(ngModel)]=\"surveyType.title\" required>\n                              <span class=\"help-block\" *ngIf=\"f.submitted && !title.valid\">El título es requerido.</span>\n                           </div>\n                           <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !modifiable.valid}\">\n                                <label for=\"modifiable\" class=\"control-label\">Modificable</label>\n                                <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"modifiable\" #modifiable=\"ngModel\" [(ngModel)]=\"surveyType.modifiable\" required>\n                                <span class=\"help-block\" *ngIf=\"f.submitted && !modifiable.valid\">El campo modificable es requerido.</span>\n                             </div>\n                             <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !multiple.valid}\">\n                                    <label for=\"multiple\" class=\"control-label\">Múltiple</label>\n                                    <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"multiple\" #multiple=\"ngModel\" [(ngModel)]=\"surveyType.multiple\" required>\n                                    <span class=\"help-block\" *ngIf=\"f.submitted && !multiple.valid\">El campo múltiple es requerido.</span>\n                            </div>                           \n                            <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !field.valid}\">\n                                    <label for=\"field\" class=\"control-label\">Campo</label>\n                                    <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"field\" #field=\"ngModel\" [(ngModel)]=\"surveyType.field\" required>\n                                    <span class=\"help-block\" *ngIf=\"f.submitted && !field.valid\">El campo es requerido.</span>                                    \n                            </div>\n                            <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error': f.submitted && !answer.valid}\">\n                                <label for=\"answer\" class=\"control-label\">Respuesta</label>\n                                <input type=\"text\" class=\"form-control\" placeholder=\"\" name=\"answer\" #answer=\"ngModel\" [(ngModel)]=\"surveyType.answer\" required>\n                                <span class=\"help-block\" *ngIf=\"f.submitted && !answer.valid\">La respuesta es requerido.</span>\n                            </div>\n                           <div class=\"form-group col-md-12\">\n                              <button type=\"submit\" class=\"btn btn-primary float-right\">Guardar</button>\n                           </div>\n                 </div>                \n                </form>\n          </div>\n      </div>\n  </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/survey-type/survey-type-form/survey-type-form.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type-form/survey-type-form.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3N1cnZleS10eXBlL3N1cnZleS10eXBlLWZvcm0vc3VydmV5LXR5cGUtZm9ybS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/survey-type/survey-type-form/survey-type-form.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type-form/survey-type-form.component.ts ***!
  \**********************************************************************************/
/*! exports provided: SurveyTypeFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyTypeFormComponent", function() { return SurveyTypeFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _survey_type__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../survey-type */ "./src/app/admin/survey-type/survey-type.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _survey_type_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../survey-type.service */ "./src/app/admin/survey-type/survey-type.service.ts");





var SurveyTypeFormComponent = /** @class */ (function () {
    function SurveyTypeFormComponent(_router, _activatedRoute, _surveyTypeService) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._surveyTypeService = _surveyTypeService;
        this.surveyType = new _survey_type__WEBPACK_IMPORTED_MODULE_2__["SurveyType"](null, null, null, null, null);
    }
    SurveyTypeFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this._activatedRoute.params.subscribe(function (params) {
            if (params['id']) {
                _this.id = params['id'];
                _this.getSurveyType();
            }
        });
    };
    SurveyTypeFormComponent.prototype.getSurveyType = function () {
        var _this = this;
        this._surveyTypeService.getSurveyType(this.id).then(function (data) {
            _this.surveyType = new _survey_type__WEBPACK_IMPORTED_MODULE_2__["SurveyType"](data._id, data.modifiable, data.multiple, data.field, data.answer);
        }).catch(function (error) { return console.log(error); });
    };
    SurveyTypeFormComponent.prototype.onSubmit = function (f) {
        var _this = this;
        this._surveyTypeService
            .saveSurveyType(this.surveyType)
            .then(function (data) {
            _this._router.navigate(['/admin/survey-types']);
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    SurveyTypeFormComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    SurveyTypeFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-survey-type-form',
            template: __webpack_require__(/*! ./survey-type-form.component.html */ "./src/app/admin/survey-type/survey-type-form/survey-type-form.component.html"),
            providers: [_survey_type_service__WEBPACK_IMPORTED_MODULE_4__["SurveyTypeService"]],
            styles: [__webpack_require__(/*! ./survey-type-form.component.scss */ "./src/app/admin/survey-type/survey-type-form/survey-type-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _survey_type_service__WEBPACK_IMPORTED_MODULE_4__["SurveyTypeService"]])
    ], SurveyTypeFormComponent);
    return SurveyTypeFormComponent;
}());



/***/ }),

/***/ "./src/app/admin/survey-type/survey-type-list/survey-type-list.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type-list/survey-type-list.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-7 col-6\">\n        <div class=\"page-title\">\n            <h3 class=\"breadcrumb-header\">Tipos de encuesta</h3>\n          </div>\n    </div>\n\n    <div class=\"col-sm-5 col-6 text-right\">\n        <a class=\"btn btn-primary btn-rounded btn-title\" routerLink=\"new\"><i class=\"fa fa-plus\"></i> Nuevo tipo de encuesta</a>\n    </div>\n</div>\n\n\n<div id=\"main-wrapper\">\n\n<div class=\"row\">\n<div class=\"col-md-12\">\n    <div class=\"panel panel-white\">\n        <div class=\"panel-heading clearfix\">\n            <h4 class=\"panel-title\">Lista</h4>\n        </div>\n        <div class=\"panel-body\">\n           <div class=\"table-responsive\">\n              <table datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" class=\"display table\" >\n                  <thead>\n                    <tr>\n                      <th>Título</th>\n                      <th>Modificable</th>\n                      <th>Múltiple</th>\n                      <th>Campo</th>\n                      <th>Respuesta</th>                      \n                      <th>Opciones</th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <tr *ngFor=\"let item of surveyTypeList\">\n                      <td>{{ item.title }}</td>\n                      <td>{{ item.modifiable }}</td>\n                      <td>{{ item.multiple }}</td>\n                      <td>{{ item.field }}</td>\n                      <td>{{ item.answer }}</td>                      \n                      <td>\n                        <a class=\"btn btn-default btn-xs\" routerLink=\"edit/{{item._id}}\">\n                          <i class=\"fa fa-edit\"></i> Editar</a>\n                      </td>\n                    </tr>\n                  </tbody>\n                \n                </table>\n     \n            </div>\n        </div>\n    </div>\n \n\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/survey-type/survey-type-list/survey-type-list.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type-list/survey-type-list.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3N1cnZleS10eXBlL3N1cnZleS10eXBlLWxpc3Qvc3VydmV5LXR5cGUtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/survey-type/survey-type-list/survey-type-list.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type-list/survey-type-list.component.ts ***!
  \**********************************************************************************/
/*! exports provided: SurveyTypeListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyTypeListComponent", function() { return SurveyTypeListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _survey_type_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../survey-type.service */ "./src/app/admin/survey-type/survey-type.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var SurveyTypeListComponent = /** @class */ (function () {
    function SurveyTypeListComponent(_surveyTypeService) {
        this._surveyTypeService = _surveyTypeService;
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.surveyTypeList = [];
    }
    SurveyTypeListComponent.prototype.ngOnInit = function () {
        this.dtOptions = {
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        };
        this.getAllSurveyTypes();
    };
    SurveyTypeListComponent.prototype.getAllSurveyTypes = function () {
        var _this = this;
        this._surveyTypeService.getSurveyTypes()
            .then(function (data) {
            _this.surveyTypeList = data;
            _this.dtTrigger.next();
        })
            .catch(function (error) { return console.log(error); });
    };
    SurveyTypeListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-survey-type-list',
            template: __webpack_require__(/*! ./survey-type-list.component.html */ "./src/app/admin/survey-type/survey-type-list/survey-type-list.component.html"),
            providers: [_survey_type_service__WEBPACK_IMPORTED_MODULE_2__["SurveyTypeService"]],
            styles: [__webpack_require__(/*! ./survey-type-list.component.scss */ "./src/app/admin/survey-type/survey-type-list/survey-type-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_survey_type_service__WEBPACK_IMPORTED_MODULE_2__["SurveyTypeService"]])
    ], SurveyTypeListComponent);
    return SurveyTypeListComponent;
}());



/***/ }),

/***/ "./src/app/admin/survey-type/survey-type.component.html":
/*!**************************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/admin/survey-type/survey-type.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3N1cnZleS10eXBlL3N1cnZleS10eXBlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/survey-type/survey-type.component.ts":
/*!************************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type.component.ts ***!
  \************************************************************/
/*! exports provided: SurveyTypeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyTypeComponent", function() { return SurveyTypeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SurveyTypeComponent = /** @class */ (function () {
    function SurveyTypeComponent() {
    }
    SurveyTypeComponent.prototype.ngOnInit = function () {
    };
    SurveyTypeComponent.prototype.ngAfterViewInit = function () {
    };
    SurveyTypeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-survey-type',
            template: __webpack_require__(/*! ./survey-type.component.html */ "./src/app/admin/survey-type/survey-type.component.html"),
            styles: [__webpack_require__(/*! ./survey-type.component.scss */ "./src/app/admin/survey-type/survey-type.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SurveyTypeComponent);
    return SurveyTypeComponent;
}());



/***/ }),

/***/ "./src/app/admin/survey-type/survey-type.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type.module.ts ***!
  \*********************************************************/
/*! exports provided: SurveyTypeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyTypeModule", function() { return SurveyTypeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _survey_type_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./survey-type.component */ "./src/app/admin/survey-type/survey-type.component.ts");
/* harmony import */ var _survey_type_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./survey-type.routing */ "./src/app/admin/survey-type/survey-type.routing.ts");
/* harmony import */ var _survey_type_list_survey_type_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./survey-type-list/survey-type-list.component */ "./src/app/admin/survey-type/survey-type-list/survey-type-list.component.ts");
/* harmony import */ var _survey_type_form_survey_type_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./survey-type-form/survey-type-form.component */ "./src/app/admin/survey-type/survey-type-form/survey-type-form.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");









var SurveyTypeModule = /** @class */ (function () {
    function SurveyTypeModule() {
    }
    SurveyTypeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _survey_type_routing__WEBPACK_IMPORTED_MODULE_5__["SurveyTypeRoutingModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"]
            ],
            declarations: [_survey_type_component__WEBPACK_IMPORTED_MODULE_4__["SurveyTypeComponent"], _survey_type_list_survey_type_list_component__WEBPACK_IMPORTED_MODULE_6__["SurveyTypeListComponent"], _survey_type_form_survey_type_form_component__WEBPACK_IMPORTED_MODULE_7__["SurveyTypeFormComponent"]]
        })
    ], SurveyTypeModule);
    return SurveyTypeModule;
}());



/***/ }),

/***/ "./src/app/admin/survey-type/survey-type.routing.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/survey-type/survey-type.routing.ts ***!
  \**********************************************************/
/*! exports provided: SurveyTypeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyTypeRoutingModule", function() { return SurveyTypeRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _survey_type_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./survey-type.component */ "./src/app/admin/survey-type/survey-type.component.ts");
/* harmony import */ var _survey_type_list_survey_type_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./survey-type-list/survey-type-list.component */ "./src/app/admin/survey-type/survey-type-list/survey-type-list.component.ts");
/* harmony import */ var _survey_type_form_survey_type_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./survey-type-form/survey-type-form.component */ "./src/app/admin/survey-type/survey-type-form/survey-type-form.component.ts");






var routes = [
    {
        path: '',
        component: _survey_type_component__WEBPACK_IMPORTED_MODULE_3__["SurveyTypeComponent"],
        children: [{
                path: '',
                component: _survey_type_list_survey_type_list_component__WEBPACK_IMPORTED_MODULE_4__["SurveyTypeListComponent"],
            }, {
                path: 'new',
                component: _survey_type_form_survey_type_form_component__WEBPACK_IMPORTED_MODULE_5__["SurveyTypeFormComponent"],
            }, {
                path: 'edit/:id',
                component: _survey_type_form_survey_type_form_component__WEBPACK_IMPORTED_MODULE_5__["SurveyTypeFormComponent"],
            }
        ]
    }
];
var SurveyTypeRoutingModule = /** @class */ (function () {
    function SurveyTypeRoutingModule() {
    }
    SurveyTypeRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], SurveyTypeRoutingModule);
    return SurveyTypeRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=survey-type-survey-type-module.js.map