import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AdminModule } from './admin/admin.module';
import { AppRoutingModule } from './app.routing';
import { DataTablesModule } from 'angular-datatables';
import { ScriptLoaderService } from './script-loader.service';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
   declarations: [
      AppComponent,
      LoginComponent
      ],
   imports: [
      BrowserModule,
      FormsModule,
      HttpModule,
      AppRoutingModule,
      AdminModule,
      ReactiveFormsModule,
      DataTablesModule,
      NgSelectModule
   ],
   providers: [
      ScriptLoaderService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
