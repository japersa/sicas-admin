/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AutoLogoutServiceService } from './auto-logout-service.service';

describe('Service: AutoLogoutService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AutoLogoutServiceService]
    });
  });

  it('should ...', inject([AutoLogoutServiceService], (service: AutoLogoutServiceService) => {
    expect(service).toBeTruthy();
  }));
});
