import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Login } from './login';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})

export class LoginComponent implements OnInit, OnDestroy {

  login = new Login(null, null)

  constructor(private _router: Router, private _loginService: LoginService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  onSubmit(f: any) {
    this._loginService.login(this.login)
      .then(data => {
        localStorage.setItem('token', JSON.stringify(data.accessToken));
        this._router.navigate(['/admin/']);
      })
      .catch(error => {
        alert("Usuario y contraseña incorrectos");
      });
  }

}
