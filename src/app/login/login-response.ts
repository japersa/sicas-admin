export class LoginResponse {
    constructor(
        public accessToken: string,
        public expiresIn: number
      ) {}
}
