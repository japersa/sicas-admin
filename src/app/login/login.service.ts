import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Login } from './login';
import { environment } from '../../environments/environment';
import { LoginResponse } from './login-response';


@Injectable()
export class LoginService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    login(user: Login): Promise < LoginResponse > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/auth`;

        return this.http.post(url, user, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
