import { Component, OnInit } from '@angular/core';
import { AutoLogoutService } from './auto-logout-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AutoLogoutService]
})

export class AppComponent implements OnInit {

  constructor(private autoLogoutService: AutoLogoutService) { 
  }

  ngOnInit() {
    localStorage.setItem('lastAction', Date.now().toString());
  }

}