import { NgModule, Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AuthGuard } from '../auth-guard';

@NgModule({
  imports: [
      RouterModule.forChild([{
          path: 'admin',
          component: AdminComponent,
          canActivate: [AuthGuard],
          children: [
              {
                path: '',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
              },
              {
                path: 'users',
                loadChildren: './user/user.module#UserModule'
              },
              {
                path: 'dependences',
                loadChildren: './dependence/dependence.module#DependenceModule'
              },
              {
                path: 'genders',
                loadChildren: './gender/gender.module#GenderModule'
              },
              {
                path: 'countries',
                loadChildren: './country/country.module#CountryModule'
              },
              {
                path: 'communes',
                loadChildren: './commune/commune.module#CommuneModule'
              },
              {
                path: 'sidewalks',
                loadChildren: './sidewalk/sidewalk.module#SidewalkModule'
              },
              {
                path: 'charges',
                loadChildren: './charge/charge.module#ChargeModule'
              },
              {
                path: 'sectors',
                loadChildren: './sector/sector.module#SectorModule'
              },
              {
                path: 'cities',
                loadChildren: './city/city.module#CityModule'
              },
              {
                path: 'departments',
                loadChildren: './department/department.module#DepartmentModule'
              },
              {
                path: 'roles',
                loadChildren: './role/role.module#RoleModule'
              },
              {
                path: 'permissions',
                loadChildren: './permission/permission.module#PermissionModule'
              },
              {
                path: 'professionals',
                loadChildren: './professional/professional.module#ProfessionalModule'
              },
              {
                path: 'surveys',
                loadChildren: './survey/survey.module#SurveyModule'
              },
              {
                path: 'survey-types',
                loadChildren: './survey-type/survey-type.module#SurveyTypeModule'
              },
              {
                path: 'subsidies',
                loadChildren: './subsidy/subsidy.module#SubsidyModule'
              },
              {
                path: 'subsidy-types',
                loadChildren: './subsidy-type/subsidy-type.module#SubsidyTypeModule'
              },
              {
                path: 'neighborhoods',
                loadChildren: './neighborhood/neighborhood.module#NeighborhoodModule'
              },
              {
                path: 'family-cards',
                loadChildren: './family-card/family-card.module#FamilyCardModule'
              },
              {
                path: 'event',
                loadChildren: './event/event.module#EventModule'
              },
              {
                path: 'filters',
                loadChildren: './filter/filter.module#FilterModule'
              },
              {
                path: 'conditions',
                loadChildren: './condition/condition.module#ConditionModule'
              },
              {
                path: 'question-manager',
                loadChildren: './question-manager/question-manager.module#QuestionManagerModule'
              }
          ]
      }])
  ],
  exports: [
      RouterModule
  ]
})
export class AdminRoutingModule {}
