import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AppSidebarComponent } from '../app-sidebar/app-sidebar.component';
import { AppHeaderComponent } from '../app-header/app-header.component';
import { AppFooterComponent } from '../app-footer/app-footer.component';
import { AdminRoutingModule } from './admin.routing';
import { DataTablesModule } from 'angular-datatables';
import { NgSelectModule } from '@ng-select/ng-select';
import { AuthGuard } from '../auth-guard';


@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    DataTablesModule,
    NgSelectModule
  ],
  declarations: [
    AdminComponent,    
    AppSidebarComponent,
    AppHeaderComponent,
    AppFooterComponent
  ],
  providers: [AuthGuard]
})
export class AdminModule { }
