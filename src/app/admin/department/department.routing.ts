import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { DepartmentComponent } from './department.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentFormComponent } from './department-form/department-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: DepartmentComponent,

    children: [{
      path: '',
      component: DepartmentListComponent,
    },{
      path: 'new',
      component: DepartmentFormComponent,
    },{
      path: 'edit/:id',
      component: DepartmentFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartmentRoutingModule {}
