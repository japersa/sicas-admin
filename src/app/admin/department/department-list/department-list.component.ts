import { Component, OnInit } from '@angular/core';
import { DepartmentService } from '../department.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.scss'],
  providers: [DepartmentService]
})
export class DepartmentListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  departmentList = [];

  constructor(private _departmentService: DepartmentService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllCountries();
  }

  getAllCountries() {
    this._departmentService.getDepartments()
      .then(data => {
        this.departmentList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));
      

  }

}
