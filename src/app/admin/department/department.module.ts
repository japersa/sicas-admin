import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { DepartmentComponent } from './department.component';
import { DepartmentRoutingModule } from './department.routing';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentFormComponent } from './department-form/department-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DepartmentRoutingModule,
    DataTablesModule
  ],
  declarations: [ DepartmentComponent, DepartmentListComponent, DepartmentFormComponent]
})
export class DepartmentModule { }
