import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Department } from './department';
import { environment } from '../../../environments/environment';

@Injectable()
export class DepartmentService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getDepartments(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/departments`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getDepartmentsCountry(id: number): Promise < Object[] > {

        let url = `${this.apiEndpoint}/departments/country/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getDepartment(id: number): Promise < Department > {

        let url = `${this.apiEndpoint}/departments/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveDepartment(department: Department): Promise < Object > {
        if (department._id) {
            return this.updateDepartment(department);
        }
        return this.addDepartment(department);
    }


    private addDepartment(department: Department): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/departments`;

        return this.http.post(url, department, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateDepartment(department: Department) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/departments/${department._id}`;

        return this.http
            .patch(url, department, {
                headers: headers
            })
            .toPromise()
            .then(() => department)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
