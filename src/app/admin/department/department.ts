import { Country } from '../country/country';

export class Department {
    constructor(
    public _id: number,
    public code: string,
    public description: string,
    public country: Country
  ) {  }
}
