import { Component, OnInit, OnDestroy } from '@angular/core';
import { Department } from '../department';
import { Router, ActivatedRoute } from '@angular/router';
import { DepartmentService } from '../department.service';
import { Country } from '../../country/country';
import { CountryService } from '../../country/country.service';

@Component({
  selector: 'app-department-form',
  templateUrl: './department-form.component.html',
  styleUrls: ['./department-form.component.scss'],
  providers: [DepartmentService, CountryService]
})

export class DepartmentFormComponent implements OnInit, OnDestroy {

  id: number;
  department = new Department(null, null, null, new Country(null, null, null));
  private sub: any;
  countryList = [];

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _departmentService: DepartmentService,
    private _countryService: CountryService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getDepartment();
      }
    });
    this.getAllCountries();
  }

  getDepartment() {
    this._departmentService.getDepartment(this.id).then(
      data => {
        this.department = new Department(data._id, data.code, data.description, data.country);
      }
    ).catch(error => console.log(error));

  }

  getAllCountries() {
    this._countryService.getCountries()
      .then(data => {
        this.countryList = data;
      })
      .catch(error => console.log(error));
  }



  onSubmit(f: any) {
    this._departmentService
      .saveDepartment(this.department)
      .then(data => {
        this._router.navigate(['/admin/departments']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}