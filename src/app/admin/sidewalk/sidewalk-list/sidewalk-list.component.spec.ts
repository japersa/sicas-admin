/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SidewalkListComponent } from './sidewalk-list.component';

describe('SidewalkListComponent', () => {
  let component: SidewalkListComponent;
  let fixture: ComponentFixture<SidewalkListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidewalkListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidewalkListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
