import { Component, OnInit } from '@angular/core';
import { SidewalkService } from '../sidewalk.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-sidewalk-list',
  templateUrl: './sidewalk-list.component.html',
  styleUrls: ['./sidewalk-list.component.scss'],
  providers: [SidewalkService]
})
export class SidewalkListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  sidewalkList = [];

  constructor(private _sidewalkService: SidewalkService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllSidewalks();
  }

  getAllSidewalks() {
    this._sidewalkService.getSidewalks()
      .then(data => {
        this.sidewalkList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));
      

  }

}
