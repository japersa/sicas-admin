import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Sidewalk } from './sidewalk';
import { environment } from '../../../environments/environment';

@Injectable()
export class SidewalkService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getSidewalks(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/sidewalks`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSidewalksCity(id: number): Promise < Object[] > {

        let url = `${this.apiEndpoint}/sidewalks/city/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSidewalk(id: number): Promise < Sidewalk > {

        let url = `${this.apiEndpoint}/sidewalks/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveSidewalk(sidewalk: Sidewalk): Promise < Object > {
        if (sidewalk._id) {
            return this.updateSidewalk(sidewalk);
        }
        return this.addSidewalk(sidewalk);
    }


    private addSidewalk(sidewalk: Sidewalk): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/sidewalks`;

        return this.http.post(url, sidewalk, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateSidewalk(sidewalk: Sidewalk) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/sidewalks/${sidewalk._id}`;

        return this.http
            .patch(url, sidewalk, {
                headers: headers
            })
            .toPromise()
            .then(() => sidewalk)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
