/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SidewalkService } from './sidewalk.service';

describe('Service: Sidewalk', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SidewalkService]
    });
  });

  it('should ...', inject([SidewalkService], (service: SidewalkService) => {
    expect(service).toBeTruthy();
  }));
});
