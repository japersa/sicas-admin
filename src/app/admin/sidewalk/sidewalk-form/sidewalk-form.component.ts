import { Component, OnInit, OnDestroy } from '@angular/core';
import { Sidewalk } from '../sidewalk';
import { Router, ActivatedRoute } from '@angular/router';
import { SidewalkService } from '../sidewalk.service';
import { CityService } from '../../city/city.service';
import { City } from '../../city/city';
import { Country } from '../../country/country';
import { Department } from '../../department/department';

@Component({
  selector: 'app-sidewalk-form',
  templateUrl: './sidewalk-form.component.html',
  styleUrls: ['./sidewalk-form.component.scss'],
  providers: [SidewalkService, CityService]
})

export class SidewalkFormComponent implements OnInit, OnDestroy {

  id: number;
  sidewalk = new Sidewalk(null, null,
    new Country(null, null, null),
    new Department(null, null, null, null),
    new City(null, null, null, null, null),
    null);
  private sub: any;
  cityList = [];

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _sidewalkService: SidewalkService,
    private _cityService: CityService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getSidewalk();
      }
    });
    this.getAllCities();
  }

  getSidewalk() {
    this._sidewalkService.getSidewalk(this.id).then(
      data => {
        this.sidewalk = new Sidewalk(data._id, data.code, data.country, data.department, data.city, data.description);
      }
    ).catch(error => console.log(error));

  }

  getAllCities() {
    this._cityService.getCities()
      .then(data => {
        this.cityList = data;
      })
      .catch(error => console.log(error));
  }



  onSubmit(f: any) {
    this._sidewalkService
      .saveSidewalk(this.sidewalk)
      .then(data => {
        this._router.navigate(['/admin/sidewalks']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}