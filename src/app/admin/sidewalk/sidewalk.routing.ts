import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { SidewalkComponent } from './sidewalk.component';
import { SidewalkListComponent } from './sidewalk-list/sidewalk-list.component';
import { SidewalkFormComponent } from './sidewalk-form/sidewalk-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: SidewalkComponent,

    children: [{
      path: '',
      component: SidewalkListComponent,
    },{
      path: 'new',
      component: SidewalkFormComponent,
    },{
      path: 'edit/:id',
      component: SidewalkFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SidewalkRoutingModule {}
