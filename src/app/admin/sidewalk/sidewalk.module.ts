import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { SidewalkComponent } from './sidewalk.component';
import { SidewalkRoutingModule } from './sidewalk.routing';
import { SidewalkListComponent } from './sidewalk-list/sidewalk-list.component';
import { SidewalkFormComponent } from './sidewalk-form/sidewalk-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SidewalkRoutingModule,
    DataTablesModule
  ],
  declarations: [ SidewalkComponent, SidewalkListComponent, SidewalkFormComponent]
})
export class SidewalkModule { }
