import { City } from '../city/city';
import { Country } from '../country/country';
import { Department } from '../department/department';

export class Sidewalk {
    constructor(
    public _id: number,
    public code: string,
    public country: Country,
    public department: Department,
    public city: City,
    public description: string
  ) {  }
}
