import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { SectorComponent } from './sector.component';
import { SectorRoutingModule } from './sector.routing';
import { SectorListComponent } from './sector-list/sector-list.component';
import { SectorFormComponent } from './sector-form/sector-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SectorRoutingModule,
    DataTablesModule
  ],
  declarations: [ SectorComponent, SectorListComponent, SectorFormComponent]
})
export class SectorModule { }
