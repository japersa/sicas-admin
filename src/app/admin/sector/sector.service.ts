import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Sector } from './sector';
import { environment } from '../../../environments/environment';

@Injectable()
export class SectorService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getSectors(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/sectors`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSector(id: number): Promise < Sector > {

        let url = `${this.apiEndpoint}/sectors/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveSector(sector: Sector): Promise < Object > {
        if (sector._id) {
            return this.updateSector(sector);
        }
        return this.addSector(sector);
    }


    private addSector(sector: Sector): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/sectors`;

        return this.http.post(url, sector, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateSector(sector: Sector) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/sectors/${sector._id}`;

        return this.http
            .patch(url, sector, {
                headers: headers
            })
            .toPromise()
            .then(() => sector)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
