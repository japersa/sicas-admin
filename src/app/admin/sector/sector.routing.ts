import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { SectorComponent } from './sector.component';
import { SectorListComponent } from './sector-list/sector-list.component';
import { SectorFormComponent } from './sector-form/sector-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: SectorComponent,

    children: [{
      path: '',
      component: SectorListComponent,
    },{
      path: 'new',
      component: SectorFormComponent,
    },{
      path: 'edit/:id',
      component: SectorFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SectorRoutingModule {}
