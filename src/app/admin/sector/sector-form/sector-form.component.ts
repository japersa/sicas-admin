import { Component, OnInit, OnDestroy } from '@angular/core';
import { Sector } from '../sector';
import { Router, ActivatedRoute } from '@angular/router';
import { SectorService } from '../sector.service';

@Component({
  selector: 'app-sector-form',
  templateUrl: './sector-form.component.html',
  styleUrls: ['./sector-form.component.scss'],
  providers: [SectorService]
})

export class SectorFormComponent implements OnInit, OnDestroy {

  id: number;
  sector = new Sector(null, null, null);
  private sub: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _sectorService: SectorService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getSector();
      }
    });
  }

  getSector() {
    this._sectorService.getSector(this.id).then(
      data => {
        this.sector = new Sector(data._id, data.code, data.description);
      }
    ).catch(error => console.log(error));

  }

  onSubmit(f: any) {
    this._sectorService
      .saveSector(this.sector)
      .then(data => {
        this._router.navigate(['/admin/sectors']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}