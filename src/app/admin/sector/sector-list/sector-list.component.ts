import { Component, OnInit } from '@angular/core';
import { SectorService } from '../sector.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-sector-list',
  templateUrl: './sector-list.component.html',
  styleUrls: ['./sector-list.component.scss'],
  providers: [SectorService]
})
export class SectorListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  sectorList = [];

  constructor(private _sectorService: SectorService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllSectors();
  }

  getAllSectors() {
    this._sectorService.getSectors()
      .then(data => {
        this.sectorList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
