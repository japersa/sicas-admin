import { Permission } from "../permission/permission";

export class Role {
    constructor(
    public _id: number,
    public name: string,
    public description: string,
    public permissions: Permission[]
  ) {  }
}
