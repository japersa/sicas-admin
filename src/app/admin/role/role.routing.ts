import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { RoleComponent } from './role.component';
import { RoleFormComponent } from './role-form/role-form.component';
import { RoleListComponent } from './role-list/role-list.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: RoleComponent,

    children: [{
      path: '',
      component: RoleListComponent,
    },{
      path: 'new',
      component: RoleFormComponent,
    },{
      path: 'edit/:id',
      component: RoleFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleRoutingModule {}
