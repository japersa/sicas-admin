import { Component, OnInit, OnDestroy } from '@angular/core';
import { RoleService } from '../role.service';
import { Role } from '../role';
import { Router, ActivatedRoute } from '@angular/router';
import { PermissionService } from '../../permission/permission.service';

@Component({
  selector: 'app-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.css'],
  providers: [RoleService, PermissionService]
})

export class RoleFormComponent implements OnInit, OnDestroy {

  id: number;
  role = new Role(null, null, null, null);
  permissions= [];
  private sub: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _roleService: RoleService,
    private _permissionService: PermissionService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getRole();
      }
      this.getAllPermissions();
    });
  }

  getRole() {
      this._roleService.getRole(this.id).then(
        data => {
          console.log(data);
          this.role = new Role(data._id, data.name, data.description, data.permissions);
          this.checkPermission();
        }
      ).catch(error => console.log(error));
  }

  checkPermission(){
    if(this.role.permissions != null && this.permissions != null){
      this.role.permissions.forEach(permissionRole => {
        this.permissions.forEach((permission, index) => {
          if(permissionRole._id == permission._id){
            this.permissions[index].checked = true;
          }
        });
      });
    }
  }

  changeCheckbox(event, index){
    this.permissions[index].checked = event.target.checked;
  }

  selectAll(event) {
    for (var i = 0; i <  this.permissions.length; i++) {
      this.permissions[i].checked = event.target.checked;
    }
  }

  getAllPermissions() {
    this._permissionService.getPermissions()
      .then(data => {
        this.permissions = data;
        this.checkPermission();
      })
      .catch(error => console.log(error));
  }

  onSubmit(f: any) {
    this.role.permissions = this.permissions.filter(function(permission) {
      return permission.checked;
    });
    
    this._roleService
      .saveRole(this.role)
      .then(data => {
        this._router.navigate(['/admin/roles']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}

