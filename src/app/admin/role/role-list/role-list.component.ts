import { Component, OnInit } from '@angular/core';
import { RoleService } from '../role.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss'],
  providers: [RoleService]
})
export class RoleListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  roleList = [];

  constructor(private _roleService: RoleService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllCountries();
  }

  getAllCountries() {
    this._roleService.getRoles()
      .then(data => {
        this.roleList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
