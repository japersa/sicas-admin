import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { RoleComponent } from './role.component';
import { RoleRoutingModule } from './role.routing';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleFormComponent } from './role-form/role-form.component';
import { CommonModule } from '@angular/common';
import { SearchFilterPipe } from '../searchfilter.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RoleRoutingModule,
    DataTablesModule
  ],
  declarations: [ RoleComponent, RoleListComponent, RoleFormComponent,  SearchFilterPipe]
})
export class RoleModule { }
