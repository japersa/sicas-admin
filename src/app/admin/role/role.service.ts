import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Role } from './role';
import { environment } from '../../../environments/environment';

@Injectable()
export class RoleService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getRoles(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/roles`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getRole(id: number): Promise < Role > {

        let url = `${this.apiEndpoint}/roles/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveRole(role: Role): Promise < Object > {
        if (role._id) {
            return this.updateRole(role);
        }
        return this.addRole(role);
    }


    private addRole(role: Role): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/roles`;

        return this.http.post(url, role, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateRole(role: Role) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/roles/${role._id}`;

        return this.http
            .patch(url, role, {
                headers: headers
            }) 
            .toPromise()
            .then(() => role)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
