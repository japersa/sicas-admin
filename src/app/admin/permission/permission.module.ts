import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { PermissionComponent } from './permission.component';
import { PermissionRoutingModule } from './permission.routing';
import { PermissionListComponent } from './permission-list/permission-list.component';
import { PermissionFormComponent } from './permission-form/permission-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PermissionRoutingModule,
    DataTablesModule
  ],
  declarations: [ PermissionComponent, PermissionListComponent, PermissionFormComponent]
})
export class PermissionModule { }
