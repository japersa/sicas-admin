import { Component, OnInit, OnDestroy } from '@angular/core';
import { Permission } from '../permission';
import { Router, ActivatedRoute } from '@angular/router';
import { PermissionService } from '../permission.service';

@Component({
  selector: 'app-permission-form',
  templateUrl: './permission-form.component.html',
  styleUrls: ['./permission-form.component.scss'],
  providers: [PermissionService]
})

export class PermissionFormComponent implements OnInit, OnDestroy {

  id: number;
  permission = new Permission(null, null, null);
  private sub: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _permissionService: PermissionService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getPermission();
      }
    });
  }

  getPermission() {
    this._permissionService.getPermission(this.id).then(
      data => {
        this.permission = new Permission(data._id, data.name, data.description);
      }
    ).catch(error => console.log(error));

  }

  onSubmit(f: any) {
    this._permissionService
      .savePermission(this.permission)
      .then(data => {
        this._router.navigate(['/admin/permissions']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}