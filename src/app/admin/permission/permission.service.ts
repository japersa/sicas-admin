import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Permission } from './permission';
import { environment } from '../../../environments/environment';

@Injectable()
export class PermissionService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getPermissions(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/permissions`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPermission(id: number): Promise < Permission > {

        let url = `${this.apiEndpoint}/permissions/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    savePermission(permission: Permission): Promise < Object > {
        if (permission._id) {
            return this.updatePermission(permission);
        }
        return this.addPermission(permission);
    }


    private addPermission(permission: Permission): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/permissions`;

        return this.http.post(url, permission, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updatePermission(permission: Permission) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/permissions/${permission._id}`;

        return this.http
            .patch(url, permission, {
                headers: headers
            })
            .toPromise()
            .then(() => permission)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
