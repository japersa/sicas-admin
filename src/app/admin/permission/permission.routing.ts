import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { PermissionComponent } from './permission.component';
import { PermissionListComponent } from './permission-list/permission-list.component';
import { PermissionFormComponent } from './permission-form/permission-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: PermissionComponent,

    children: [{
      path: '',
      component: PermissionListComponent,
    },{
      path: 'new',
      component: PermissionFormComponent,
    },{
      path: 'edit/:id',
      component: PermissionFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermissionRoutingModule {}
