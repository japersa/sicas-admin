export class Permission {
    constructor(
    public _id: number,
    public name: string,
    public description: string 
  ) {  }
}
