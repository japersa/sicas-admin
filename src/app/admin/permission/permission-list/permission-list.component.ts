import { Component, OnInit } from '@angular/core';
import { PermissionService } from '../permission.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-permission-list',
  templateUrl: './permission-list.component.html',
  styleUrls: ['./permission-list.component.scss'],
  providers: [PermissionService]
})
export class PermissionListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  permissionList = [];

  constructor(private _permissionService: PermissionService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllPermissions();
  }

  getAllPermissions() {
    this._permissionService.getPermissions()
      .then(data => {
        this.permissionList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
