import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { SurveyComponent } from './survey.component';
import { SurveyListComponent } from './survey-list/survey-list.component';
import { SurveyFormComponent } from './survey-form/survey-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: SurveyComponent,

    children: [{
      path: '',
      component: SurveyListComponent,
    },{
      path: 'new',
      component: SurveyFormComponent,
    },{
      path: 'edit/:id',
      component: SurveyFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SurveyRoutingModule {}
