import { Component, OnInit, OnDestroy } from '@angular/core';
import { Survey } from '../survey';
import { Router, ActivatedRoute } from '@angular/router';
import { SurveyService } from '../survey.service';
import { SurveyType } from '../../survey-type/survey-type';
import { SurveyTypeService } from '../../survey-type/survey-type.service';

@Component({
  selector: 'app-survey-form',
  templateUrl: './survey-form.component.html',
  styleUrls: ['./survey-form.component.scss'],
  providers: [SurveyService, SurveyTypeService]
})

export class SurveyFormComponent implements OnInit, OnDestroy {

  id: number;
  survey = new Survey(null, null, null, null, new SurveyType(null, null, null, null, null), null);
  private sub: any;
  surveyTypeList = [];

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _surveyService: SurveyService,
    private _surveyTypeService: SurveyTypeService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getSurvey();
      } else {
        this.survey.access_key =  Math.random().toString(36).slice(-8);
      }
    });
    this.getAllSurveyTypes();
  }

  getSurvey() {
    this._surveyService.getSurvey(this.id).then(
      data => {
        this.survey = new Survey(data._id,
          data.name,
          data.description,
          data.lock,
          data.survey_type,
          data.access_key);
      }
    ).catch(error => console.log(error));

  }

  getAllSurveyTypes() {
    this._surveyTypeService.getSurveyTypes()
      .then(data => {
        this.surveyTypeList = data;
      })
      .catch(error => console.log(error));
  }


  onSubmit(f: any) {
    this._surveyService
      .saveSurvey(this.survey)
      .then(data => {
        this._router.navigate(['/admin/surveys']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}