import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { SurveyComponent } from './survey.component';
import { SurveyRoutingModule } from './survey.routing';
import { SurveyListComponent } from './survey-list/survey-list.component';
import { SurveyFormComponent } from './survey-form/survey-form.component';
import { CommonModule } from '@angular/common';
import { SurveyModalComponent } from './survey-modal/survey-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SurveyRoutingModule,
    DataTablesModule
  ],
  declarations: [ SurveyComponent, SurveyListComponent, SurveyFormComponent, SurveyModalComponent]
})
export class SurveyModule { }
