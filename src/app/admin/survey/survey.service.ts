import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Survey } from './survey';
import { environment } from '../../../environments/environment';

@Injectable()
export class SurveyService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getSurveys(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/surveys`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSurvey(id: number): Promise < Survey > {

        let url = `${this.apiEndpoint}/surveys/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveSurvey(department: Survey): Promise < Object > {
        if (department._id) {
            return this.updateSurvey(department);
        }
        return this.addSurvey(department);
    }


    private addSurvey(department: Survey): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/surveys`;

        return this.http.post(url, department, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateSurvey(department: Survey) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/surveys/${department._id}`;

        return this.http
            .patch(url, department, {
                headers: headers
            })
            .toPromise()
            .then(() => department)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
