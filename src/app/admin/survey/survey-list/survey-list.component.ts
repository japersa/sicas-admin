import { Component, OnInit } from '@angular/core';
import { SurveyService } from '../survey.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.scss'],
  providers: [SurveyService]
})
export class SurveyListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  surveyList = [];

  constructor(private _surveyService: SurveyService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllSurveys();
  }

  getAllSurveys() {
    this._surveyService.getSurveys()
      .then(data => {
        this.surveyList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
