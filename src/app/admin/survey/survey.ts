import { SurveyType } from '../survey-type/survey-type';

export class Survey {
    constructor(
    public _id: number,
    public name: string,
    public description: string,
    public lock: boolean,
    public survey_type: SurveyType,
    public access_key: string
  ) {  }
}
