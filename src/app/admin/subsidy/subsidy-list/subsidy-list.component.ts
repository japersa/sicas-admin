import { Component, OnInit } from '@angular/core';
import { SubsidyService } from '../subsidy.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-subsidy-list',
  templateUrl: './subsidy-list.component.html',
  styleUrls: ['./subsidy-list.component.scss'],
  providers: [SubsidyService]
})
export class SubsidyListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  subsidyList = [];

  constructor(private _subsidyService: SubsidyService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllSubsidies();
  }

  getAllSubsidies() {
    this._subsidyService.getSubsidies()
      .then(data => {
        this.subsidyList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));
      

  }

}
