import { SubsidyType } from '../subsidy-type/subsidy-type';

export class Subsidy {
    constructor(
    public _id: number,
    public code: string,
    public description: string,
    public subsidy_type: SubsidyType
  ) {  }
}
