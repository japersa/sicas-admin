import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { SubsidyComponent } from './subsidy.component';
import { SubsidyRoutingModule } from './subsidy.routing';
import { SubsidyListComponent } from './subsidy-list/subsidy-list.component';
import { SubsidyFormComponent } from './subsidy-form/subsidy-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SubsidyRoutingModule,
    DataTablesModule
  ],
  declarations: [ SubsidyComponent, SubsidyListComponent, SubsidyFormComponent]
})
export class SubsidyModule { }
