import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subsidy } from '../subsidy';
import { Router, ActivatedRoute } from '@angular/router';
import { SubsidyService } from '../subsidy.service';
import { DepartmentService } from '../../department/department.service';
import { Department } from '../../department/department';
import { SubsidyType } from '../../subsidy-type/subsidy-type';
import { SubsidyTypeService } from '../../subsidy-type/subsidy-type.service';

@Component({
  selector: 'app-subsidy-form',
  templateUrl: './subsidy-form.component.html',
  styleUrls: ['./subsidy-form.component.scss'],
  providers: [SubsidyService, SubsidyTypeService]
})

export class SubsidyFormComponent implements OnInit, OnDestroy {

  id: number;
  subsidy = new Subsidy(null, null, null, new SubsidyType(null, null));
  private sub: any;
  subsidyTypeList = [];

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _subsidyService: SubsidyService,
    private _subsidyTypeService: SubsidyTypeService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getSubsidy();
      }
    });
    this.getAllSubsidyTypes();
  }

  getSubsidy() {
    this._subsidyService.getSubsidy(this.id).then(
      data => {
        this.subsidy = new Subsidy(data._id, data.code, data.description, data.subsidy_type);
      }
    ).catch(error => console.log(error));

  }

  getAllSubsidyTypes() {
    this._subsidyTypeService.getSubsidyTypes()
      .then(data => {
        this.subsidyTypeList = data;
      })
      .catch(error => console.log(error));
  }

  onSubmit(f: any) {
    this._subsidyService
      .saveSubsidy(this.subsidy)
      .then(data => {
        this._router.navigate(['/admin/subsidies']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}