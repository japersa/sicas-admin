import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { SubsidyComponent } from './subsidy.component';
import { SubsidyListComponent } from './subsidy-list/subsidy-list.component';
import { SubsidyFormComponent } from './subsidy-form/subsidy-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: SubsidyComponent,

    children: [{
      path: '',
      component: SubsidyListComponent,
    },{
      path: 'new',
      component: SubsidyFormComponent,
    },{
      path: 'edit/:id',
      component: SubsidyFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubsidyRoutingModule {}
