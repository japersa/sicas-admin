import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Subsidy } from './subsidy';
import { environment } from '../../../environments/environment';

@Injectable()
export class SubsidyService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getSubsidies(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/subsidies`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSubsidy(id: number): Promise < Subsidy > {

        let url = `${this.apiEndpoint}/subsidies/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveSubsidy(subsidy: Subsidy): Promise < Object > {
        if (subsidy._id) {
            return this.updateSubsidy(subsidy);
        }
        return this.addSubsidy(subsidy);
    }


    private addSubsidy(subsidy: Subsidy): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/subsidies`;

        return this.http.post(url, subsidy, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateSubsidy(subsidy: Subsidy) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/subsidies/${subsidy._id}`;

        return this.http
            .patch(url, subsidy, {
                headers: headers
            })
            .toPromise()
            .then(() => subsidy)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
