/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SubsidyService } from './subsidy.service';

describe('Service: Subsidy', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubsidyService]
    });
  });

  it('should ...', inject([SubsidyService], (service: SubsidyService) => {
    expect(service).toBeTruthy();
  }));
});
