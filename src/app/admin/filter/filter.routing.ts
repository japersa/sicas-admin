import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { FilterComponent } from './filter.component';
import { FilterListComponent } from './filter-list/filter-list.component';
import { FilterFormComponent } from './filter-form/filter-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: FilterComponent,

    children: [{
      path: '',
      component: FilterListComponent,
    },{
      path: 'new',
      component: FilterFormComponent,
    },{
      path: 'edit/:id',
      component: FilterFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FilterRoutingModule {}
