import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Filter } from './filter';
import { environment } from '../../../environments/environment';

@Injectable()
export class FilterService {
  
  private apiEndpoint = environment.apiUrl;

  constructor(private http: Http) {}

  getFilters(): Promise < Object[] > {

      let url = `${this.apiEndpoint}/filters`;

      return this.http.get(url)
          .toPromise()
          .then(response => response.json())
          .catch(this.handleError);
  }

  getFilter(id: number): Promise < Filter > {

      let url = `${this.apiEndpoint}/filters/${id}`;

      return this.http.get(url)
          .toPromise()
          .then(response => response.json())
          .catch(this.handleError);
  }

  saveFilter(filter: Filter): Promise < Object > {
      if (filter._id) {
          return this.updateFilter(filter);
      }
      return this.addFilter(filter);
  }


  private addFilter(filter: Filter): Promise < Object > {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      let url = `${this.apiEndpoint}/filters`;

      return this.http.post(url, filter, {
              headers: headers
          })
          .toPromise()
          .then(response => response.json())
          .catch(this.handleError);
  }

  private updateFilter(filter: Filter) {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      let url = `${this.apiEndpoint}/filters/${filter._id}`;

      return this.http
          .patch(url, filter, {
              headers: headers
          })
          .toPromise()
          .then(() => filter)
          .catch(this.handleError);
  }

  private handleError(error: any) {
      console.error('error', error);
      return Promise.reject(error.message || error);
  }

}
