import { Component, OnInit } from '@angular/core';
import { FilterService } from '../filter.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-filter-list',
  templateUrl: './filter-list.component.html',
  styleUrls: ['./filter-list.component.css'],
  providers: [FilterService]
})
export class FilterListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  filterList = [];

  constructor(private _filterService: FilterService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllFilters();
  }

  getAllFilters() {
    this._filterService.getFilters()
      .then(data => {
        this.filterList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
