import { Component, OnInit, OnDestroy } from '@angular/core';
import { Filter } from '../filter';
import { Router, ActivatedRoute } from '@angular/router';
import { FilterService } from '../../filter/filter.service';

@Component({
  selector: 'app-filter-form',
  templateUrl: './filter-form.component.html',
  styleUrls: ['./filter-form.component.css'],
  providers: [FilterService]
})
export class FilterFormComponent implements OnInit, OnDestroy {
  
  id: number;
  filter = new Filter(null, null, null);
  private sub: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _filterService: FilterService
    ) { }

    ngOnInit() {
      this.sub = this._activatedRoute.params.subscribe(params => {
        if(params['id']){
          this.id = params['id'];
          this.getFilter();
        }
      });
    }
  
    getFilter() {
      this._filterService.getFilter(this.id).then(
        data => {
          this.filter = new Filter(data._id, data.title, data.description);
        }
      ).catch(error => console.log(error));
  
    }
  
    onSubmit(f: any) {
      this._filterService
        .saveFilter(this.filter)
        .then(data => {
          this._router.navigate(['/admin/filters']);
        })
        .catch(error => {
          console.log(error);
        });
    }
  
    ngOnDestroy() {
      this.sub.unsubscribe();
    }

}
