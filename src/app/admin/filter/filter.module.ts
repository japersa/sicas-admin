import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter.component';
import { FilterListComponent } from './filter-list/filter-list.component';
import { FilterFormComponent } from './filter-form/filter-form.component';
import { FilterRoutingModule } from './filter.routing'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FilterRoutingModule,
    DataTablesModule
  ],
  declarations: [FilterComponent, FilterListComponent, FilterFormComponent]
})
export class FilterModule { }
