import { OnDestroy, OnInit, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';
import { RoleService } from '../../role/role.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css'],
  providers: [UserService, RoleService]
})

export class UserFormComponent implements OnInit, OnDestroy {

  id: number;
  user = new User(null, null, null, null, null);
  private sub: any;
  roles_ids = [];
  roles = [];

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _userService: UserService,
    private _roleService: RoleService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getUser();
      }
    });
    this.getAllRoles();
  }

  getUser() {
    this._userService.getUser(this.id).then(
      data => {
        this.user = new User(data._id, data.name, data.email, null, data.roles);
        this.checkUserRoles();
      }
    ).catch(error => console.log(error));
  }


  getAllRoles() {
    this._roleService.getRoles()
      .then(data => {
        this.roles = data;
        this.checkUserRoles();
      })
      .catch(error => console.log(error));
  }

  checkUserRoles(){
      this.roles.forEach(element => {
        this.user.roles.forEach(role => {
          if(element._id == role._id){
            this.roles_ids.push(element._id);
          }
        });
      });
  }

  setUserRoles(){
    this.user.roles = [];
    this.roles_ids.forEach(element => {
      this.roles.forEach(role => {
        if(element == role._id){
          this.user.roles.push(role);
        }
      });
    });
  }

  onSubmit(f: any) {
    this.setUserRoles();
    console.log(this.roles_ids);
    this._userService
      .saveUser(this.user)
      .then(data => {
        this._router.navigate(['/admin/users']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
