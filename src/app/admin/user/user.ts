import { Role } from '../role/role';

export class User {
    constructor(
    public _id: number,
    public name: string,
    public email: string,
    public password: string,
    public roles: Role[]
  ) {  }
}
