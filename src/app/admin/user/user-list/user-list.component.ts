import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  providers: [UserService]
})
export class UserListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  userList = [];

  constructor(private _userService: UserService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllUsers();
  }

  getAllUsers() {
    this._userService.getUsers()
      .then(data => {
        this.userList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
