import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { UserComponent } from './user.component';
import { UserRoutingModule } from './user.routing';
import { UserListComponent } from './user-list/user-list.component';
import { UserFormComponent } from './user-form/user-form.component';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UserRoutingModule,
    DataTablesModule,
    NgSelectModule
  ],
  declarations: [ UserComponent, UserListComponent, UserFormComponent]
})
export class UserModule { }
