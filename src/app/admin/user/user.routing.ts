import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { UserComponent } from './user.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserFormComponent } from './user-form/user-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,

    children: [{
      path: '',
      component: UserListComponent,
    },{
      path: 'new',
      component: UserFormComponent,
    },{
      path: 'edit/:id',
      component: UserFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
