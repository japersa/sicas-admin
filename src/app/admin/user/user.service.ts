import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { User } from './user';
import { environment } from '../../../environments/environment';

@Injectable()
export class UserService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getUsers(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/users`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getUser(id: number): Promise < User > {

        let url = `${this.apiEndpoint}/users/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveUser(user: User): Promise < Object > {
        if (user._id) {
            return this.updateUser(user);
        }
        return this.addUser(user);
    }


    private addUser(user: User): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/users`;

        return this.http.post(url, user, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateUser(user: User) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/users/${user._id}`;

        return this.http
            .patch(url, user, {
                headers: headers
            })
            .toPromise()
            .then(() => user)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
