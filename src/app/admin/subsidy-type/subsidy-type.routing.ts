import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { SubsidyTypeComponent } from './subsidy-type.component';
import { SubsidyTypeListComponent } from './subsidy-type-list/subsidy-type-list.component';
import { SubsidyTypeFormComponent } from './subsidy-type-form/subsidy-type-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: SubsidyTypeComponent,

    children: [{
      path: '',
      component: SubsidyTypeListComponent,
    },{
      path: 'new',
      component: SubsidyTypeFormComponent,
    },{
      path: 'edit/:id',
      component: SubsidyTypeFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubsidyTypeRoutingModule {}
