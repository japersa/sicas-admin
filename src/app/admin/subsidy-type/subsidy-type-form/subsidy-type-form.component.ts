import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubsidyType } from '../subsidy-type';
import { Router, ActivatedRoute } from '@angular/router';
import { SubsidyTypeService } from '../subsidy-type.service';

@Component({
  selector: 'app-subsidy-type-form',
  templateUrl: './subsidy-type-form.component.html',
  styleUrls: ['./subsidy-type-form.component.scss'],
  providers: [SubsidyTypeService]
})

export class SubsidyTypeFormComponent implements OnInit, OnDestroy {

  id: number;
  subsidyType = new SubsidyType(null, null);
  private sub: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _subsidyTypeService: SubsidyTypeService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getSubsidyType();
      }
    });
  }

  getSubsidyType() {
    this._subsidyTypeService.getSubsidyType(this.id).then(
      data => {
        this.subsidyType = new SubsidyType(data._id, data.description);
      }
    ).catch(error => console.log(error));

  }

  onSubmit(f: any) {
    this._subsidyTypeService
      .saveSubsidyType(this.subsidyType)
      .then(data => {
        this._router.navigate(['/admin/subsidy-types']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}