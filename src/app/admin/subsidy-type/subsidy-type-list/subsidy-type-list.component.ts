import { Component, OnInit } from '@angular/core';
import { SubsidyTypeService } from '../subsidy-type.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-subsidy-type-list',
  templateUrl: './subsidy-type-list.component.html',
  styleUrls: ['./subsidy-type-list.component.scss'],
  providers: [SubsidyTypeService]
})
export class SubsidyTypeListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  subsidyTypeList = [];

  constructor(private _subsidyTypeService: SubsidyTypeService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllSubsidyTypes();
  }

  getAllSubsidyTypes() {
    this._subsidyTypeService.getSubsidyTypes()
      .then(data => {
        this.subsidyTypeList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
