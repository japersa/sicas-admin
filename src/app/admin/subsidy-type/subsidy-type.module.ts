import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { SubsidyTypeComponent } from './subsidy-type.component';
import { SubsidyTypeRoutingModule } from './subsidy-type.routing';
import { SubsidyTypeListComponent } from './subsidy-type-list/subsidy-type-list.component';
import { SubsidyTypeFormComponent } from './subsidy-type-form/subsidy-type-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SubsidyTypeRoutingModule,
    DataTablesModule
  ],
  declarations: [ SubsidyTypeComponent, SubsidyTypeListComponent, SubsidyTypeFormComponent]
})
export class SubsidyTypeModule { }
