/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SubsidyTypeService } from './subsidy-type.service';

describe('Service: SubsidyType', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubsidyTypeService]
    });
  });

  it('should ...', inject([SubsidyTypeService], (service: SubsidyTypeService) => {
    expect(service).toBeTruthy();
  }));
});
