import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-subsidy-type',
  templateUrl: './subsidy-type.component.html',
  styleUrls: ['./subsidy-type.component.scss']
})
export class SubsidyTypeComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

}
