import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { SubsidyType } from './subsidy-type';
import { environment } from '../../../environments/environment';

@Injectable()
export class SubsidyTypeService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getSubsidyTypes(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/subsidy-types`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSubsidyType(id: number): Promise < SubsidyType > {

        let url = `${this.apiEndpoint}/subsidy-types/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveSubsidyType(department: SubsidyType): Promise < Object > {
        if (department._id) {
            return this.updateSubsidyType(department);
        }
        return this.addSubsidyType(department);
    }


    private addSubsidyType(department: SubsidyType): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/subsidy-types`;

        return this.http.post(url, department, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateSubsidyType(department: SubsidyType) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/subsidy-types/${department._id}`;

        return this.http
            .patch(url, department, {
                headers: headers
            })
            .toPromise()
            .then(() => department)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
