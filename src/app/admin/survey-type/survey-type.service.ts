import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { SurveyType } from './survey-type';
import { environment } from '../../../environments/environment';

@Injectable()
export class SurveyTypeService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getSurveyTypes(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/survey-types`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSurveyType(id: number): Promise < SurveyType > {

        let url = `${this.apiEndpoint}/survey-types/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveSurveyType(department: SurveyType): Promise < Object > {
        if (department._id) {
            return this.updateSurveyType(department);
        }
        return this.addSurveyType(department);
    }


    private addSurveyType(department: SurveyType): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/survey-types`;

        return this.http.post(url, department, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateSurveyType(department: SurveyType) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/survey-types/${department._id}`;

        return this.http
            .patch(url, department, {
                headers: headers
            })
            .toPromise()
            .then(() => department)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
