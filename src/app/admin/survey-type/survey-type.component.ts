import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-survey-type',
  templateUrl: './survey-type.component.html',
  styleUrls: ['./survey-type.component.scss']
})
export class SurveyTypeComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

}
