import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { SurveyTypeComponent } from './survey-type.component';
import { SurveyTypeRoutingModule } from './survey-type.routing';
import { SurveyTypeListComponent } from './survey-type-list/survey-type-list.component';
import { SurveyTypeFormComponent } from './survey-type-form/survey-type-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SurveyTypeRoutingModule,
    DataTablesModule
  ],
  declarations: [ SurveyTypeComponent, SurveyTypeListComponent, SurveyTypeFormComponent]
})
export class SurveyTypeModule { }
