import { Component, OnInit, OnDestroy } from '@angular/core';
import { SurveyType } from '../survey-type';
import { Router, ActivatedRoute } from '@angular/router';
import { SurveyTypeService } from '../survey-type.service';

@Component({
  selector: 'app-survey-type-form',
  templateUrl: './survey-type-form.component.html',
  styleUrls: ['./survey-type-form.component.scss'],
  providers: [SurveyTypeService]
})

export class SurveyTypeFormComponent implements OnInit, OnDestroy {

  id: number;
  surveyType = new SurveyType(null, null, null, null, null);
  private sub: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _surveyTypeService: SurveyTypeService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getSurveyType();
      }
    });
  }

  getSurveyType() {
    this._surveyTypeService.getSurveyType(this.id).then(
      data => {
        this.surveyType = new SurveyType(data._id, data.modifiable, data.multiple, data.field, data.answer);
      }
    ).catch(error => console.log(error));

  }

  onSubmit(f: any) {
    this._surveyTypeService
      .saveSurveyType(this.surveyType)
      .then(data => {
        this._router.navigate(['/admin/survey-types']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}