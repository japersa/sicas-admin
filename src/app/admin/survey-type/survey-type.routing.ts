import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { SurveyTypeComponent } from './survey-type.component';
import { SurveyTypeListComponent } from './survey-type-list/survey-type-list.component';
import { SurveyTypeFormComponent } from './survey-type-form/survey-type-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: SurveyTypeComponent,

    children: [{
      path: '',
      component: SurveyTypeListComponent,
    },{
      path: 'new',
      component: SurveyTypeFormComponent,
    },{
      path: 'edit/:id',
      component: SurveyTypeFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SurveyTypeRoutingModule {}
