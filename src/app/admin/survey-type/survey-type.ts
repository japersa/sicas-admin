export class SurveyType {
    constructor(
    public _id: number,
    public modifiable: string,    
    public multiple : string,
    public field: string,
    public answer: string,          
  ) {  }
}
