/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SurveyTypeService } from './survey-type.service';

describe('Service: SurveyType', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SurveyTypeService]
    });
  });

  it('should ...', inject([SurveyTypeService], (service: SurveyTypeService) => {
    expect(service).toBeTruthy();
  }));
});
