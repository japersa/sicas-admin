import { Component, OnInit } from '@angular/core';
import { SurveyTypeService } from '../survey-type.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-survey-type-list',
  templateUrl: './survey-type-list.component.html',
  styleUrls: ['./survey-type-list.component.scss'],
  providers: [SurveyTypeService]
})
export class SurveyTypeListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  surveyTypeList = [];

  constructor(private _surveyTypeService: SurveyTypeService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllSurveyTypes();
  }

  getAllSurveyTypes() {
    this._surveyTypeService.getSurveyTypes()
      .then(data => {
        this.surveyTypeList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
