import { Component, OnInit } from '@angular/core';
import { DependenceService } from '../dependence.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-dependence-list',
  templateUrl: './dependence-list.component.html',
  styleUrls: ['./dependence-list.component.scss'],
  providers: [DependenceService]
})
export class DependenceListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  dependenceList = [];

  constructor(private _dependenceService: DependenceService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllDependences();
  }

  getAllDependences() {
    this._dependenceService.getDependences()
      .then(data => {
        this.dependenceList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
