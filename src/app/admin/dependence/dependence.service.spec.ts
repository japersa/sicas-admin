/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DependenceService } from './dependence.service';

describe('Service: Dependence', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DependenceService]
    });
  });

  it('should ...', inject([DependenceService], (service: DependenceService) => {
    expect(service).toBeTruthy();
  }));
});
