import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Dependence } from './dependence';
import { environment } from '../../../environments/environment';

@Injectable()
export class DependenceService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getDependences(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/dependences`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getDependence(id: number): Promise < Dependence > {

        let url = `${this.apiEndpoint}/dependences/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveDependence(dependence: Dependence): Promise < Object > {
        if (dependence._id) {
            return this.updateDependence(dependence);
        }
        return this.addDependence(dependence);
    }


    private addDependence(dependence: Dependence): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/dependences`;

        return this.http.post(url, dependence, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateDependence(dependence: Dependence) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/dependences/${dependence._id}`;

        return this.http
            .patch(url, dependence, {
                headers: headers
            })
            .toPromise()
            .then(() => dependence)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
