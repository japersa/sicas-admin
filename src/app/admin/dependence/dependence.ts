export class Dependence {
    constructor(
    public _id: number,
    public title: string,
    public description: string 
  ) {  }
}
