import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { DependenceComponent } from './dependence.component';
import { DependenceRoutingModule } from './dependence.routing';
import { DependenceListComponent } from './dependence-list/dependence-list.component';
import { DependenceFormComponent } from './dependence-form/dependence-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DependenceRoutingModule,
    DataTablesModule
  ],
  declarations: [ DependenceComponent, DependenceListComponent, DependenceFormComponent]
})
export class DependenceModule { }
