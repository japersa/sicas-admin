import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { DependenceComponent } from './dependence.component';
import { DependenceListComponent } from './dependence-list/dependence-list.component';
import { DependenceFormComponent } from './dependence-form/dependence-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: DependenceComponent,

    children: [{
      path: '',
      component: DependenceListComponent,
    },{
      path: 'new',
      component: DependenceFormComponent,
    },{
      path: 'edit/:id',
      component: DependenceFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DependenceRoutingModule {}
