import { Component, OnInit, OnDestroy } from '@angular/core';
import { Dependence } from '../dependence';
import { Router, ActivatedRoute } from '@angular/router';
import { DependenceService } from '../dependence.service';

@Component({
  selector: 'app-dependence-form',
  templateUrl: './dependence-form.component.html',
  styleUrls: ['./dependence-form.component.scss'],
  providers: [DependenceService]
})

export class DependenceFormComponent implements OnInit, OnDestroy {

  id: number;
  dependence = new Dependence(null, null, null);
  private sub: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _dependenceService: DependenceService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getDependence();
      }
    });
  }

  getDependence() {
    this._dependenceService.getDependence(this.id).then(
      data => {
        this.dependence = new Dependence(data._id, data.title, data.description);
      }
    ).catch(error => console.log(error));

  }

  onSubmit(f: any) {
    this._dependenceService
      .saveDependence(this.dependence)
      .then(data => {
        this._router.navigate(['/admin/dependences']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}