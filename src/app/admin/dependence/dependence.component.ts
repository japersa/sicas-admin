import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-dependence',
  templateUrl: './dependence.component.html',
  styleUrls: ['./dependence.component.scss']
})
export class DependenceComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

}
