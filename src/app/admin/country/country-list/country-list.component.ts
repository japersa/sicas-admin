import { Component, OnInit } from '@angular/core';
import { CountryService } from '../country.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss'],
  providers: [CountryService]
})
export class CountryListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  countryList = [];

  constructor(private _countryService: CountryService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllCountries();
  }

  getAllCountries() {
    this._countryService.getCountries()
      .then(data => {
        this.countryList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));
  }

}
