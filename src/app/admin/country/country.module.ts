import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { CountryComponent } from './country.component';
import { CountryRoutingModule } from './country.routing';
import { CountryListComponent } from './country-list/country-list.component';
import { CountryFormComponent } from './country-form/country-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CountryRoutingModule,
    DataTablesModule
  ],
  declarations: [ CountryComponent, CountryListComponent, CountryFormComponent]
})
export class CountryModule { }
