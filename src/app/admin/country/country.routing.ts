import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { CountryComponent } from './country.component';
import { CountryListComponent } from './country-list/country-list.component';
import { CountryFormComponent } from './country-form/country-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: CountryComponent,

    children: [{
      path: '',
      component: CountryListComponent,
    },{
      path: 'new',
      component: CountryFormComponent,
    },{
      path: 'edit/:id',
      component: CountryFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryRoutingModule {}
