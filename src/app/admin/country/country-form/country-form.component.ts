import { Component, OnInit, OnDestroy } from '@angular/core';
import { Country } from '../country';
import { Router, ActivatedRoute } from '@angular/router';
import { CountryService } from '../country.service';

@Component({
  selector: 'app-country-form',
  templateUrl: './country-form.component.html',
  styleUrls: ['./country-form.component.scss'],
  providers: [CountryService]
})

export class CountryFormComponent implements OnInit, OnDestroy {

  id: number;
  country = new Country(null, null, null);
  private sub: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _countryService: CountryService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getCountry();
      }
    });
  }

  getCountry() {
    this._countryService.getCountry(this.id).then(
      data => {
        this.country = new Country(data._id, data.code, data.description);
      }
    ).catch(error => console.log(error));

  }

  onSubmit(f: any) {
    this._countryService
      .saveCountry(this.country)
      .then(data => {
        this._router.navigate(['/admin/countries']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}