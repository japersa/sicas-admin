import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Country } from './country';
import { environment } from '../../../environments/environment';

@Injectable()
export class CountryService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getCountries(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/countries`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCountry(id: number): Promise < Country > {

        let url = `${this.apiEndpoint}/countries/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveCountry(country: Country): Promise < Object > {
        if (country._id) {
            return this.updateCountry(country);
        }
        return this.addCountry(country);
    }


    private addCountry(country: Country): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/countries`;

        return this.http.post(url, country, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateCountry(country: Country) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/countries/${country._id}`;

        return this.http
            .patch(url, country, {
                headers: headers
            })
            .toPromise()
            .then(() => country)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
