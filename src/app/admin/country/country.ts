export class Country {
    constructor(
    public _id: number,
    public code: string,
    public description: string 
  ) {  }
}
