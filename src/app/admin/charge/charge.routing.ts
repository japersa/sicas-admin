import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { ChargeComponent } from './charge.component';
import { ChargeListComponent } from './charge-list/charge-list.component';
import { ChargeFormComponent } from './charge-form/charge-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: ChargeComponent,
    children: [{
      path: '',
      component: ChargeListComponent,
    },{
      path: 'new',
      component: ChargeFormComponent,
    },{
      path: 'edit/:id',
      component: ChargeFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChargeRoutingModule {}
