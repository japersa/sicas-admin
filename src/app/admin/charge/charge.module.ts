import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { ChargeComponent } from './charge.component';
import { ChargeRoutingModule } from './charge.routing';
import { ChargeListComponent } from './charge-list/charge-list.component';
import { ChargeFormComponent } from './charge-form/charge-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ChargeRoutingModule,
    DataTablesModule
  ],
  declarations: [ ChargeComponent, ChargeListComponent, ChargeFormComponent]
})
export class ChargeModule { }
