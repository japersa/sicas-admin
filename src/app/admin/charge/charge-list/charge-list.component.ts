import { Component, OnInit } from '@angular/core';
import { ChargeService } from '../charge.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-charge-list',
  templateUrl: './charge-list.component.html',
  styleUrls: ['./charge-list.component.scss'],
  providers: [ChargeService]
})
export class ChargeListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  chargeList = [];

  constructor(private _chargeService: ChargeService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllCharges();
  }

  getAllCharges() {
    this._chargeService.getCharges()
      .then(data => {
        this.chargeList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
