import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Charge } from './charge';
import { environment } from '../../../environments/environment';

@Injectable()
export class ChargeService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getCharges(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/charges`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCharge(id: number): Promise < Charge > {

        let url = `${this.apiEndpoint}/charges/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveCharge(department: Charge): Promise < Object > {
        if (department._id) {
            return this.updateCharge(department);
        }
        return this.addCharge(department);
    }


    private addCharge(department: Charge): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/charges`;

        return this.http.post(url, department, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateCharge(department: Charge) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/charges/${department._id}`;

        return this.http
            .patch(url, department, {
                headers: headers
            })
            .toPromise()
            .then(() => department)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
