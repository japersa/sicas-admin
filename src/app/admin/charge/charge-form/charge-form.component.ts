import { Component, OnInit, OnDestroy } from '@angular/core';
import { Charge } from '../charge';
import { Router, ActivatedRoute } from '@angular/router';
import { ChargeService } from '../charge.service';

@Component({
  selector: 'app-charge-form',
  templateUrl: './charge-form.component.html',
  styleUrls: ['./charge-form.component.scss'],
  providers: [ChargeService]
})

export class ChargeFormComponent implements OnInit, OnDestroy {

  id: number;
  charge = new Charge(null, null, null);
  private sub: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _chargeService: ChargeService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getCharge();
      }
    });
  }

  getCharge() {
    this._chargeService.getCharge(this.id).then(
      data => {
        this.charge = new Charge(data._id, data.code, data.description);
      }
    ).catch(error => console.log(error));

  }

  onSubmit(f: any) {
    this._chargeService
      .saveCharge(this.charge)
      .then(data => {
        this._router.navigate(['/admin/charges']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}