import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventComponent } from './event.component';
import { TimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { EventCalendarComponent } from './event-calendar/event-calendar.component';
import { EventListComponent } from './event-list/event-list.component';
import {ScheduleModule, AgendaService, DayService, DragAndDropService, ResizeService, WeekService, WorkWeekService, MonthService } from '@syncfusion/ej2-angular-schedule';
import { EventRoutingModule } from './event.routing';


@NgModule({
  imports: [
    CommonModule, ScheduleModule, EventRoutingModule, TimePickerModule
  ],
  declarations: [EventComponent, EventListComponent, EventCalendarComponent],
  providers: [AgendaService, DayService, WeekService, WorkWeekService, MonthService, DragAndDropService, ResizeService],

})
export class EventModule { }
