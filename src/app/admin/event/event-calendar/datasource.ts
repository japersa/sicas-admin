export let scheduleData: Object[] = [
    {
        Id: 1,
        Subject: 'Explosion of Betelgeuse Star',
        StartTime: new Date(2018, 1, 11, 9, 30),
        EndTime: new Date(2018, 1, 11, 11, 0),
        CategoryColor: '#1aaa55'
    }
];
export let eventsData: Object[] = [
    {
        Id: 1,
        Subject: 'Server Maintenance',
        StartTime: new Date(2018, 1, 11, 10, 0),
        EndTime: new Date(2018, 1, 11, 11, 30),
        EventType: 'maintenance',
        City: 'Seattle',
        CategoryColor: '#1aaa55'
    }
];
export let eventData: Object[] = [
    {
        Id: 1,
        Subject: 'Surgery - Andrew',
        EventType: 'Confirmed',
        StartTime: new Date(2018, 1, 12, 9, 0),
        EndTime: new Date(2018, 1, 12, 10, 0)
    }
];