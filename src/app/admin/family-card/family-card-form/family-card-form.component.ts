import { Component, OnInit, OnDestroy } from '@angular/core';
import { FamilyCard } from '../family-card';
import { Router, ActivatedRoute } from '@angular/router';
import { FamilyCardService } from '../family-card.service';
import { Country } from '../../country/country';
import { Department } from '../../department/department';
import { City } from '../../city/city';
import { Commune } from '../../commune/commune';
import { Neighborhood } from '../../neighborhood/neighborhood';
import { Sidewalk } from '../../sidewalk/sidewalk';
import { CountryService } from '../../country/country.service';
import { CommuneService } from '../../commune/commune.service';
import { DepartmentService } from '../../department/department.service';
import { CityService } from '../../city/city.service';
import { NeighborhoodService } from '../../neighborhood/neighborhood.service';
import { SidewalkService } from '../../sidewalk/sidewalk.service';
import { Sector } from '../../sector/sector';
import { SectorService } from '../../sector/sector.service';

@Component({
  selector: 'app-family-card-form',
  templateUrl: './family-card-form.component.html',
  styleUrls: ['./family-card-form.component.scss'],
  providers: [FamilyCardService, CountryService, DepartmentService, CityService, CommuneService, NeighborhoodService, SidewalkService, SectorService]
})

export class FamilyCardFormComponent implements OnInit, OnDestroy {

  id: number;
  familyCard = new FamilyCard(
    null,
    new Date().getTime().toString(),
    new Country(null, null, null),
    new Department(null, null, null, null),
    new City(null, null, null, null, null),
    new Commune(null, null, null, null, null, null, null),
    new Neighborhood(null, null, null, null, null, null, null, null),
    new Sidewalk(null, null, null, null, null, null), null, null, null, null, null,
    new Sector(null, null, null));
  private sub: any;
  countryList = [];
  departmentList = [];
  cityList = [];
  communeList = [];
  neighborhoodList = [];
  sidewalkList = [];
  sectorList = [];

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _familyCardService: FamilyCardService,
    private _countryService: CountryService,
    private _departmentService: DepartmentService, 
    private _cityService: CityService, 
    private _communeService: CommuneService, 
    private _neighborhoodService: NeighborhoodService, 
    private _sidewalkService: SidewalkService,
    private _sectorService: SectorService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getFamilyCard();
      }
    });
    this.getAllCountries();
    this.getAllSectors();
  }

  getFamilyCard() {
    this._familyCardService.getFamilyCard(this.id).then(
      data => {
        this.familyCard = data;
        this.getAllDepartments();
        this.getAllCities();
        this.getAllCommunes();
        this.getAllNeighborhoods();
        this.getAllSidewalks();
      }
    ).catch(error => console.log(error));

  }

  onSubmit(f: any) {
    this._familyCardService
      .saveFamilyCard(this.familyCard)
      .then(data => {
        this._router.navigate(['/admin/family-cards']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


  getAllSectors() {
    this._sectorService.getSectors()
      .then(data => {
        this.sectorList = data;
      })
      .catch(error => console.log(error));

  }

  getAddress(place: Object) {
    var location = place['place']['geometry']['location'];
    let address = place['value'];
    console.log(place);
    this.familyCard.address = address;    
    this.familyCard.latitude = location.lat();
    this.familyCard.longitude = location.lng();
  }

  getAllCountries() {
    this._countryService.getCountries()
      .then(data => {
        this.countryList = data;
      })
      .catch(error => console.log(error));
  }

  getAllDepartments() {
    this._departmentService.getDepartmentsCountry(this.familyCard.country._id)
      .then(data => {
        this.departmentList = data;
      })
      .catch(error => console.log(error));
  }
  
  getAllCities() {
    this._cityService.getCitiesDepartment(this.familyCard.department._id)
      .then(data => {
        this.cityList = data;
      })
      .catch(error => console.log(error));
  }

  getAllCommunes() {
    this._communeService.getCommunesCity(this.familyCard.city._id)
      .then(data => {
        this.communeList = data;
      })
      .catch(error => console.log(error));
  }

  getAllNeighborhoods() {
    this._neighborhoodService.getNeighborhoods()
      .then(data => {
        this.neighborhoodList = data;
      })
      .catch(error => console.log(error));
  }

  getAllSidewalks() {
    this._sidewalkService.getSidewalksCity(this.familyCard.city._id)
      .then(data => {
        this.sidewalkList = data;
      })
      .catch(error => console.log(error));
  }

}