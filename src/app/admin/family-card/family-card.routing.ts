import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { FamilyCardComponent } from './family-card.component';
import { FamilyCardListComponent } from './family-card-list/family-card-list.component';
import { FamilyCardFormComponent } from './family-card-form/family-card-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: FamilyCardComponent,

    children: [{
      path: '',
      component: FamilyCardListComponent,
    },{
      path: 'new',
      component: FamilyCardFormComponent,
    },{
      path: 'edit/:id',
      component: FamilyCardFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FamilyCardRoutingModule {}
