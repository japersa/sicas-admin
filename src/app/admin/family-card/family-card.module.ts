import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { FamilyCardComponent } from './family-card.component';
import { FamilyCardRoutingModule } from './family-card.routing';
import { FamilyCardListComponent } from './family-card-list/family-card-list.component';
import { FamilyCardFormComponent } from './family-card-form/family-card-form.component';
import { CommonModule } from '@angular/common';
import { GoogleplaceDirective } from 'src/app/googleplace.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FamilyCardRoutingModule,
    DataTablesModule
  ],
  declarations: [ FamilyCardComponent, FamilyCardListComponent, FamilyCardFormComponent, GoogleplaceDirective]
})
export class FamilyCardModule { }
