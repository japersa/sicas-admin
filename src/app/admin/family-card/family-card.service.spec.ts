/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FamilyCardService } from './family-card.service';

describe('Service: FamilyCard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FamilyCardService]
    });
  });

  it('should ...', inject([FamilyCardService], (service: FamilyCardService) => {
    expect(service).toBeTruthy();
  }));
});
