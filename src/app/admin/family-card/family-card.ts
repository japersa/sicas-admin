import { Country } from '../country/country';
import { Department } from '../department/department';
import { City } from '../city/city';
import { Commune } from '../commune/commune';
import { Neighborhood } from '../neighborhood/neighborhood';
import { Sidewalk } from '../sidewalk/sidewalk';
import { Sector } from '../sector/sector';

export class FamilyCard {
    constructor(
    public _id: number,
    public number_card: string,
    public country: Country,
    public department: Department,
    public city: City,
    public commune: Commune,
    public neighborhood: Neighborhood,
    public sidewalk: Sidewalk,
    public address: string,
    public latitude: number,
    public longitude: number,
    public phone: string,
    public status: boolean,
    public sector: Sector
  ) {  }
}
