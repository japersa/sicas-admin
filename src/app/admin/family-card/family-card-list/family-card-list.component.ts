import { Component, OnInit } from '@angular/core';
import { FamilyCardService } from '../family-card.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-family-card-list',
  templateUrl: './family-card-list.component.html',
  styleUrls: ['./family-card-list.component.scss'],
  providers: [FamilyCardService]
})
export class FamilyCardListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  familyCardList = [];

  constructor(private _familyCardService: FamilyCardService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllFamilyCards();
  }

  getAllFamilyCards() {
    this._familyCardService.getFamilyCards()
      .then(data => {
        this.familyCardList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
