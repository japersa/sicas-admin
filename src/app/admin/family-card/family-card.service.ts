import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { FamilyCard } from './family-card';
import { environment } from '../../../environments/environment';

@Injectable()
export class FamilyCardService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getFamilyCards(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/family-cards`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getFamilyCard(id: number): Promise < FamilyCard > {

        let url = `${this.apiEndpoint}/family-cards/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveFamilyCard(familyCard: FamilyCard): Promise < Object > {
        if (familyCard._id) {
            return this.updateFamilyCard(familyCard);
        }
        return this.addFamilyCard(familyCard);
    }


    private addFamilyCard(familyCard: FamilyCard): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/family-cards`;

        return this.http.post(url, familyCard, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateFamilyCard(familyCard: FamilyCard) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/family-cards/${familyCard._id}`;

        return this.http
            .patch(url, familyCard, {
                headers: headers
            })
            .toPromise()
            .then(() => familyCard)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
