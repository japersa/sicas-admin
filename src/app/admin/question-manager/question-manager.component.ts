import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UpperCasePipe } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-question-manager',
  templateUrl: './question-manager.component.html',
  styleUrls: ['./question-manager.component.css']
})
export class QuestionManagerComponent implements OnInit {

  key: string = 'categories';
  
  data: Array<Object> =[];

  questionsList =[];  

  index = null; 
  patern = null;

  constructor() {  
   }

  ngOnInit() {    
  }

  AddCategory(category:string){    
    this.data.push( {name: category, categories: []});
    $('#idCategory').val('');
    this.index = null;
    console.log(category);
  }

  addQuestion(indice){
    console.log(indice);
    this.openSurveyModal();
    this.index = indice;
    this.patern = this.questionsList[indice].name;
    console.log(this.patern);    
  }
  
  confirmAddQuestion(question){      
    this.data.push({ 
      name: question, 
      categories: []
    });     
    $('#idCategory').val('');
    this.closeSurveyModal();
  }

  deleteCategory(index: number){
    if (index > -1) {
      this.questionsList.splice(index, 1);
    }    
  }
  
  openModal() {
        $('#questionModal2').modal('show');            
  }

  closeModal() {    
    $('#questionModal2').modal('hide');
    $('#idCategory').val('');
    this.index = null;
  }

  openSurveyModal(){
    $('#surveyModal').modal('show');
  }
  closeSurveyModal(){
    $('#surveyModal').modal('hide');
  }
}
