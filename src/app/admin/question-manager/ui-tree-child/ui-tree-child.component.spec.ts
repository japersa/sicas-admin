/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { UiTreeChildComponent } from './ui-tree-child.component';

describe('UiTreeChildComponent', () => {
  let component: UiTreeChildComponent;
  let fixture: ComponentFixture<UiTreeChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiTreeChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiTreeChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
