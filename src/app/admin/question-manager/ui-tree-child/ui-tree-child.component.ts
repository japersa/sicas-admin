import { Component, OnInit, Input } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-ui-tree-child',
  templateUrl: './ui-tree-child.component.html',
  styleUrls: ['./ui-tree-child.component.css']
})
export class UiTreeChildComponent implements OnInit {

  @Input('data') items: Array<Object>;
  @Input('key') key: string;
  
  item = {
    name: "",
    categories: []
  }; 

  question = {
    name: "",
    categories: []
  }; 

  index = null; 
  patern = null;

  category = {
    name: "",
    categories: []
  };

  constructor() { }

  ngOnInit() {
  }

  openModal(question) {
    console.log(question)
    this.item = question
    this.category = {
      name: question.name,
      categories: question.categories
    };
    $('#editCategoryModal').modal('show');            
  }

  confirmCategory(question){   
    console.log(question)   
    this.item.name = question.name;
    this.item.categories = question.categories;

    this.category = {
      name: "",
      categories: []
    };
    this.closeSurveyModal();
  }

  closeSurveyModal(){
    $('#editCategoryModal').modal('hide');
  }


  openModalQuestion(category) {
    this.category = category;
    $('#questionModal').modal('show');            
  }

  confirmQuestion(question){   
    console.log(question)   
    this.category.categories.push(question)
    this.question = {
      name: "",
      categories: []
    };
    this.category = {
      name: "",
      categories: []
    };
    this.closeQuestionModal();
  }

  closeQuestionModal(){
    $('#questionModal').modal('hide');
  }
}
