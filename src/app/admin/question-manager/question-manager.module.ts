import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { QuestionManagerComponent } from './question-manager.component';
import { QuestionManagerRoutingModule } from './question-manager.routing';
import { UiTreeComponent } from './ui-tree/ui-tree.component';
import { UiTreeChildComponent } from './ui-tree-child/ui-tree-child.component';


@NgModule({
  imports: [
    CommonModule,
    QuestionManagerRoutingModule,
    FormsModule
  ],
  declarations: [QuestionManagerComponent, UiTreeComponent, UiTreeChildComponent]
})
export class QuestionManagerModule { }
