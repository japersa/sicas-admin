import { Component, OnInit, Input } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-ui-tree',
  templateUrl: './ui-tree.component.html',
  styleUrls: ['./ui-tree.component.css']
})
export class UiTreeComponent implements OnInit {

  @Input('data') items: Array<Object>;
  @Input('key') key: string;

  itemw = {
    name: "",
    categories: []
  }; 

  question = {
    name: "",
    categories: []
  }; 

  index = null; 
  patern = null;

  category = {
    name: "",
    categories: []
  };

  constructor() { }

  ngOnInit() {
  }

  openModal(category) {
    console.log(category)
    this.itemw = category
    this.category = {
      name: category.name,
      categories: category.categories
    };
    $('#editCategoryModal').modal('show');            
  }

  confirmCategory(question){   
    console.log(question)   
    this.itemw.name = question.name;
    this.itemw.categories = question.categories;

    this.category = {
      name: "",
      categories: []
    };

    this.closeSurveyModal();
  }

  closeSurveyModal(){
    $('#editCategoryModal').modal('hide');
  }


  openModalQuestion(category) {
    this.category = {
      name: category.name,
      categories: category.categories
    };
    
    console.log(category)
    $('#questionModal').modal('show');            
  }

  confirmQuestion(question){   
    console.log(question)   
    console.log(this.category)   

    this.category.categories.push(question)
    this.question = {
      name: "",
      categories: []
    };

    this.closeQuestionModal();
  }

  closeQuestionModal(){
    $('#questionModal').modal('hide');
  }
}


