import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConditionComponent } from './condition.component';
import { ConditionFormComponent } from './condition-form/condition-form.component';
import { ConditionRoutingModule } from './condition.routing';

@NgModule({
  imports: [
    CommonModule, ConditionRoutingModule 
  ],
  declarations: [ConditionComponent, ConditionFormComponent]
})
export class ConditionModule { }
