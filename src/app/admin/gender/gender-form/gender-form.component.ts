import { Component, OnInit, OnDestroy } from '@angular/core';
import { Gender } from '../gender';
import { Router, ActivatedRoute } from '@angular/router';
import { GenderService } from '../gender.service';

@Component({
  selector: 'app-gender-form',
  templateUrl: './gender-form.component.html',
  styleUrls: ['./gender-form.component.scss'],
  providers: [GenderService]
})

export class GenderFormComponent implements OnInit, OnDestroy {

  id: number;
  gender = new Gender(null, null, null);
  private sub: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _genderService: GenderService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getGender();
      }
    });
  }

  getGender() {
    this._genderService.getGender(this.id).then(
      data => {
        this.gender = new Gender(data._id, data.code, data.description);
      }
    ).catch(error => console.log(error));

  }

  onSubmit(f: any) {
    this._genderService
      .saveGender(this.gender)
      .then(data => {
        this._router.navigate(['/admin/genders']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}