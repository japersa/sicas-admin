export class Gender {
    constructor(
    public _id: number,
    public code: string,
    public description: string 
  ) {  }
}
