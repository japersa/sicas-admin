import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { GenderComponent } from './gender.component';
import { GenderRoutingModule } from './gender.routing';
import { GenderListComponent } from './gender-list/gender-list.component';
import { GenderFormComponent } from './gender-form/gender-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    GenderRoutingModule,
    DataTablesModule
  ],
  declarations: [ GenderComponent, GenderListComponent, GenderFormComponent]
})
export class GenderModule { }
