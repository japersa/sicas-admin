import { Component, OnInit } from '@angular/core';
import { GenderService } from '../gender.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-gender-list',
  templateUrl: './gender-list.component.html',
  styleUrls: ['./gender-list.component.scss'],
  providers: [GenderService]
})
export class GenderListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  genderList = [];

  constructor(private _genderService: GenderService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllGenders();
  }

  getAllGenders() {
    this._genderService.getGenders()
      .then(data => {
        this.genderList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
