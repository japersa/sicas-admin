import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Gender } from './gender';
import { environment } from '../../../environments/environment';

@Injectable()
export class GenderService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getGenders(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/genders`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getGender(id: number): Promise < Gender > {

        let url = `${this.apiEndpoint}/genders/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveGender(gender: Gender): Promise < Object > {
        if (gender._id) {
            return this.updateGender(gender);
        }
        return this.addGender(gender);
    }


    private addGender(gender: Gender): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/genders`;

        return this.http.post(url, gender, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateGender(gender: Gender) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/genders/${gender._id}`;

        return this.http
            .patch(url, gender, {
                headers: headers
            })
            .toPromise()
            .then(() => gender)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
