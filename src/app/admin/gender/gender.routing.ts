import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { GenderComponent } from './gender.component';
import { GenderListComponent } from './gender-list/gender-list.component';
import { GenderFormComponent } from './gender-form/gender-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: GenderComponent,

    children: [{
      path: '',
      component: GenderListComponent,
    },{
      path: 'new',
      component: GenderFormComponent,
    },{
      path: 'edit/:id',
      component: GenderFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenderRoutingModule {}
