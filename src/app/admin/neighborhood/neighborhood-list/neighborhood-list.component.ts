import { Component, OnInit } from '@angular/core';
import { NeighborhoodService } from '../neighborhood.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-neighborhood-list',
  templateUrl: './neighborhood-list.component.html',
  styleUrls: ['./neighborhood-list.component.scss'],
  providers: [NeighborhoodService]
})
export class NeighborhoodListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  neighborhoodList = [];

  constructor(private _neighborhoodService: NeighborhoodService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllCountries();
  }

  getAllCountries() {
    this._neighborhoodService.getNeighborhoods()
      .then(data => {
        this.neighborhoodList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));
      

  }

}
