/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NeighborhoodService } from './neighborhood.service';

describe('Service: Neighborhood', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NeighborhoodService]
    });
  });

  it('should ...', inject([NeighborhoodService], (service: NeighborhoodService) => {
    expect(service).toBeTruthy();
  }));
});
