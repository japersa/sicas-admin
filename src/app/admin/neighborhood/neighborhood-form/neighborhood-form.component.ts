import { Component, OnInit, OnDestroy } from '@angular/core';
import { Neighborhood } from '../neighborhood';
import { Router, ActivatedRoute } from '@angular/router';
import { NeighborhoodService } from '../neighborhood.service';
import { Commune } from '../../commune/commune';
import { CommuneService } from '../../commune/commune.service';
import { City } from '../../city/city';
import { Country } from '../../country/country';
import { Department } from '../../department/department';
import { Sidewalk } from '../../sidewalk/sidewalk';

@Component({
  selector: 'app-neighborhood-form',
  templateUrl: './neighborhood-form.component.html',
  styleUrls: ['./neighborhood-form.component.scss'],
  providers: [NeighborhoodService, CommuneService]
})

export class NeighborhoodFormComponent implements OnInit, OnDestroy {

  id: number;
  neighborhood = new Neighborhood(null, null, null,
    new Country(null, null, null),
    new Department(null, null, null, null),
    new City(null, null, null, null, null),
    new Sidewalk(null, null, null, null, null, null),
    new Commune(null, null, null, null, null, null, null));
  private sub: any;
  communeList = [];

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _neighborhoodService: NeighborhoodService,
    private _communeService: CommuneService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getNeighborhood();
      }
    });
    this.getAllCommunes();
  }

  getNeighborhood() {
    this._neighborhoodService.getNeighborhood(this.id).then(
      data => {
        this.neighborhood = new Neighborhood(
            data._id,
            data.code,
            data.description,
            data.country,
            data.department,
            data.city,
            data.sidewalk,
            data.commune
          );
      }
    ).catch(error => console.log(error));

  }

  getAllCommunes() {
    this._communeService.getCommunes()
      .then(data => {
        this.communeList = data;
      })
      .catch(error => console.log(error));
  }

  onSubmit(f: any) {
    this._neighborhoodService
      .saveNeighborhood(this.neighborhood)
      .then(data => {
        this._router.navigate(['/admin/neighborhoods']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}