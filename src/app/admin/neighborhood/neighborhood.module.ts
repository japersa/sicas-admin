import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { NeighborhoodComponent } from './neighborhood.component';
import { NeighborhoodRoutingModule } from './neighborhood.routing';
import { NeighborhoodListComponent } from './neighborhood-list/neighborhood-list.component';
import { NeighborhoodFormComponent } from './neighborhood-form/neighborhood-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NeighborhoodRoutingModule,
    DataTablesModule
  ],
  declarations: [ NeighborhoodComponent, NeighborhoodListComponent, NeighborhoodFormComponent]
})
export class NeighborhoodModule { }
