import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Neighborhood } from './neighborhood';
import { environment } from '../../../environments/environment';

@Injectable()
export class NeighborhoodService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getNeighborhoods(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/neighborhoods`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getNeighborhoodsCommune(id: number): Promise < Object[] > {
        let url = `${this.apiEndpoint}/neighborhoods/commune/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getNeighborhood(id: number): Promise < Neighborhood > {

        let url = `${this.apiEndpoint}/neighborhoods/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveNeighborhood(neighborhood: Neighborhood): Promise < Object > {
        if (neighborhood._id) {
            return this.updateNeighborhood(neighborhood);
        }
        return this.addNeighborhood(neighborhood);
    }


    private addNeighborhood(neighborhood: Neighborhood): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/neighborhoods`;

        return this.http.post(url, neighborhood, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateNeighborhood(neighborhood: Neighborhood) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/neighborhoods/${neighborhood._id}`;

        return this.http
            .patch(url, neighborhood, {
                headers: headers
            })
            .toPromise()
            .then(() => neighborhood)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
