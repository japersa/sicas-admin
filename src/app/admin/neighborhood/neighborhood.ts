import { Commune } from '../commune/commune';
import { City } from '../city/city';
import { Sidewalk } from '../sidewalk/sidewalk';
import { Department } from '../department/department';
import { Country } from '../country/country';

export class Neighborhood {
    constructor(
    public _id: number,
    public code: string,
    public description: string,
    public country: Country,
    public department: Department,
    public city: City,
    public sidewalk: Sidewalk,
    public commune: Commune
  ) {  }
}
