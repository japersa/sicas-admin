import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { NeighborhoodComponent } from './neighborhood.component';
import { NeighborhoodListComponent } from './neighborhood-list/neighborhood-list.component';
import { NeighborhoodFormComponent } from './neighborhood-form/neighborhood-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: NeighborhoodComponent,

    children: [{
      path: '',
      component: NeighborhoodListComponent,
    },{
      path: 'new',
      component: NeighborhoodFormComponent,
    },{
      path: 'edit/:id',
      component: NeighborhoodFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NeighborhoodRoutingModule {}
