import { Department } from '../department/department';
import { Country } from '../country/country';

export class City {
    constructor(
    public _id: number,
    public code: string,
    public country: Country,
    public department: Department,
    public description: string,
  ) {  }
}
