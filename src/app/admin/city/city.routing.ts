import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { CityComponent } from './city.component';
import { CityListComponent } from './city-list/city-list.component';
import { CityFormComponent } from './city-form/city-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: CityComponent,

    children: [{
      path: '',
      component: CityListComponent,
    },{
      path: 'new',
      component: CityFormComponent,
    },{
      path: 'edit/:id',
      component: CityFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CityRoutingModule {}
