import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { CityComponent } from './city.component';
import { CityRoutingModule } from './city.routing';
import { CityListComponent } from './city-list/city-list.component';
import { CityFormComponent } from './city-form/city-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CityRoutingModule,
    DataTablesModule
  ],
  declarations: [ CityComponent, CityListComponent, CityFormComponent]
})
export class CityModule { }
