import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { City } from './city';
import { environment } from '../../../environments/environment';

@Injectable()
export class CityService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getCities(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/cities`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCitiesDepartment(id: number): Promise < Object[] > {

        let url = `${this.apiEndpoint}/cities/department/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCity(id: number): Promise < City > {

        let url = `${this.apiEndpoint}/cities/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveCity(city: City): Promise < Object > {
        if (city._id) {
            return this.updateCity(city);
        }
        return this.addCity(city);
    }


    private addCity(city: City): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/cities`;

        return this.http.post(url, city, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateCity(city: City) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/cities/${city._id}`;

        return this.http
            .patch(url, city, {
                headers: headers
            })
            .toPromise()
            .then(() => city)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
