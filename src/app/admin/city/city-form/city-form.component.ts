import { Component, OnInit, OnDestroy } from '@angular/core';
import { City } from '../city';
import { Router, ActivatedRoute } from '@angular/router';
import { CityService } from '../city.service';
import { DepartmentService } from '../../department/department.service';
import { Department } from '../../department/department';
import { Country } from '../../country/country';

@Component({
  selector: 'app-city-form',
  templateUrl: './city-form.component.html',
  styleUrls: ['./city-form.component.scss'],
  providers: [CityService, DepartmentService]
})

export class CityFormComponent implements OnInit, OnDestroy {

  id: number;
  city = new City(
    null,
    null,
    new Country(null, null, null),
    new Department(null, null, null, null),
    null);
  private sub: any;
  departmentList = [];

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _cityService: CityService,
    private _departmentService: DepartmentService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getCity();
      }
    });
    this.getAllDepartments();
  }

  getCity() {
    this._cityService.getCity(this.id).then(
      data => {
        this.city = new City(data._id, data.code, data.country, data.department, data.description);
      }
    ).catch(error => console.log(error));

  }

  getAllDepartments() {
    this._departmentService.getDepartments()
      .then(data => {
        this.departmentList = data;
      })
      .catch(error => console.log(error));
  }



  onSubmit(f: any) {
    this._cityService
      .saveCity(this.city)
      .then(data => {
        this._router.navigate(['/admin/cities']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}