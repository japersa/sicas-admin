import { Component, OnInit } from '@angular/core';
import { CityService } from '../city.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.scss'],
  providers: [CityService]
})
export class CityListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  cityList = [];

  constructor(private _cityService: CityService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllCities();
  }

  getAllCities() {
    this._cityService.getCities()
      .then(data => {
        this.cityList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));
      

  }

}
