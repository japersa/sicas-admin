import { Component, OnInit, OnDestroy } from '@angular/core';
import { Commune } from '../commune';
import { Router, ActivatedRoute } from '@angular/router';
import { CommuneService } from '../commune.service';
import { DepartmentService } from '../../department/department.service';
import { City } from '../../city/city';
import { CityService } from '../../city/city.service';
import { Department } from '../../department/department';
import { Sidewalk } from '../../sidewalk/sidewalk';
import { Country } from '../../country/country';

@Component({
  selector: 'app-commune-form',
  templateUrl: './commune-form.component.html',
  styleUrls: ['./commune-form.component.scss'],
  providers: [CommuneService, CityService]
})

export class CommuneFormComponent implements OnInit, OnDestroy {

  id: number;
  commune = new Commune(null, null, null,
  new Country(null, null, null),
  new Department(null, null, null, null),
  new City(null, null, null, null, null),
  new Sidewalk(null, null, null, null, null, null));
  private sub: any;
  cityList = [];

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _communeService: CommuneService,
    private _cityService: CityService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getCommune();
      }
    });
    this.getAllDepartments();
  }

  getCommune() {
    this._communeService.getCommune(this.id).then(
      data => {
        this.commune = new Commune(data._id, data.code, data.description, data.country, data.department, data.city, data.sidewalk);
      }
    ).catch(error => console.log(error));

  }

  getAllDepartments() {
    this._cityService.getCities()
      .then(data => {
        this.cityList = data;
      })
      .catch(error => console.log(error));
  }



  onSubmit(f: any) {
    this._communeService
      .saveCommune(this.commune)
      .then(data => {
        this._router.navigate(['/admin/communes']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}