import { City } from '../city/city';
import { Country } from '../country/country';
import { Department } from '../department/department';
import { Sidewalk } from '../sidewalk/sidewalk';

export class Commune {
    constructor(
    public _id: number,
    public code: string,
    public description: string,
    public country: Country,
    public department: Department,
    public city: City,
    public sidewalk: Sidewalk
  ) {  }
}
