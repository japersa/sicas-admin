import { Component, OnInit } from '@angular/core';
import { CommuneService } from '../commune.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-commune-list',
  templateUrl: './commune-list.component.html',
  styleUrls: ['./commune-list.component.scss'],
  providers: [CommuneService]
})
export class CommuneListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  communeList = [];

  constructor(private _communeService: CommuneService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllCountries();
  }

  getAllCountries() {
    this._communeService.getCommunes()
      .then(data => {
        this.communeList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
