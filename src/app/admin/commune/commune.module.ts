import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { CommuneComponent } from './commune.component';
import { CommuneRoutingModule } from './commune.routing';
import { CommuneListComponent } from './commune-list/commune-list.component';
import { CommuneFormComponent } from './commune-form/commune-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CommuneRoutingModule,
    DataTablesModule
  ],
  declarations: [ CommuneComponent, CommuneListComponent, CommuneFormComponent]
})
export class CommuneModule { }
