/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CommuneService } from './commune.service';

describe('Service: Commune', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommuneService]
    });
  });

  it('should ...', inject([CommuneService], (service: CommuneService) => {
    expect(service).toBeTruthy();
  }));
});
