import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { CommuneComponent } from './commune.component';
import { CommuneListComponent } from './commune-list/commune-list.component';
import { CommuneFormComponent } from './commune-form/commune-form.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: CommuneComponent,

    children: [{
      path: '',
      component: CommuneListComponent,
    },{
      path: 'new',
      component: CommuneFormComponent,
    },{
      path: 'edit/:id',
      component: CommuneFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommuneRoutingModule {}
