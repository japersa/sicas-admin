import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Commune } from './commune';
import { environment } from '../../../environments/environment';

@Injectable()
export class CommuneService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getCommunes(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/communes`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCommunesCity(id: number): Promise < Object[] > {

        let url = `${this.apiEndpoint}/communes/city/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCommune(id: number): Promise < Commune > {

        let url = `${this.apiEndpoint}/communes/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveCommune(commune: Commune): Promise < Object > {
        if (commune._id) {
            return this.updateCommune(commune);
        }
        return this.addCommune(commune);
    }


    private addCommune(commune: Commune): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/communes`;

        return this.http.post(url, commune, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateCommune(commune: Commune) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/communes/${commune._id}`;

        return this.http
            .patch(url, commune, {
                headers: headers
            })
            .toPromise()
            .then(() => commune)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
