import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { ProfessionalComponent } from './professional.component';
import { ProfessionalFormComponent } from './professional-form/professional-form.component';
import { ProfessionalListComponent } from './professional-list/professional-list.component';
import { AuthGuard } from 'src/app/auth-guard';

const routes: Routes = [
  {
    path: '',
    component: ProfessionalComponent,

    children: [{
      path: '',
      component: ProfessionalListComponent,
    },{
      path: 'new',
      component: ProfessionalFormComponent,
    },{
      path: 'edit/:id',
      component: ProfessionalFormComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfessionalRoutingModule {}
