import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Professional } from './professional';
import { environment } from '../../../environments/environment';

@Injectable()
export class ProfessionalService {

    private apiEndpoint = environment.apiUrl;

    constructor(private http: Http) {}

    getProfessionals(): Promise < Object[] > {

        let url = `${this.apiEndpoint}/professionals`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getProfessional(id: number): Promise < Professional > {

        let url = `${this.apiEndpoint}/professionals/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveProfessional(professional: Professional): Promise < Object > {
        if (professional._id) {
            return this.updateProfessional(professional);
        }
        return this.addProfessional(professional);
    }


    private addProfessional(professional: Professional): Promise < Object > {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/professionals`;

        return this.http.post(url, professional, {
                headers: headers
            })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private updateProfessional(professional: Professional) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiEndpoint}/professionals/${professional._id}`;

        return this.http
            .patch(url, professional, {
                headers: headers
            })
            .toPromise()
            .then(() => professional)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('error', error);
        return Promise.reject(error.message || error);
    }

}
