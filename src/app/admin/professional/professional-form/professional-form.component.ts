import { Component, OnInit, OnDestroy } from '@angular/core';
import { Professional } from '../professional';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfessionalService } from '../professional.service';
import { Charge } from '../../charge/charge';
import { ChargeService } from '../../charge/charge.service';

@Component({
  selector: 'app-professional-form',
  templateUrl: './professional-form.component.html',
  styleUrls: ['./professional-form.component.scss'],
  providers: [ProfessionalService, ChargeService]
})

export class ProfessionalFormComponent implements OnInit, OnDestroy {

  id: number;
  professional = new Professional(null, null, new Charge(null, null, null));
  private sub: any;
  chargeList = [];

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _professionalService: ProfessionalService,
    private _chargeService: ChargeService
  ) {}

  ngOnInit() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      if(params['id']){
        this.id = params['id'];
        this.getProfessional();
      }
    });
    this.getAllCharges();
  }

  getProfessional() {
    this._professionalService.getProfessional(this.id).then(
      data => {
        this.professional = new Professional(data._id, data.description, data.charge);
      }
    ).catch(error => console.log(error));

  }

  getAllCharges() {
    this._chargeService.getCharges()
      .then(data => {
        this.chargeList = data;
      })
      .catch(error => console.log(error));
  }



  onSubmit(f: any) {
    this._professionalService
      .saveProfessional(this.professional)
      .then(data => {
        this._router.navigate(['/admin/professionals']);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}