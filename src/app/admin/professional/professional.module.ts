import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { ProfessionalComponent } from './professional.component';
import { ProfessionalRoutingModule } from './professional.routing';
import { ProfessionalListComponent } from './professional-list/professional-list.component';
import { ProfessionalFormComponent } from './professional-form/professional-form.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProfessionalRoutingModule,
    DataTablesModule
  ],
  declarations: [ ProfessionalComponent, ProfessionalListComponent, ProfessionalFormComponent]
})
export class ProfessionalModule { }
