import { Component, OnInit } from '@angular/core';
import { ProfessionalService } from '../professional.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-professional-list',
  templateUrl: './professional-list.component.html',
  styleUrls: ['./professional-list.component.scss'],
  providers: [ProfessionalService]
})
export class ProfessionalListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject < any > ();
  professionalList = [];

  constructor(private _professionalService: ProfessionalService) {}

  ngOnInit() {
    this.dtOptions = {
      language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      }
    }
    this.getAllCountries();
  }

  getAllCountries() {
    this._professionalService.getProfessionals()
      .then(data => {
        this.professionalList = data;
        this.dtTrigger.next();
      })
      .catch(error => console.log(error));

  }

}
