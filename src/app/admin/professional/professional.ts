import { Charge } from "../charge/charge";

export class Professional {
    constructor(
    public _id: number,
    public description: string,
    public charge: Charge
  ) {  }
}
